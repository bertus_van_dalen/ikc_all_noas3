<?php
class MY_Config extends CI_Config {
  
  private $__fullUrl;
  private $__baseUrl;
  private $__firstSegment;
  private $__cacheFile;
  private $__DEFAULT_PROJ = 'mps';
  private $__loadConfigsAfterConstruct;
  
  function __construct() {
    parent::__construct();
    $this->_print('error reporting level: ' . error_reporting());
    $this->__fullUrl = "http://" . filter_input(INPUT_SERVER, 'HTTP_HOST') . filter_input(INPUT_SERVER, 'REQUEST_URI');
    $this->__baseUrl = $this->_getBaseUrl();
    $this->_defineGlobals();
    $this->_setBaseItems();
    $this->__loadConfigsAfterConstruct = array("myconfig");
    $this->_getDB();
    $this->__firstSegment = $this->_getFirstSegment();
    define('FCF_CONF', $this->_retrieveConf());
    $this->__loadConfigsAfterConstructLoad();
    header('P3P: CP="Potato"');
	}
  private function _getDB(){
    $dbconfFile = FCF_PRIV_CACHE_DIR . '/dbconf/db_conf.json';
    if(is_file($dbconfFile)){
      $this->set_item("mydb", json_decode(file_get_contents($dbconfFile)));
    }
  }
  private function __loadConfigsAfterConstructLoad(){
    log_message("debug",json_encode($this->__loadConfigsAfterConstruct));
    foreach ($this->__loadConfigsAfterConstruct as $i => $cf){
      $this->load($cf);
    }
  }
  private function _print($m){
    if (isset($_GET['fcf_print'])) {
      $out = "<pre style='white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;'>";
      $out .= $m;
      $out .= "</pre>";
      print($out);
    }
  }
  private function _getBaseUrl(){
    $this->_print('full url: ' . $this->__fullUrl);
    $urlParsed = parse_url($this->__fullUrl);
    if(false === $urlParsed) return "";
    $this->_print('url_parse: ' . print_r($urlParsed,true));
    $urlReconstructed = $urlParsed['scheme'] . "://" . $urlParsed['host'];
    if(array_key_exists("port", $urlParsed)) { $urlReconstructed .= ":" . $urlParsed["port"]; }
    $self = filter_input(INPUT_SERVER, 'PHP_SELF');
    $this->_print('self: ' . $self);
    $endOfBase = strlen(substr($self, 0, strrpos($self, '/') + 1));
    $this->_print('end of base: ' . $endOfBase);
    $urlReconstructed .= substr($urlParsed['path'], 0, $endOfBase);
    $this->_print('url 1: ' . $urlReconstructed);
    $indexpos = strpos($urlReconstructed, 'index.php');
    if ($indexpos) { $urlReconstructed = substr($urlReconstructed, 0, $indexpos);}
    $this->_print('url 2: ' . $urlReconstructed);
    return $urlReconstructed;
  }
  private function _getFirstSegment(){
    $path = substr($this->__fullUrl, strlen($this->__baseUrl));
    $this->_print("path: " . $path);
    if('?' !== substr($path, 0, 1)){
      $qSplit = explode('?', $path);
      $qSplitFiltered = array_filter($qSplit);
      $pathNoQ = reset($qSplitFiltered);
      $pathNoQSplit = array_filter(explode('/', $pathNoQ));
    }else{
      $pathNoQSplit = array();
    }
    $this->_print('path split: ' . print_r($pathNoQSplit,true));
    $firstSegment = count($pathNoQSplit) > 0 ? reset($pathNoQSplit) : $this->__DEFAULT_PROJ;
    $this->_print("first segment: " . $firstSegment);
    return $firstSegment;
  }
  private function _defineGlobals(){
    $cwd = getcwd();
    $dd = getenv('OPENSHIFT_DATA_DIR') ? getenv('OPENSHIFT_DATA_DIR'): FCPATH . 'data';
    if(strripos($dd, '/') === strlen($dd) -1) { $dd = substr ($dd, 0, strlen ($dd) -1); }
    define('FCF_DATA_DIR', $dd);
    define('FCF_PRIV_CACHE_DIR', FCF_DATA_DIR . '/priv_cache');
    define('FCF_BASE_URL', $this->__baseUrl);
    define('FCF_LOG_DIR', $cwd . "/log");
    define('FCF_CACHE_DIR', FCF_DATA_DIR . "/cache");
    define('FCF_TMP_DATA_DIR', FCF_DATA_DIR . "/tmp_data");
    define('FCF_DOWNLOAD_DIR', $cwd . "/download");
    define('FCF_DATA_URL', FCF_BASE_URL . "data");
    define('FCF_CACHE_URL', FCF_DATA_URL . "/cache");
    define('FCF_PRIV_CACHE_URL', FCF_DATA_URL . "/priv_cache");
    define('FCF_TMP_DATA_URL', FCF_DATA_URL . "/tmp_data");
    define('FCF_DOWNLOAD_URL', FCF_BASE_URL . "download");
  }
  private function _setBaseItems(){
    $this->set_item("base_url", FCF_BASE_URL);
  }
  private function _retrieveConf(){
    $this->__cacheFile = FCF_PRIV_CACHE_DIR . '/projects.phpd';
    if(!is_file($this->__cacheFile)){ return 'none'; }
    return $this->_retrieveConfFromFile();
  }
  private function _retrieveConfFromFile(){
    if(is_file($this->__cacheFile)){
      $p = unserialize(file_get_contents($this->__cacheFile));
    }
    $default = strpos(filter_input(INPUT_SERVER, 'REQUEST_URI'), '/manage') === 0 ? 'none' : $this->__DEFAULT_PROJ;
    return (!isset($p) || !in_array($this->__firstSegment, $p)) ? $default : $this->__firstSegment;
  }
}