<?php

/**
 * copyright (c) 2008 - 2014 e.k. van dalen the netherlands, all rights reserved
 */
define('ENVIRONMENT', 'production');
error_reporting(E_ALL);
ini_set('display_errors', '1');
 
$system_path = 'ci';
if (realpath($system_path) !== FALSE) {
  $system_path = realpath($system_path) . '/';
}
$system_path = rtrim($system_path, '/') . '/';
define('BASEPATH', str_replace("\\", "/", $system_path));
if (!is_dir($system_path)) {
  exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: " . pathinfo(__FILE__, PATHINFO_BASENAME));
}

define('SELF', pathinfo($_SERVER["SCRIPT_FILENAME"], PATHINFO_BASENAME));
$application_folder = 'app';
if (is_dir($application_folder)) {
  define('APPPATH', $application_folder . '/');
} else {
  if (!is_dir(BASEPATH . $application_folder . '/')) {
    exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
  }
  define('APPPATH', BASEPATH . $application_folder . '/');
}

define('EXT', '.php');
define('FCPATH', str_replace(SELF, '', $_SERVER["SCRIPT_FILENAME"]));
define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));
require_once BASEPATH . 'core/CodeIgniter.php';