<?php
require_once(BASEPATH.'database/DB_forge.php');
require_once(BASEPATH.'database/drivers/mysql/mysql_forge.php');

class Dbforge extends CI_DB_mysql_forge{

	public function __construct()
	{
		parent::__construct();
	}

	public function set_database($db)
	{
		$this->db =& $db;
	}
}