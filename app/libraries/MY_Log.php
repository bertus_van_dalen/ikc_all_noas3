<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Logging Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Logging
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/errors.html
 */
class MY_Log extends CI_Log {

	protected $_log_path;
	protected $_threshold	= 2;
	protected $_date_fmt	= 'Y-m-d H:i:s';
	protected $_enabled	= TRUE;
	protected $_levels	= array('ERROR' => '1', 'INFO' => '2',  'DEBUG' => '3', 'ALL' => '4', "DATABASE" => "5");
  protected $_levelNumbers = array(1,2,3,4,5);

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$config =& get_config();

		$this->_log_path = ($config['log_path'] != '') ? $config['log_path'] : APPPATH.'logs/';

		if ( ! is_dir($this->_log_path) OR ! is_really_writable($this->_log_path))
		{
			$this->_enabled = FALSE;
		}

		if (is_numeric($config['log_threshold']))
		{
			$this->_threshold = $config['log_threshold'];
		}

		if ($config['log_date_format'] != '')
		{
			$this->_date_fmt = $config['log_date_format'];
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Write Log File
	 *
	 * Generally this function will be called using the global log_message() function
	 *
	 * @param	string	the error level
	 * @param	string	the error message
	 * @param	bool	whether the error is a native PHP error
	 * @return	bool
	 */
	public function write_log($level = 'error', $msg, $php_error = FALSE)
	{
		//exit("watnou");
		if ($this->_enabled === FALSE)
		{
			return FALSE;
		}

		$level = strtoupper($level);

		if ( ! isset($this->_levels[$level]) OR ($this->_levels[$level] > $this->_threshold))
		{
			return FALSE;
		}

		//$filepath = $this->_log_path . date('Y-m-d') . '-' . (($level == "DATABASE") ? $level : "LOG") . '.php';
    $filepath = $this->_log_path . date('Y-m-d') . '-' . (($level == "DATABASE") ? "LOG" : "LOG") . '.php';
    
    $message  = '';

		if ( ! file_exists($filepath))
		{
			$message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
		}

		if ( ! $fp = @fopen($filepath, FOPEN_WRITE_CREATE))
		{
			return FALSE;
		}
		
		$dtstr = date($this->_date_fmt);
		
		if(is_array($msg)){
		 throw new Exception(print_r($msg,true));
		}

		$message .= $level.' '.(($level == 'INFO') ? ' -' : '-').' '.$dtstr. ' --> '.$msg."\n";

		flock($fp, LOCK_EX);
		fwrite($fp, $message);
		flock($fp, LOCK_UN);
		fclose($fp);

		// @chmod($filepath, FILE_WRITE_MODE); operation not permitted in my environment...
		
		return TRUE;
	}
  public function setThreshold($l){
    if(!in_array($l,$this->_levelNumbers)){
      log_message("error","MY_Log->setThreshold: unsupported log threshold (" . $l . ")");
      return false;
    }
    $this->_threshold = $l;
    return true;
  }
  public function getLevels(){
    return $this->_levels;
  }
  public function isValidLogLevel($v){
    return in_array($v, $this->_levels);
  }
}
// END Log Class

/* End of file Log.php */
/* Location: ./system/libraries/Log.php */
