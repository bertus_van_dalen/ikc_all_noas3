<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include(APPPATH.'/third_party/rb/rb.php');
include(APPPATH.'/third_party/rb/rb_preloader.php');
include(APPPATH.'/third_party/rb/rb_sql_helper.php');

use \RedBeanPHP\Logger as RedBean_Logger;

class CI_RB_Logger implements RedBean_Logger {
  private $CI;
  function __construct(){
      $this->CI =& get_instance();
      log_message("debug","logger constructor");
  }
  public function log() {
    if (func_num_args() > 0) {
      foreach (func_get_args() as $argument) {
        if (is_array($argument)) {
          if (count($argument)) {
            log_message("database", print_r($argument, true));
          }
        } else {
          if ($argument) {
            log_message("database", $argument);
          }
        }
      }
    }
  }
}

class Rb {
	private $_conn;
  function __construct() {
    $ci = get_instance();
    $db = $ci->config->item("mydb");
    if(!$db){
      $this->_conn = false;
      return;
    }
    $this->_conn = true;
    // todo - abstract into its own class, the retrieval of env for DB
    if(false !== getenv('OPENSHIFT_MYSQL_DB_HOST')){
      $dbh = getenv('OPENSHIFT_MYSQL_DB_HOST');
      $dbu = getenv('OPENSHIFT_MYSQL_DB_USERNAME');
      $dbp = getenv('OPENSHIFT_MYSQL_DB_PASSWORD');
      $dbd = $db->database;
      $dbr = getenv('OPENSHIFT_MYSQL_DB_PORT');
      R::setup("mysql:host=" . $dbh . ";dbname=" . $dbd . ";port=" . $dbr, $dbu, $dbp);
    }else{
      R::setup("mysql:host=" . $db->hostname . ";dbname=" . $db->database, $db->username, $db->password);
    }
    R::debug(true, 1);
    $logger = new CI_RB_Logger();
		R::getDatabaseAdapter()
            ->getDatabase()
            ->setLogger($logger);
	}
  public function hasConnection(){
    return $this->_conn;
  }
}