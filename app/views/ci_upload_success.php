<html>
<head>
<title>Upload Form</title>
</head>
<body>

<h3>Your file was successfully uploaded!</h3>

<ul>
<?php foreach ($upload_data as $item => $value):?>
<li><?php echo $item;?>: <?php echo $value;?></li>
<?php endforeach; ?>
</ul>

<?php if(isset($uploadUrl)): ?><p><?php echo anchor($uploadUrl, 'Upload Another File!'); ?></p><?php endif; ?>
<?php if(isset($listUrl)):?><p><?php echo anchor($listUrl, 'See yr files!'); ?></p><?php endif; ?>

</body>
</html>