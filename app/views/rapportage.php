<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Status Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
a:link {
	color: #61c7dd;
}
a:hover {
	color: #59b8cc;
}
</style>
</head>
<body bgcolor="#161616" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" bgcolor="#161616"><table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="font: 14px Helvetica, Arial, sans-serif; color: #767572; line-height: 100%;">
      <tr>
        <td valign="top"><table width="600" height="204" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td style="height: 56px;"><img src="<?php echo $img_loc; ?>arrow.gif" width="96" height="56" alt="" style="display: block;"></td>
          </tr>
          <tr>
            <td colspan="2" style="background-image: url(<?php echo $img_loc; ?>header_top_separator.gif); background-repeat: no-repeat; height: 7px;"><img src="<?php echo $img_loc; ?>header_top_separator.gif" alt="" width="600" height="7" style="display: block;"/></td>
          </tr>
          <tr>
            <td valign="top" style="width: 504px; height: 125px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="height: 125px;">
              <tr>
                <td valign="top" bgcolor="#161616" style="height: 10px;">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" bgcolor="#161616" style="height: 50px; font-family: Helvetica, Arial, sans-serif; font-size: 55px; font-weight: bold; color: #f9f8f2; letter-spacing: -2px; line-height: 90%;"> RAPPORTAGE </td>
              </tr>
              <tr>
                <td valign="top" bgcolor="#161616" style="height: 25px; font-family: Helvetica, Arial, sans-serif; font-size: 19px; font-weight: bold; color: #4e4e4e; letter-spacing: -2px;">gegenereerd op <?php setlocale(LC_TIME, 'nl_NL'); echo strftime("%A %e %B %Y").", ".strftime("%X"); ?></td>
              </tr>
            </table></td>
            <td valign="top"><img src="<?php echo $img_loc; ?>stats_baby.png" width="96" height="125" alt=""></td>
          </tr>
          <tr>
            <td colspan="2" valign="top" style="height: 16px; background-image: url(<?php echo $img_loc; ?>header_border.gif); background-repeat: no-repeat;"><img src="<?php echo $img_loc; ?>header_border.gif" width="600" height="16"/></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top" bgcolor="#161616"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top" style="height: 35px;">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="font-family: Helvetica, Arial, sans-serif; font-size: 30px; font-weight: bold; color: #767572; letter-spacing: -2px;">Ter kennisgeving</td>
              </tr>
              <tr>
                <td style="height: 30px;">&nbsp;</td>
              </tr>
              <tr>
                <td><img src="<?php echo $img_loc; ?>image_london2.jpg" width="600" height="269" alt=""></td>
              </tr>
              <tr>
                <td style="height: 30px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="font-family: Helvetica, Arial, sans-serif; font-size: 30px; font-weight: bold; color: #767572; letter-spacing: -2px;">De volgende problemen zijn gedetecteerd.</td>
              </tr>
              <tr>
                <td style="height: 30px;">&nbsp;</td>
              </tr>
              
	      <tr>
                <td style="font-family: Helvetica, Arial, sans-serif; color: #767572;">
		<?php
			$issuesStr = '';
			if(isset($errors)){
				foreach($errors as $error){
					if(strlen($issuesStr)>0){
						$issuesStr .= '<br /><br />';
					}
					$issuesStr .= $error;
				}
			}
			if(isset($warnings)){
				foreach($warnings as $warning){
					if(strlen($issuesStr)>0){
						$issuesStr .= '<br /><br />';
					}
					$issuesStr .= $warning;
				}
			}
			echo $issuesStr;
		?>
		</td>
              </tr>
              
	      
	      <tr>
                <td style="height: 40px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="height: 30px;"><img src="<?php echo $img_loc; ?>footer_05.gif" width="599" height="25" alt=""></td>
              </tr>
              <tr>
                <td style="height: 20px;">&nbsp;</td>
              </tr>
              <tr>
                <td style="height: 30px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="47%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-family: Helvetica, Arial, sans-serif; font-size: 25px; font-weight: bold; color: #767572; letter-spacing: -2px; padding-bottom: 10px;">Waarom deze mail?</td>
                      </tr>
                      <tr>
                        <td valign="top" style="font-family: Helvetica, Arial, sans-serif; color: #767572; line-height: 130%;">Deze mail is automatisch verstuurd door het veiligheidsmonitoringsysteem van de website. Adeptive zal u zo snel mogelijk een reactie sturen indien de overeengekomen monitoringtermijn van deze website nog niet is verstreken.</td>
                      </tr>
                    </table></td>
                    <td width="6%" valign="top">&nbsp;</td>
                    <td width="47%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-family: Helvetica, Arial, sans-serif; font-size: 25px; font-weight: bold; color: #767572; letter-spacing: -2px; padding-bottom: 10px;">Wat moet u doen?</td>
                      </tr>
                      <tr>
                        <td valign="top" style="font-family: Helvetica, Arial, sans-serif; color: #767572; line-height: 130%;">Als de monitoringstermijn van de website is verstreken dan kunt u deze mail beoordelen en eventueel doorsturen naar Adeptive. Is de monitoringstermijn nog niet verstreken dan hoeft u niks te doen. Heeft u vragen naar aanleiding van deze mail dan kunt u contact opnemen (zie contact informatie onderaan deze mail)</td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td style="height: 30px;">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td><table width="600" border="0" cellpadding="0" cellspacing="0" style="padding-top: 10px;">
          <tr>
            <td colspan="6"><img src="<?php echo $img_loc; ?>footer_05.gif" width="599" height="25" alt=""></td>
            <td width="1" rowspan="3"><img src="<?php echo $img_loc; ?>footer_02.gif" width="1" height="140" alt=""></td>
          </tr>
          <tr>
            <td width="179" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="21%" valign="top"><img src="<?php echo $img_loc; ?>icon_email_friend.gif" width="37" height="37" alt=""></td>
                <td width="10%" valign="top">&nbsp;</td>
                <td width="69%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-family: Helvetica, Arial, sans-serif; font-size: 17px; font-weight: bold; color: #f9f8f2;">statistieken</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px; color: #767572;">U kunt de statistieken bekijken <a href = "<?php echo $base_url; ?>crondaily/generalstats">statistieken</a>.
                    </td>
                  </tr>
                </table></td>
              </tr>
            </table>
              <p style="font-size: 16px; font-weight: bold; margin: 0; padding: 0 0 6px 0;">&nbsp;</p></td>
            <td width="30" valign="top">&nbsp;</td>
            <td width="36" rowspan="2" valign="top"><img src="<?php echo $img_loc; ?>icon-footer.gif" width="36" height="37" alt=""></td>
            <td width="16" rowspan="2" valign="top">&nbsp;</td>
            <td width="334" rowspan="2" valign="top"><p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; color: #767572; margin: 0; padding: 0 0 6px 0;">Contact:</p>
              <p style="font-family: Helvetica, Arial, sans-serif; font-size: 11px; color: #767572; margin: 0; padding: 0; letter-spacing: -0.3px;">
              	<?php
              		if(isset($contact)){
              			foreach($contact as $c){
              				echo $c->name.", m: ".$c->email.", t: ".$c->tel."<br />";
              			}
              		}
              	?>
              </p>
            </td>
          </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
