 <html>
  <head>
    <title>ikcomponeer back-end</title>
    <script id="ikc_busy_creating_template_TPL" type="text/x-jsrender">
      <div>
        <img src="<?php echo $ikc->conf->css_url_shared; ?>images/busy.gif"></img><span> busy creating template, please wait</span>
      </div>
    </script>
    <script id="ikc_success_creating_template_TPL" type="text/x-jsrender">
      <div>
        <span>You just created a template with ID {{:id}}. To edit it, go here: </span><a href="<?php echo $ikc->conf->project_url; ?>musbas/editTemplate/{{:id}}">click!</a>
      </div>
    </script>
    <script type="text/javascript">
      if(typeof(ikc) == "undefined"){
        var ikc = {};
      }
      ikc.db = {};
      ikc.db.template = <?php echo json_encode($ikc->templates); ?>;
      ikc.conf = <?php echo json_encode($ikc->conf); ?>;
    </script>
    <?php echo $ikc->js; ?>
  </head>
  <body>
    <iframe src="<?php echo $ikc->conf->project_url;?>xframe" id="ikcXframe" style="display:none"></iframe>
    <h1>choose template to create template for</h1>
    <h4>
      
   A template can have a parent. A composition that has a template 
   can also have settings. A composition that has a template that has 
   a parent template will load that parent's composition. This allows
   for the composition to have its own settings. The composition thus is 
   useless except that its settings will be loaded.
    </h4>
    <div id="ikc_workspace">
      <h2>available templates:</h2>
      <?php
      $headings = array();
      if(count($ikc->templates)){
        foreach($ikc->templates[0] as $colName => $fieldVal){
          $headings[] = $colName;
        }
      }
      $this->table->set_heading($headings);
      echo $this->table->generate($ikc->templates);
      ?>
      <form class="ikc-form" id="create_template_for_template">
        <span>template id to create another composition for: </span>
        <input class="ikc-formfield" id="template_id" type="text"></input>
        <input type="button" value="go" class="ikc-formsubmit"></input>
      </form>
    </div>
  </body>
</html>