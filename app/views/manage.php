<!DOCTYPE html>
<html ng-app="manage">
  <head>
    <title>manage ikcomponeer</title>
    <?php echo $carabiner->display('css'); ?>
    <?php echo $carabiner->display('js'); ?>
    
    <!-- watch out, ikc is defined in here... -->
    <script type="text/javascript">var ikc = <?php echo json_encode($ikc);?>;</script>
    
    <!-- watch out, depends on ikc being defined -->
    <?php foreach($extra_scripts as $s): ?>
    <script type="text/javascript" src="<?php echo $s; ?>"></script>
    <?php endforeach; ?>
    
    <script type="text/javascript" src="<?php echo $ikc->conf->base_url; ?>ckfinder/ckfinder.js"></script>
    
    <style type="text/css">
      input.ng-invalid.ng-dirty {
        background-color: #FA787E;
      }
      input.ng-valid.ng-dirty {
        background-color: #78FA89;
      }
    </style>
  </head>
  <body>
    <div ui-view>
    </div>
  </body>
</html> 