var ikc = <?php echo json_encode($ikc); ?>;
ikc.l = {
    p : function(s){
      var d;
      if (window.DOMParser)
      {
          p=new DOMParser();
          d=p.parseFromString(s,"text/xml");
      }
      else // IE
      {
          d=new ActiveXObject("Microsoft.XMLDOM");
          d.async=false;
          d.loadXML(s); 
      }
      return d;
    },
    a : function(){
      var s = this.p("<d>" + ikc.js + "</d>");
      var sa = s.getElementsByTagName("script");
      var i = 0;
      while( i < sa.length){
          var e = sa[i];
          var es = e.getAttribute("src");
          var de = document.createElement("script");
          de.setAttribute("src",es+"?cb="+ikc.conf.cb);
          document.getElementsByTagName("head")[0].appendChild(de);
          i++;
      }
    },
    c : function(){
      return this.d && this.$R;
    },
    t : function(){
      if(!this.c()) return;
      ikc.init();
      for(var i=0;i<this.rs.length;i++){
        var cbo = this.rs[i];
        var cb = cbo.callback;
        var cbct = typeof(cbo.context) === "undefined" ? this : cbo.context;
        var cbargs = typeof(cbo.args) === "undefined" ? [] : cbo.args;
        cb.apply(cbct, cbargs);
      }
      this.rs = [];
    },
    rs : [],
    r : function(o){
      if(this.c()){
        var ct = typeof(o.context) === "undefined" ? this : o.context;
        var a = typeof(o.args) === "undefined" ? [] : o.args;
        o.callback.apply(ct, a);
        return;
      }
      this.rs.push(o);
    }
  };
for(var k in ikc.l.f){
  ikc[k] = ikc.l.f[k];
}
ikc.l.a();
// 2016-05-15 usage: metropoleopschool roept dit aan in de html
function embedComponeertool(id, start){
    if(typeof(start) === "undefined") { start = true; }
    ikc.l.d = id;
    ikc.l.st = start;
    ikc.l.t();
}