<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" prefix="fb: http://www.facebook.com/2008/fbml" prefix="og: http://ogp.me/ns#" prefix="music: http://ogp.me/ns/music#">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>ikcomponeer</title>
    <iframe src="<?php echo $ikc->conf->project_url;?>xframe" id="ikcXframe" style="display:none"></iframe>
    <script type="text/javascript" src="<?php echo $ikc->conf->project_url;?>jshelpers"></script>
    <?php if(isset($janrain_headers)) echo $janrain_headers; ?>
    <script type="text/javascript">embedComponeertool( 'content' ); </script>
    <link href="<?php echo $ikc->conf->base_url . $ikc->conf->projectCss; ?>" rel="stylesheet" type="text/css">
    <?php if(isset($open_social_headers)) echo $open_social_headers; ?>
  </head>
	<body>
		<div id="container">
      <div id="header">
        <div id="logo"></div>
        <?php if(isset($janrain_headers)): ?><a class="janrainEngage" href="#" style="display: none;">Sign-In</a><?php endif; ?>
        <div id="mycarousel"><a>>></a></div>
      </div>
			<div id="content">
				<p>Voor het componeer programma is de nieuwste Flash Player nodig.
					<a href = "http://www.adobe.com/nl/shockwave/download/triggerpages_mmcom/flash.html" target="_blank">Installeer hier de nieuwste Flash Player</a>
				 </p>
			</div>
			<div id="secondary">
				<div id="movieViewer"><div class="iframeContainer"></div></div>
			</div>
			<div id="basicDialog" title="Basic dialog">
			</div>
			<div id="footer">
        <p><a href="info" style="color:white;">&copy; 2009-2014, Stichting Omroep Muziek</a></p>
			</div>
		</div>
	</body>
</html>
