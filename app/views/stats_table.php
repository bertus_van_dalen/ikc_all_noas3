<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'day');
	<?php
	$numRows = 0;
	foreach($table as $k => $v){
		// de eerste keer verzamelen we ook de columns
		if($numRows == 0){
			foreach($v->events as $name => $val){
				if(strpos($name,'SESSIONSTART') || strpos($name,'audioupload submitchunk succes')){
					continue;
				}
				echo "data.addColumn('number','" . str_replace("'", "", $name) . "');\n";
			}
		}
		$numRows++;
	}
	?>
	data.addRows(<?php echo $numRows; ?>);
        <?php
	$rowNum = 0;
	foreach($table as $k => $v){
		echo "data.setValue(" . $rowNum . ", 0, '" . $v->date . "');\n";
		$colNum = 1;
		foreach($v->events as $name => $val){
			if(strpos($name,'SESSIONSTART') || strpos($name,'audioupload submitchunk succes')){
				continue;
			}
			echo "data.setValue(" . $rowNum . ", " . $colNum . ", " . $val . ");\n"; 
			$colNum++;
		}
		$rowNum++;
	}
	?>
	var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, {width: 1600, height: 1000, title: 'Company Performance'});
      }
    </script>
  </head>

  <body>
    <div id="chart_div"></div>
  </body>
</html>
