<?php
ob_start();
function writeObject($obj){
	foreach($obj as $k => $v){
		if(is_numeric($k)){
			echo "<r>";
			writeObject($v);
			echo "</r>";
		}else{	
			echo "<".$k.">";
			if(is_object($v) || is_array($v)){
				writeObject($v);
			}else{
				echo $v;
			}
			echo "</".$k.">";
		}
	}
}
writeObject($data);
$out = ob_get_contents();
ob_end_clean();

$xml = new SimpleXmlElement($out);
$dom = dom_import_simplexml($xml)->ownerDocument;
$dom->formatOutput = true;
echo $dom->saveXML();
//echo $xml->asXML();
?>

