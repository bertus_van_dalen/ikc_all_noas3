<meta property="fb:app_id" content="292111367506193" />
  <?php

  function print_ns_props($prependString, $obj, $baseNs) {
    foreach ($obj as $k => $v) {
      if (is_string($v)) {
        echo '<meta property="' . $baseNs . $prependString . $k . '" content="' . $v . '" />';
      } else {
        $appendToPrependString = $prependString . $k . ':';
        print_ns_props($appendToPrependString, $v, $baseNs);
      }
    }
  }

  print_ns_props(':', $og, 'og');
  if (isset($music)) {
    print_ns_props(':', $music, 'music');
  }
  ?>