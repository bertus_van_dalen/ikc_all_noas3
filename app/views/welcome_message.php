<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>welcome</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

</style>
</head>
<body>
<h1>lijstje</h1>
<ul>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/createTemplateOfSubset">create template of subset</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/createTemplateOfTemplate">create template of template</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/editTemplate">edit or delete template</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/uploadRawAudioStyleZipTest">style zip versie 1 TEST</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/uploadRawAudioStyleZip">style zip versie 1 upload</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/emptyTmpZipDir">emptyTmpZipDir</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/dldStyleClips/styleId/">dldStyleClips/styleID</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/resampleStyleClips/??/">resampleStyleClips/styleID</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>admin/projectConfigurator">project instellingen maken</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>admin/client_has_user_for_moderator///">kijk of een client (arg1) een gebruiker (arg2) als moderator heeft</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>admin/client_moderator///">koppel een client (arg 1 client id) aan een moderator (arg 2 user id)</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>admin/client_moderator///">koppel een client (arg 1 client id) aan een moderator (arg 2 user id)</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>admin/create_agent_permission//">maak agent permissie aan voor gebruiker id (arg1)</a></li>
  <li><a href="<?php echo $ikc->conf->project_url; ?>musbas/createMusicPool/">create music pool</a></li>
</ul>
</body>
</html>
