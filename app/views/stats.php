<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>IkComponeer @AVROv2</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

</style>

<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script type='text/javascript'>
  google.load('visualization', '1', {packages:['table']});
  google.setOnLoadCallback(drawTable);
  function drawTable() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Authenticated users');
    data.addColumn('number', 'Compositions stored');
    data.addColumn('number', 'Compositions published');
    <?php foreach($months as $month => $info): ?>
    	data.addRow(['<?php echo $month; ?>',<?php echo $info->authenticated_users; ?>,<?php echo $info->stored_compositions; ?>,<?php echo $info->published_compositions; ?>]);
    <?php endforeach; ?>	
    
    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, {showRowNumber: false});
  }
</script>

</head>
<body>
<h1>Statistieken voor het gebruik van de componeertool voor het project <?php echo $project; ?></h1>
<div id='table_div'></div>
</body>
</html>
