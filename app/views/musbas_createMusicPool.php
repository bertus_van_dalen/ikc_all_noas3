<html>
  <head>
    <title>ikcomponeer back-end</title>
    <script id="ikc_busy_TPL" type="text/x-jsrender">
      <div>
        <img src="<?php echo $ikc->conf->css_url_shared; ?>images/busy.gif"></img><span> busy, please wait</span>
      </div>
    </script>
    <script type="text/javascript">
      if(typeof(ikc) == "undefined"){
        var ikc = {};
      }
      ikc.conf = <?php echo json_encode($ikc->conf); ?>;
    </script>
    <?php echo $ikc->js; ?>
    <script id="ikc_busy_creating_musicpool_TPL" type="text/x-jsrender">
      <div>
        <img src="<?php echo $ikc->conf->css_url_shared; ?>images/busy.gif"></img><span> busy creating musicpool, please wait</span>
      </div>
    </script>
    <script id="ikc_success_creating_musicpool_TPL" type="text/x-jsrender">
      <div>
        <span>You just created a musicpool with ID {{:id}}. To view it, go here: </span><a href="<?php echo $ikc->conf->project_url; ?>musicPool/get/{{:id}}">click!</a>
      </div>
    </script>
  </head>
  <body>
    <iframe src="<?php echo $ikc->conf->project_url;?>xframe" id="ikcXframe" style="display:none"></iframe>
    <h1>creates musicpool</h1>
    <div id="ikc_workspace">
      directory name to create:
      <form class="ikc-form" id="create_musicpool">
        <input class="ikc-formfield" id="directory_name" type="text"></input>
        <input type="button" value="go" class="ikc-formsubmit"></input>
      </form>
    </div>
  </body>
</html>