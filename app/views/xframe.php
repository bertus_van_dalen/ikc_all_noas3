<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<div id="test">Send me a message!</div>
                <script>
                        var source = null;
                        var prod_locs = [];
<?php
foreach ($prod_locs as $i => $regexp) {
  echo "prod_locs.push(new RegExp('" . $regexp . "', 'i'));";
  echo "prod_locs.push(new RegExp('" . str_replace('http://', 'http://www.', $regexp) . "', 'i'));";
}
?>
			
                        function listener(event) {
                            var forbidden = true;
                            var i = 0;
                            var prodLocRegExp;
                            var numDomains = prod_locs.length;
                            for (i = 0; i < numDomains; i++) {
                              prodLocRegExp = prod_locs[i];
                              if (prodLocRegExp.test(event.origin)) {
                                forbidden = false;
                                break;
                              }
                            }
                            if (forbidden)
                              return
                            var origin = event.origin;
                            var data = JSON.parse(event.data);
                            var ajaxurl = data.ajaxurl;
                            var messageID = data.messageID;
                            var postData = "json="+JSON.stringify(data.postData);
                            var source = event.source;
                            
                            sendRequest(ajaxurl, function(req) {
                              var message = {
                                messageID: messageID,
                                response: typeof(req.response) == "undefined" ? req.responseText : req.response,
                                status: req.status,
                                statusText: req.statusText
                              }
                              source.postMessage(JSON.stringify(message), origin);
                            }, postData);

                          }

                          if (window.addEventListener) {
                            addEventListener("message", listener, false)
                          } else {
                            attachEvent("onmessage", listener)
                          }

                          function sendRequest(url, callback, postData) {
                            var req = createXMLHTTPObject();
                            if (!req)
                              return;
                            var method = (postData) ? "POST" : "GET";
                            req.open(method, url, true);
                            req.withCredentials = "true";
                            //req.setRequestHeader('User-Agent','XMLHTTP/1.0');
                            if (postData)
                              req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                            req.onreadystatechange = function() {
                              if (req.readyState != 4)
                                return;
                              if (req.status != 200 && req.status != 304) {
                                //          alert('HTTP error ' + req.status);
                                return;
                              }
                              callback(req);
                            }
                            if (req.readyState == 4)
                              return;
                            req.send(postData);
                          }

                          var XMLHttpFactories = [
                            function() {
                              return new XMLHttpRequest()
                            },
                            function() {
                              return new ActiveXObject("Msxml2.XMLHTTP")
                            },
                            function() {
                              return new ActiveXObject("Msxml3.XMLHTTP")
                            },
                            function() {
                              return new ActiveXObject("Microsoft.XMLHTTP")
                            }
                          ];

                          function createXMLHTTPObject() {
                            var xmlhttp = false;
                            for (var i = 0; i < XMLHttpFactories.length; i++) {
                              try {
                                xmlhttp = XMLHttpFactories[i]();
                              }
                              catch (e) {
                                continue;
                              }
                              break;
                            }
                            return xmlhttp;
                          }
                </script>
	</body>
</html>