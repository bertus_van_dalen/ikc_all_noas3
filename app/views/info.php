<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>ikcomponeer info</title>
    <iframe src="<?php echo $ikc->conf->project_url;?>xframe" id="ikcXframe" style="display:none"></iframe>
    <link href="<?php echo $ikc->conf->css_url_shared; ?>styles.css" rel="stylesheet" type="text/css">
  </head>
	<body>
		<div id="container">
      <div id="info_header" style="margin: 0 auto; width:960px; height:90px; color:#333333; text-align:center;">
        <br/><h2>informatie ikcomponeer</h2>
      </div>
      <div id="content" style="width:960px;margin:0 auto;">
        <div id="content_info"style="">
          <p>
            Wij nodigen u uit om zich voor de nieuwsbrief aan te melden. U ontvangt twee keer per jaar ikcomponeer-nieuws.
            <form id="mailinglist_form" action="info/nieuwsbrief" method="post">
              <label>voer uw mail adres in: </label>
              <input name="uwMailAdres" type="text"></input>
              <input type="submit" style="cursor: pointer;" value="aanmelden"></input>
            </form>
          </p>
          <p>
            De componeeromgeving van ikcomponeer.nl is in meerdere contexten inzetbaar. Momenteel kunt u hem bijvoorbeeld vinden,
            ingebed in de NTR SchoolTV site voor het programma Muziek Klassiek, waar kinderen met een SchoolTV-account kunnen 
            componeren. Een ander voorbeeld is de online leerlingenportal Kijk Kunst Doe Kunst van het Kunstgebouw Zuid-Holland. 
            De componeertool verschijnt daar sinds 2013 in 5 educatieve programma's.</p>
            
            <p>Toepassingen worden in andere sites ingebed, maar centraal ondersteund en beheerd. Na de inbouw functioneert het dus in een andere site, 
              maar wordt het door ons beheerd en hebben anderen dus geen hosting of beheer infrastructuur nodig.
          </p>
          <p>
            De componeertool is verkrijgbaar via <a href="http://adeptive.nl" target="_blank" >adeptive.nl</a>.
          </p>
          <p>U wordt hierbij uitgenodigd om muziek te (laten) ontwikkelen en als muziekblokjes beschikbaar te maken. 
            Ook kunt u reeksen van oefeningen ontwikkelen voor de doelgroep en de componeeromgeving in uw eigen site toepassen.
          </p>
          <p>
            De componeeromgeving kan voor elke portal anders worden ingesteld als het over 
            grafische vormgeving, muzikale inhoud of authenticatie / inloggen gaat. 
            Ook de functionaliteit kan in hoge mate geconfigureerd worden. 
            Context specifieke inhoud en workflows kunnen kortom worden gefaciliteerd. 
          </p>
          <p>
            De componeertool muziekblokjes zijn CC-gelicenseerd (free-culture). 
            Deze kunnen een meerwaarde bieden voor alle gebruikers van de componeertool. 
            Daarnaast worden ten behoeve van specifieke portals muziekpakketten gebruikt 
            met een beperkt intellectueel eigendom waarbij het formaat uitsluitend een 
            interactieve netwerkuitzending c.q. webcast is. In de praktijk betekent dit,
            dat er dan geen resulterende mp3 bestanden worden uitgegeven.
          </p>
          <p>
            De ontwikkeling van dit project berust voor een aanzienlijk deel op vrijwillige inzet. 
            Daarbij bestaat de bedoeling een innovatieve routekaart uit te voeren. 
            Onderzocht wordt in hoeverre dit via crowd sourcing gefinancierd kan worden.
          </p>
          <p>
            Innovatieve routekaart:</p>
          <div style="width:400px; margin:0 auto; text-align:left;">
            <ul>
              <li>
                diverse optimalisaties (sneller laden, etc)
              </li>
              <li>
                groepspagina's aansluitend bij klassen
              </li>
              <li>
                fasering open-source front-end  
              </li>
              <li>
                verbeterde statistieken
              </li>
              <li>
                internationalisatie
              </li>
              <li>
                mobile app
              </li>
              <li>
                HTML5 versie (Web Audio API)
              </li>
              <li>
                beter gestroomlijnde json back end
              </li>
            </ul>
          </div>
          <p>
            De muziekblokjes zijn opgenomen door musici van het MCO / de SOM naar wie bijzondere 
            dank uitgaat.</p>
          <p>Het project is mogelijk gemaakt door het Muziekcentrum van de Omroep (MCO) c.q. 
            de Stichting Omroep Muziek (SOM) en de Hogeschool voor de Kunsten Utrecht (HKU).
          </p>
          <p>Het muzikale arrangement, het compositorische basismateriaal en de productie 
            zijn verzorgd door Than van Nispen tot Pannerden.</p>
          <p>De mixage is verzorgd door GreenCouch Music and Sound for Media.</p>
          <p>Het interactie ontwerp is verzorgd door Lotte Meijer.</p>
          <p>Het grafisch ontwerp is verzorgd door Sonja van Vuure.</p>
          <p>Het software ontwerp, de ontwikkeling, het ICT-beheer en de 
            exploitatie zijn verzorgd door Bertus van Dalen.</p>
          </p>
        </div>
        <div id="footer">
          <p> &copy; 2009-2014, Stichting Omroep Muziek</p>
        </div>
      </div>
    </div>
	</body>
</html>
