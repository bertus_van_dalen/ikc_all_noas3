
<!DOCTYPE html>

<!--
  Metropole Orkest - 2013 website
  Copyright (C) 2013, Metropole Orkest <www.metropoleorkest.nl>
  Design by Pony Design Club <hello@ponydesignclub.nl>
  Development by Daan Vos de Wael <daan@vosdewael.com>
  Adapted for ikcomponeer.nl by ek.vandalen in 2014
-->

<html lang="nl-NL"><head>

<title><?php echo $ikc->conf->project_title; ?></title>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
<link href="<?php echo $ikc->conf->base_url . $ikc->conf->projectCss; ?>" rel="stylesheet" type="text/css">

<iframe src="<?php echo $ikc->conf->project_url;?>xframe" id="ikcXframe" style="display:none"></iframe>
<script type="text/javascript" src="<?php echo $ikc->conf->project_url;?>jshelpers"></script>
<?php if(isset($janrain_headers)) echo $janrain_headers; ?>
<script type="text/javascript">embedComponeertool( 'content', false ); </script>

<?php if(isset($open_social_headers)) echo $open_social_headers; ?>
</head>
<body id="home">
  <?php if(isset($janrain_headers)): ?><a class="janrainEngage" href="#" style="display: none;">Sign-In</a><?php endif; ?>

<div id="wrapper" style="overflow-x: hidden;">

<div id="header">
  <div class="left"></div>
  <div class="container">
    <p class="hidden-phone">de ikcomponeer site<br>van het <a href="http://www.mo.nl">metropole orkest</a></p>
    <h1><a href="#/ikc_27129" title="Metropole Orkest" rel="home">Metropole Orkest</a></h1>
    <p class="hidden-phone">voor scholen en kinderen<br>vrij te gebruiken</p>
  </div>
  <div class="right"></div>
</div>
<div id="main-nav">
    <div class="logo-small hidden-phone"></div>
    <div class="container"><ul id="menu-main-nav" class="menu">
            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a id="ikcomponeer_mps_ikc_tour_de_france" href="#/ikc_27129">Home</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.mo.nl/educatie/" target="_blank">Educatie</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#/auteursrechtelijk" id="auteursrechtelijk">creative commons</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#/informatie_voor_docenten" id="informatie_voor_docenten">docenten</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#/vraag_en_antwoord" id="vraag_en_antwoord">vraag en antwoord</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#/contactformulier" id="contactformulier">Probleem melden</a></li>
            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#/ikcomponeer_mps_start_help" id="ikcomponeer_mps_start_help">Help video</a></li>
        </ul>
    </div>
  </div>
  <div class="home-mid">
    <span style="left: 973px; top: 10px;" class="navarro" id="cclogo">
      <a rel="license" href="http://creativecommons.org/licenses/by/4.0/deed.nl" target="_blank">
        <img alt="Creative Commons-Licentie" style="border-width:0" src="https://licensebuttons.net/l/by/4.0/88x31.png" />
      </a>
    </span>
    <span style="font-size: 53px; left: -61px; top: 394px;" 
          class="glyphicon glyphicon-chevron-left navarro ikc-shift-left"></span>
    <span style="font-size: 53px; left: 968px; top: 394px;" 
          class="glyphicon glyphicon-chevron-right navarro ikc-shift-right"></span>
    <span style="font-size: 164px; left: -218px; top: 334px;" 
          class="glyphicon glyphicon-chevron-left navarro ikc-shift-left"></span>
    <span style="font-size: 164px; left: 1008px; top: 334px;" 
          class="glyphicon glyphicon-chevron-right navarro ikc-shift-right"></span>
    <span style="font-size: 368px; left: -536px; top: 232px;" 
          class="glyphicon glyphicon-chevron-left navarro ikc-shift-left"></span>
    <span style="font-size: 368px; left: 1106px; top: 219px;" 
          class="glyphicon glyphicon-chevron-right navarro ikc-shift-right"></span>
    <div class="container">
      <div id="content">
        <p>Voor het componeer programma is de nieuwste Flash Player nodig.
          <a href = "http://www.adobe.com/nl/shockwave/download/triggerpages_mmcom/flash.html" target="_blank">Installeer hier de nieuwste Flash Player</a>
        </p>
      </div>
    </div>
  </div>
<div id="push"></div>
</div> <!-- #wrapper -->
<div id="footer">
  <div class="container">
    <div class="row">
      <div class="span4 tweets">
        <h5>MELD EEN PROBLEEM</h5>
        <ul class="tweet-slides">
          <li><p><a href="#/contactformulier">Gebruik het contactformulier</a></p></li>
        </ul>
      </div>

      <div class="span4 tweets">
        <h5>Metropole Educatie</h5>
        <ul>
          <li><a href="http://www.mo.nl/educatie/" target="_blank">meer over Metropole Educatie</a></li>
        </ul>
      </div>

      <div class="span4">
        <h5>inhoud</h5>
        <ul>
          <li><a href="#/vraag_en_antwoord">vraag en antwoord</a></li>
          <li><a href="#/informatie_voor_docenten" >informatie voor docenten</a></li>
          <li><a href="#/auteursrechtelijk" >auteursrechtelijk</a></li>
          <li><a href="#/privacy" >privacy</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-44181605-1', 'mo.nl');ga('send', 'pageview');</script>

</body></html>