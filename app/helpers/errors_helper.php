<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

if (!function_exists('ikc_get_error')) {

  function ikc_get_error($code, $field, $val, $lang) {
    $errors = array(
        "0" => array(
            "en" => "unknown error",
            "nl" => "onbekende fout"
        ),
        "1" => array(
            "en" => "missing field",
            "nl" => "ontbrekend veld"
        ),
        "2" => array(
            "en" => "your username must be at least 8 characters",
            "nl" => "je gebruikersnaam moet minstens 8 letters lang zijn"
        ),
        "3" => array(
            "en" => "your username must be at most 24 characters",
            "nl" => "je gebruikersnaam moet hooguit 24 letters lang zijn"
        ),
        "4" => array(
            "en" => "you need to be logged in",
            "nl" => "je moet wel ingelogd zijn"
        ),
        "5" => array(
            "en" => "this method does not exist",
            "nl" => "deze methode bestaat niet"
        ),
        "6" => array(
            "en" => "missing property 'data'",
            "nl" => "ontbrekende eigenschap 'data'"
        ),
        '7' => array(
            "en" => "data not an array",
            "nl" => "data is geen array"
        ),
        '8' => array(
            "en" => "not booleish",
            "nl" => "niet booleish"
        ),
        '9' => array(
            "en" => "not a string",
            "nl" => "geen string"
        ),
        '10' => array(
            "en" => "results is not an array",
            "nl" => "results is geen array"
        ),
        '11' => array(
            "en" => "permission problem",
            "nl" => "rechten probleem"
        ),
        '12' => array(
            "en" => "inexistent type",
            "nl" => "niet bestaand type"
        ),
        '13' => array(
            "en" => "missing id",
            "nl" => "geen id"
        ),
        "14" => array(
            "en" => "id must be int",
            "nl" => "id moet een int zijn"
        ),
        "15" => array(
            "en" => "object needs to exist in the database",
            "nl" => "het object moet al bestaan in de database"
        ),
        "16" => array(
            "en" => "problem with the trash can",
            "nl" => "probleem met de prullenbak"
        ),
        "17" => array(
            "en" => "problem with the unread method",
            "nl" => "probleem met de unread methode"
        ),
        "18" => array(
            "en" => "must be numerical integer value",
            "nl" => "moet numerieke integer waarde zijn"
        ),
        "19" => array(
            "en" => "does not share the same style",
            "nl" => "heeft niet dezelfde stijl"
        ),
        "20" => array(
            "en" => "no style is coupled to this part",
            "nl" => "deze instrumentgroep heeft geen stijl"
        ),
        "21" => array(
            "en" => "clip does not have a part",
            "nl" => "clip heeft geen instrumentgroep"
        ),
        "22" => array(
            "en" => "clips do not share a part",
            "nl" => "clips hebben niet dezelfde instrumentgroep"
        ),
        "23" => array(
            "en" => "file does not exist",
            "nl" => "bestand bestaat niet"
        )
    );
    if (!(array_key_exists($code, $errors))) {
      log_message("error", "unknown error: " . $code);
      $code = "0";
    }
    $e = new stdClass();
    $e->message = $errors[$code][$lang];
    $e->field = $field;
    $e->val = $val;
    return $e;
  }

}