<?php

if (!defined('BASEPATH')){
  exit('No direct script access allowed');
}

function fs_remove_separator($path){
  if(strripos($path, DIRECTORY_SEPARATOR) === strlen($path) - 1){
    $path = substr($path, 0, strlen($path) - 1);
  }
  return $path;
}

function fs_create_dir($path, $mode){
  if(false === mkdir($path, $mode, true)){
    log_message("error","could not create " . $path);
    return false;
  }else{
    log_message("debug","created dir " . $path);
    return true;
  }
}

function fs_is_writable($path){
  if(!is_writable($path)){
    log_message("error","fs_is_writable " . $path . " is not writable");
    return false;
  }
  return true; 
}

function fs_touchable($path){
  if(false === touch($path)){
    log_message("error","fs_touchable could not touch " . $path);
    return false;
  }
  return true;
}

function fs_create_or_touch_inexistent($path, $dir){
  if($dir){
    return fs_create_dir($path, 0755);
  }else{
    return fs_touchable($path);
  }
}

function fs_is_existent_writable($path){
  if(is_writable($path)){
    return true;
  }else{
    log_message("error", "fs_is_existent_writable unwritable destination " . $path);
    return false;
  }
}

function fs_copyable($source, $dest){
  if(!file_exists($source)){
    log_message("error","fs_copyable " . $source . " doest not exist");
    return false;
  }
  if(!is_readable($source)){
    log_message("error","fs_copyable " . $source . " is not readable");
    return false;
  }
  if(!file_exists($dest)){
    return fs_create_or_touch_inexistent($dest, is_dir($source));
  }else{
    return fs_is_existent_writable($dest);
  }
}

function fs_copy_all_dir($source, $dest){
  foreach (
    $iterator = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS),
      RecursiveIteratorIterator::SELF_FIRST) as $item
    ){
    $cp = $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName();
    if ($item->isDir()) {
      mkdir($cp);
      log_message("debug","created dir " . $cp);
    } else {
      fs_copy_file($item, $cp);
    }
  }
}

function fs_copy_file($source, $dest){
  if(false === copy($source, $dest)){
    log_message("error","fs_copy_file could not copy " . $source . " to " . $dest);
    return false;
  }
  log_message("debug", "copied file " . $dest);
  return true;
}

function fs_copy_all($source, $dest){
  $source = fs_remove_separator($source);
  $dest = fs_remove_separator($dest);
  if(false === fs_copyable($source, $dest)) { return false; }
  return is_dir($source) ? fs_copy_all_dir($source, $dest) : fs_copy_file($source, $dest);
}

function fs_rmdir($p){
  if(false === rmdir($p)){
    log_message("error","fs_rmdir could not remove root dir " . $p);
  }else{
    log_message("debug","fs_rmdir removed dir " . $p);
  }
}

function fs_unlink($p){
  if(false === unlink($p)){
    log_message("error","fs_unlink could not remove file " . $p);
  }else{
    log_message("debug","fs_unlink removed file " . $p);
  }
}

function fs_remove_by_iterator($p){
  $pn = $p->getPathname();
  if($p->isDir()){
    fs_rmdir($pn);
  }else{
    fs_unlink($pn);
  }
}

function fs_delete_all($path){
  $dir_iterator = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
  $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::CHILD_FIRST);
  foreach($iterator as $p) {
    fs_remove_by_iterator($p);
  }
}

