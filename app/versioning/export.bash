#!/bin/bash

# clear old file
touch output.sql
rm output.sql

# Database credentials
 user="ikc_p"
 password="duh39EwepaswA5uxAy6crakeWAs8xUMA"
 host="localhost"
 db="ikc_p"
 cred="--user=$user --password=$password --host=$host"

# Dump database into SQL file
 mysqldump -d $cred $db >> output.sql

# export from to
 tables="project client playlist projectalias domain"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="style cclimit ruleset rule"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="movement part clip"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="subset subpart subclip"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="noshare"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="colorseries color"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="template"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="scrollitem"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="composition"
 where="--where=\"id IN (SELECT composition_id FROM template)\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="role rolefor"
 mysqldump --no-create-info $cred $db $tables  >> output.sql
 tables="user"
 where="--where=\"id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="profile"
 where="--where=\"id IN(SELECT DISTINCT profile_id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovoidgoogle"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovcliavro"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovclikgb"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovclintr"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovoidanon"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovoidfacebook"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovoidhyves"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovoidlinkedin"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovoidmicrosoftaccount"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovoidtwitter"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql
 tables="userprovoidwindowslive"
 where="--where=\"user_id IN(SELECT DISTINCT id FROM user WHERE id IN(SELECT DISTINCT user_id FROM composition WHERE id IN(SELECT composition_id FROM template)))\""
 eval mysqldump --lock-tables=false --no-create-info $cred $where $db $tables >> output.sql

#koppeltabellen en andere dependante tabellen
 tables="clip_movement conf conf_project conf_template domain_project movement_style playlist_project role_user rule_ruleset"
 mysqldump --no-create-info $cred $db $tables  >> output.sql


