# Dump database structure (not the data) from specified tables
# so that tables referenced in constraints will be present
# (otherwise all tables will be dumped in alphabetic order)


tables="client"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# clientmoderatorship depends on client
# clientmoderated depends on client and clientmoderatorship
tables="clientmoderatorship clientmoderated"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# project depends on domain
# domain_project depends on domain and project
tables="domain project domain_project"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# user depends on profile
tables="profile user"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# client_user depends on client and user
tables="client_user"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# rule_ruleset depends on rule and ruleset
tables="rule ruleset rule_ruleset"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# rolefor depends on role, user and client
tables="role rolefor"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# role_user depends on role and user
tables="role_user"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# style depends on ruleset
tables="style"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# subset depends on style
tables="subset"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# noshare depends on subset
tables="noshare"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# template and composition depend on each other
# todo - unidirectional dependency between template and composition
# template depends on subset
# composition depends on user, style and project
tables="template composition"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# scrollitem depends on template and project
tables="scrollitem"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# cclimit depends on style and project
tables="cclimit"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# projectalias depends on project
tables="projectalias"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# playlist depends on project
tables="playlist"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# playlist_project depends on playlist and project
tables="playlist_project"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# composition_playlist depends on composition and playlist
tables="composition_playlist"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# conf_project depends on conf and project
tables="conf conf_project"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# autosave depend on composition
tables="autosave"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# achievement depends on composition
# and achievement_user depends on achievement and user
tables="achievement achievement_user"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# session depends on token and user
tables="token session"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# part is deprecated legacy - no idea what it still does any more
tables="part"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# subpart depends on part and subset
tables="subpart"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# clip depends on part
# subclip depends on clip and subpart
tables="clip subclip"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# movement_style depends on movement and style
# clip_movement depends on clip and movement
tables="movement movement_style clip_movement"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# all the id provider tables depend on user
tables="userprovclikgb userprovcliavro userprovclintr userprovoidgoogle userprovoidhyves userprovoidfacebook userprovoidmicrosoftaccount userprovoidlinkedin userprovoidanon userprovoidwindowslive userprovoidtwitter"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# color depends on colorseries
tables="colorseries color"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# nieuwsbriefadres and img are independent overall-use tables
tables="nieuwsbriefadres img"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

tables="client"
mysqldump --no-create-info $cred $db $tables >> $outfile

# clientmoderatorship depends on client
# clientmoderated depends on client and clientmoderatorship
tables="clientmoderatorship clientmoderated"
mysqldump --no-create-info $cred $db $tables >> $outfile

# project depends on domain
# domain_project depends on domain and project
tables="domain project domain_project"
mysqldump --no-create-info $cred $db $tables >> $outfile

# user depends on profile
tables="profile user"
mysqldump --no-create-info $cred $db $tables >> $outfile

# client_user depends on client and user
tables="client_user"
mysqldump --no-create-info $cred $db $tables >> $outfile

# rule_ruleset depends on rule and ruleset
tables="rule ruleset rule_ruleset"
mysqldump --no-create-info $cred $db $tables >> $outfile

# rolefor depends on role, user and client
tables="role rolefor"
mysqldump --no-create-info $cred $db $tables >> $outfile

# role_user depends on role and user
tables="role_user"
mysqldump --no-create-info $cred $db $tables >> $outfile

# style depends on ruleset
tables="style"
mysqldump --no-create-info $cred $db $tables >> $outfile

# subset depends on style
tables="subset"
mysqldump --no-create-info $cred $db $tables >> $outfile

# noshare depends on subset
tables="noshare"
mysqldump --no-create-info $cred $db $tables >> $outfile

# template and composition depend on each other
# todo - unidirectional dependency between template and composition
# template depends on subset
# composition depends on user, style and project
tables="template composition"
mysqldump --no-create-info $cred $db $tables >> $outfile

# conf_template depends on conf and template
tables="conf_template"
mysqldump -d --add-drop-table=FALSE $cred $db $tables >> $outfile

# scrollitem depends on template and project
tables="scrollitem"
mysqldump --no-create-info $cred $db $tables >> $outfile

# cclimit depends on style and project
tables="cclimit"
mysqldump --no-create-info $cred $db $tables >> $outfile

# projectalias depends on project
tables="projectalias"
mysqldump --no-create-info $cred $db $tables >> $outfile

# playlist depends on project
tables="playlist"
mysqldump --no-create-info $cred $db $tables >> $outfile

# playlist_project depends on playlist and project
tables="playlist_project"
mysqldump --no-create-info $cred $db $tables >> $outfile

# composition_playlist depends on composition and playlist
tables="composition_playlist"
mysqldump --no-create-info $cred $db $tables >> $outfile

# conf_project depends on conf and project
tables="conf conf_project"
mysqldump --no-create-info $cred $db $tables >> $outfile

# autosave depend on composition
tables="autosave"
mysqldump --no-create-info $cred $db $tables >> $outfile

# achievement depends on composition
# and achievement_user depends on achievement and user
tables="achievement achievement_user"
mysqldump --no-create-info $cred $db $tables >> $outfile

# session depends on token and user
tables="token session"
mysqldump --no-create-info $cred $db $tables >> $outfile

# part is legacy, no idea what it still does nowadays
tables="part"
mysqldump --no-create-info $cred $db $tables >> $outfile

# subpart depends on part and subset
tables="subpart"
mysqldump --no-create-info $cred $db $tables >> $outfile

# clip depends on part
# subclip depends on clip and subpart
tables="clip subclip"
mysqldump --no-create-info $cred $db $tables >> $outfile

# movement_style depends on movement and style
# clip_movement depends on clip and movement
tables="movement movement_style clip_movement"
mysqldump --no-create-info $cred $db $tables >> $outfile

# all the id provider tables depend on user
tables="userprovclikgb userprovcliavro userprovclintr userprovoidgoogle userprovoidhyves userprovoidfacebook userprovoidmicrosoftaccount userprovoidlinkedin userprovoidanon userprovoidwindowslive userprovoidtwitter"
mysqldump --no-create-info $cred $db $tables >> $outfile

# color depends on colorseries
tables="colorseries color"
mysqldump --no-create-info $cred $db $tables >> $outfile

# nieuwsbriefadres and img are independent overall-use tables
tables="nieuwsbriefadres img"
mysqldump --no-create-info $cred $db $tables >> $outfile

# conf_template depends on conf and template
tables="conf_template"
mysqldump --no-create-info $cred $db $tables >> $outfile