<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {
  public function hour(){
    $this->load->library('rb');
    $t = time();
    $hr = $t - 4500; // 15 min overlap
    $agentRole = R::find("role"," name = ?",array("agent"));
    $agentUsers = $agentRole->sharedUser;
    $where;
    $whereVals = array($hr);
    $firstWhere = true;
    foreach($agentUsers as $id => $user){
      if($firstWhere){
        $firstWhere = false;
        $where = " user_id = ?";
      }else{
        $where .= " OR user_id = ?";
      }
      $whereVals[] = $id;
    }
    $sessions = R::find("session"," mtime > ? AND (" . $where . ")", $whereVals);
    $tooOld = array();
    foreach($sessions as $id => $session){
      if($session->mtime < $t - 900){
        $tooOld[] = $session;
      }
    }
    $viewData = array("tooOld" => $tooOld);
    if(count($tooOld) > 0){
      $body = $this->load->view("mails/admin/agent_fallout", $viewData, true);
      $this->load->library('email');
      $config['mailtype'] = 'html';
      $this->email->initialize($config);
      $this->email->from('noreply@adeptive.nl');
      $this->email->to($this->config->item('notification_recip_agent_fallout'));
      $this->email->subject('!!! ikcomponeer agent session fallout');
      $this->email->message($body);
      $this->email->send();
    }
  }
}
?>
