<?php

class Form extends CI_Controller {

  private $formID = "null";

  public function __construct() {
    parent::__construct();
  }

  public function open($formID) {
    $this->load->helper('form');
    if ($formID) {
      $firstUnderscorePos = strpos($formID, "_");
      $modelname = substr($formID, 0, $firstUnderscorePos);
      log_message("debug","looking for model " . $modelname);
      $this->load->model($modelname);
      log_message("debug","loaded model " . $modelname);
      $methodname = substr($formID, $firstUnderscorePos + 1);
      log_message("debug","looking for method " . $methodname);
      if (!($this->$modelname->hasMethod($methodname, &$this->result))) {
        log_message("debug", $modelname . " has no method " . $methodname);
        exit;
      }
      log_message("debug", $modelname . " will execute method " . $methodname);
      $formContents = $this->$modelname->$methodname();
    }else{
      log_message("debug","no formID parameter in url");
      exit;
    }
    echo form_open($this->config->item("project_url") . 'form/submit/' . $formID);
    echo $formContents;
    echo form_close();
  }
  
  /*public function index($formID = "") {
    $this->result = array(
        "data" => $_POST,
        "formID" => $formID,
        "status" => "error",
        "results" => array(),
        "errors" => array(),
        "project_url" => $this->config->item("project_url")
    );
    $urlData = $this->uri->ruri_to_assoc(4);
    $this->result['data'] = $this->result['data'] + $urlData;
    if ($formID) {
      $firstUnderscorePos = strpos($formID, "_");
      $modelname = substr($formID, 0, $firstUnderscorePos);
      $this->load->model($modelname);
      $methodname = substr($formID, $firstUnderscorePos + 1);
      if (!($this->$modelname->hasMethod($methodname, &$this->result))) {
        return $this->_out();
      }
      $this->$modelname->$methodname(&$this->result);
    }
    $this->load->view("form", $this->result);
  }*/

  private $result;

  private function setHeaders() {
    $this->output->set_header("Access-Control-Allow-Origin: " . $this->input->get_request_header('Origin', TRUE));
    $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
    $this->output->set_header("Access-Control-Allow-Credentials: true");
    $this->output->set_status_header('200');
    $this->output->set_header("Content-Type: text/plain charset=UTF-8");
  }

  private function _out() {
    $this->output->append_output(json_encode($this->result));
  }

  public function submit($formID) {
    $this->load->helper("form");
    $this->result = array(
        "data" => $_POST,
        "formID" => $formID,
        "status" => "error",
        "results" => array(),
        "errors" => array()
    );
    
    $firstUnderscorePos = strpos($formID, "_");
    $modelname = substr($formID, 0, $firstUnderscorePos);
    log_message("debug","looking for model " . $modelname);
    $this->load->model($modelname);
    log_message("debug","loaded model " . $modelname);
    $methodname = substr($formID, $firstUnderscorePos + 1);
    log_message("debug","looking for method " . $methodname);
    if (!($this->$modelname->hasMethod($methodname, &$this->result))) {
      log_message("debug","method does not exist: " . $methodname);
      return $this->_out();
    }
    log_message("debug","will now submit to " . $modelname . "->" . $methodname);
    $f = $this->$modelname->$methodname(&$this->result);
    if($f){
      echo form_open($this->config->item("project_url") . 'form/submit/' . $formID);
      echo $f;
      echo form_close();
      return;
    }else{
      $this->setHeaders();
      return $this->_out();
    }
  }
}

?>