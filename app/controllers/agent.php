<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Agent extends CI_Controller {

  private $_TIME_TO_FINISH = 600; // ten minutes
  private $_MAX_SLEEP_TIMES = 5;
  private $_dir;
  private $_metaFile;
  private $_metaFileLocked;

  function __construct() {
    parent::__construct();
    $this->_dir = $this->config->item("published_dir");
    $this->_metaFile = $this->_dir . "meta";
    $this->_metaFileLocked = $this->_dir . ".meta";
    $this->load->model("Session");
  }
  
  private function _isLimited($cpBean){
    $limited = true;
    // if it is cclimited it must have at least one record with mix = true
    // and this record must apply to this project
    $cclimits = $cpBean->template->subset->style->ownCclimit;
    if(count($cclimits)){
      foreach($cclimits as $cclimit){
        if($cclimit->mix == true && $cclimit->project->id == $this->Session->projectId()){
          $limited = false;
          break;
        }
      }
    }else{
      $limited = false;
    }
    return $limited;
  }
  
  // DEPRECATE when optimized agent works
  public function getTask() {
    if (!$this->Session->userHasRole($this->Session->ROLE_NAME_AGENT)) {
      $message = "permission problem";
      show_error($message);
      return;
    }
    log_message('debug','getTask, loading meta file...');
    $meta = $this->getMeta();
    $t = time();
    $task = null;
    $queue = R::find("composition", " published = TRUE AND hasmp3 = FALSE ORDER BY mtime DESC");
    $queueHas = false;
    foreach ($queue as $id => $cp) {
      if($this->_isLimited($cp)){
        continue;
      }
      log_message('debug','in queue: ' . $id);
      $queueHas = true;
      if (array_key_exists($id, $meta)) { // if it was already assigned
        log_message('debug','already assigned!');
        $taskT = $meta[$id]["t"]; // look at the time it was assigned
        $giveItSomeTime = ($taskT + $this->_TIME_TO_FINISH) - $t;
        log_message("debug","time to go: " . $giveItSomeTime);
        if ($giveItSomeTime < 0) { // and if assignment takes too long
          log_message('debug','time to finish exceeded, reassigning...');
          $meta[$id]["t"] = $t; // refresh the timestamp
          $meta[$id]["s"] = session_id(); // and assign to this session
          $task = $id;
          break;
        }
        continue;
      } else { // this task was not yet assigned, assign
        log_message('debug','not yet assigned, assigning...');
        $meta[$id] = Array();
        $meta[$id]["t"] = $t;
        $meta[$id]["s"] = session_id();
        $task = $id;
        break;
      }
    }
    if (!$queueHas) { // quickly (and dirtily) entirely clean up the meta data
      foreach ($meta as $id => $info) {
        unset($meta[$id]);
      }
    }
    $this->_writeMeta($meta);
    echo $task ? $task : 0;
  }

  public function submit($id, $hasMP3 = null) {
    log_message("debug","SUBMIT");
    $message = "ok";
    log_message("debug", "agent->submit(" . $id . ")");
    if (!$id) {
      $message = "no id";
      log_message("debug", $message);
      show_error($message);
      return;
    }
    log_message("debug", "id OK");
    /*if (!$this->Session->userHasRole($this->Session->ROLE_NAME_AGENT)) {
      log_message("debug", "permission problem");
      $message = "permission problem";
      show_error($message);
      return;
    }*/
    log_message("debug", "user role OK");
    $this->load->model("Mcomposition");
    if (!$this->Mcomposition->mem_load($id)) {
      log_message("error", "agent->submit - composition cannot be loaded: " . $id);
      $message = "composition cannot be loaded";
      show_error($message);
      return;
    }
    log_message("debug", "comp mem load OK");
    
    if ($this->Mcomposition->mem_hasmp3()) {
      $message = "agent->submit - composition already has property hasmp3 = true: " . $id;
      log_message("debug", $message);
      show_error($message);
      return;
    }
    log_message("debug", "mp3 state OK");
    $fn = $this->config->item('published_dir') . $id . ".MP3";
    if ($hasMP3 == "0") {
      log_message("debug", "empty composition, set hasmp3 to true to avoid further mixdown attempts and return");
      $this->Mcomposition->mem_set_hasmp3(true);
      $this->Mcomposition->mem_set_published(false);
      $this->Mcomposition->mem_store($id);
      echo $id;
      return;
    }
    if (false === $data = file_get_contents('php://input')) {
      $message = "cannot retrieve data from post";
      log_message("debug", $message);
      show_error($message);
      return;
    }
    log_message("debug", "post data OK");
    $fh;
    if (false === $fh = fopen($fn, 'w')) {
      $message = "problem creating write handler " . $fn;
      log_message("debug", $message);
      show_error($message);
      return;
    }
    log_message("debug", "file write handler OK");
    if (false === fwrite($fh, $data)) {
      $message = "problem writing to " . $fn;
      log_message("debug", $message);
      show_error($message);
      return;
    }
    log_message("debug", "data written OK");
    if (false === fclose($fh)) {
      $message = "cannot close handler for " . $fn;
      log_message("debug", $message);
      show_error($message);
      return;
    }
    log_message("debug", "close write handler OK");
    
    chmod($fn, 0644);

    $this->Mcomposition->mem_set_hasmp3(true);
    $this->Mcomposition->mem_set_pubtime();
    $this->Mcomposition->mem_store($id);
    $meta = $this->_readMeta();
    if (array_key_exists($id, $meta)) {
      unset($meta[$id]);
    }
    $this->_writeMeta($meta);
    
    echo $id;
  }

  private function getMeta() {
    $meta;
    $maxSleep = $this->_MAX_SLEEP_TIMES;
    $count = 0;
    log_message('debug','meta file locked?');
    while (false === $meta = $this->_readMeta()) {
      log_message('debug','seems to be locked, script will sleep 1 second');
      if ($count == $maxSleep) {
        log_message("error", "agent->getTask max sleep time reached... moving back meta file");
        $meta = $this->_readMeta(true);
        break;
      }
      $count++;
      sleep(1);
    }
    log_message('debug','successfully opened meta data, returning...');
    return $meta;
  }

  private function _readMeta($ignoreLock = false) {
    log_message('debug','agent.readMeta (ignoreLock=' . ($ignoreLock ? 'true' : 'false') . ')');
    log_message('debug','_metaFile = ' . $this->_metaFile);
    log_message('debug','_metaFileLocked = ' . $this->_metaFileLocked);
    if (!file_exists($this->_metaFile) && !file_exists($this->_metaFileLocked)) {
      log_message('debug','both do not exist... touching _metaFileLocked and returning new empty array...');
      touch($this->_metaFileLocked);
      $meta = Array();
      return $meta;
    } else if (file_exists($this->_metaFile)) {
      log_message('debug','not locked... load, move and unserialize ' . $this->_metaFile);
      $metaContents = file_get_contents($this->_metaFile);
      rename($this->_metaFile, $this->_metaFileLocked);
      log_message('debug','moved to ' . $this->_metaFileLocked);
      $meta = unserialize($metaContents);
      if(!is_array($meta)){
        log_message('error','meta file is corrupted, returning new empty array...');
        touch($this->_metaFileLocked);
        $meta = Array();
        return $meta;
      }
      log_message('debug','unserialized, returning');
      return $meta;
    } else {
      if ($ignoreLock) {
        log_message('debug','ignoring lock, loading and unserializing');
        $metaContents = file_get_contents($this->_metaFileLocked);
        log_message('debug','loaded ' . $this->_metaFileLocked);
        $meta = unserialize($metaContents);
        log_message('debug','unserialized, returning');
        return $meta;
      }
      return false;
    }
  }

  private function _writeMeta($data) {
    log_message('debug','agent->_writeMeta(data)');
    $contents = serialize($data);
    log_message('debug','serialized');
    file_put_contents($this->_metaFileLocked, $contents);
    log_message('debug','stored to ' . $this->_metaFileLocked);
    rename($this->_metaFileLocked, $this->_metaFile);
    log_message('debug','moved ' . $this->_metaFileLocked . ' to ' . $this->_metaFile);
  }

  // daarna ga je zorgen dat de mp3 wordt afgemixd
  // en als ie klaar is dat er een nieuwe geopend wordt
  // joe moet ook nog controleren of het wel opgepikt wordt door de playlist update
  // enneh... de errors moetn wel gelogd worden in AGENT, sowieso eens denken hoe die logt
  // wat doen we als iemand unpublisht??? dat gebeurt met de write properties functie
  // daar moet ie published EN hasmp3 op nul zetten alsmede de mp3 file weghalen
  // ook is het nog eens zo... er moet in de meta file een entry zijn voor de laatste keer dat we iets hoorden van een bepaalde sessie
  // en dan moet een cron job een mailtje sturen elke keer als er een sessie uitvalt
}

?>
