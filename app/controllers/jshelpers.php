<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Jshelpers extends CI_Controller {

  public function index() {
    $this->load->model("Session");
    $this->load->model("fcf/Carabinerwrapper");
    $this->load->model("conf");
    $data = new stdClass();
    $data->ikc = new stdClass();
    $data->ikc->s = $this->Session->getStatus();
    //$data->ikc->playlist = $playlist;
    $data->ikc->conf = $this->conf->get();
    $data->ikc->js = $this->Carabinerwrapper->jsTagsForModule("embed");

    $this->output->set_content_type('text/javascript');
    $this->load->view('embed', $data);
  }
  
  public function playlist() {
    $fl = $this->config->item("cache_dir") . "playlist/playlist.json";
    $this->output->set_content_type('application/json');
    $this->output->set_header('Cache-Control: no-cache, must-revalidate');
    $this->output->set_header('Expires: ' . date('r', time() + 60));
    if(!file_exists($fl)){
      log_message('error','no playlist file. Maybe you should configure a playlist for the project in the DB');
      $output = json_encode(array());
    }else{
      $output = file_get_contents($fl);
    }
    $this->output->set_output($output);
  }

}
