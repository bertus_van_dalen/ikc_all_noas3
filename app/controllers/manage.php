<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends CI_Controller {
  private $_no_db = false;
  function __construct() {
    parent::__construct();
    $this->load->model("Session");
  }
  public function index(){
    $vm = new stdClass();

    // load carabiner to handle javascripts
    $this->load->library('carabiner');
    $carabiner_config = $this->config->item("carabiner_bundle_for_manage_controller");
    $this->carabiner->config($carabiner_config);
    
    // sort files alphabetically
    require_once(APPPATH . 'libraries/ExampleSortedIterator.php');
    
    // add all stylesheets to carabiner
    $styleDir = $carabiner_config["style_dir"];
    if(is_dir($styleDir)){
      $dit = new RecursiveDirectoryIterator($styleDir);
      $rit = new RecursiveIteratorIterator($dit);
      $sit = new ExampleSortedIterator($rit);
      foreach ($sit as $file) {
          $filename = $file->getPathname();
          if(strripos($filename, '.css') != strlen($filename) - 4) continue;
          $withoutPath = substr($filename, strlen($styleDir));  
          $this->carabiner->css($withoutPath);
      }
    }else{
      die("no dir " . $styleDir);
    }
    
    // add all javascripts to carabiner
    $scriptDir = $carabiner_config["script_dir"];
    if(is_dir($scriptDir)){
      $dit = new RecursiveDirectoryIterator($scriptDir);
      $rit = new RecursiveIteratorIterator($dit);
      $sit = new ExampleSortedIterator($rit);
      foreach ($sit as $file) {
        $filename = $file->getPathname();
        if(strripos($filename, '.js') != strlen($filename) - 3) continue;
        $withoutPath = substr($filename, strlen($scriptDir));  
        $this->carabiner->js($withoutPath);
      }
    }else{
      die("no dir " . $scriptDir);
    }
    
    // add carabiner to viewmodel
    $vm->carabiner = $this->carabiner;
    
    // add conf to viewmodel (in the fashion used in other viewmodels)
    //$this->load->model("Conf");
    $vm->ikc = new stdClass();
    $vm->ikc->conf = new stdClass();
    $vm->ikc->conf->angular_templates = $this->config->item('base_url') . 'ng/angular_templates/';
    $vm->ikc->conf->db_conf_exists = is_object($this->config->item("db"));
    $vm->ikc->conf->base_url = $this->config->item("base_url");
    $vm->ikc->conf->clips_url = $this->config->item("clips_url");
    $vm->ikc->conf->project_url = $this->config->item("project_url");
    $vm->extra_scripts = array($this->config->item('js_dir_rel') . 'ikcomponeer.js');
    $this->load->view("manage", $vm);
  }
  public function exportDevEnv(){
    $this->load->model("Dev_env");
    $arg = null;
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
      $arg = $_POST;
    }
    echo json_encode($this->Dev_env->GetAllExports($arg));
  }
  public function install(){
    $this->load->model("install");
    echo json_encode($this->install->Status($_SERVER['REQUEST_METHOD'] == 'POST' ? $_POST : null));
  }
  public function installDirs(){
    $this->load->model("install");
    echo json_encode($this->install->installDirs(array($_POST['dir'])));
  }
  public function startExportAsync($name){
    $this->load->model("Dev_env");
    $this->Dev_env->StartExport($name);
  }
  public function rpc(){
    $model = filter_input(INPUT_POST, "model");
    $command = filter_input(INPUT_POST, "command");
    $args = isset($_POST["args"]) ? $_POST["args"] : null;
    $this->load->model($model);
    echo json_encode($this->$model->$command($args));
  }
  public function clipUpload($subclipId, $subsetId){
    $this->load->model("style");
    $this->style->uploadSubclip($subclipId, $_FILES['file']['tmp_name'], $_FILES['file']['name'], $_FILES['file']['type'], $subsetId);
  }
}