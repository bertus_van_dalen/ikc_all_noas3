<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xframe extends CI_Controller { 
	public function index()
	{
    $this->load->model("session");
    $data = array("prod_locs" => $this->session->projectProdLocs());
    $this->load->view('xframe', $data);
	}
}
