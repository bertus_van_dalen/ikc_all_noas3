<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Img extends CI_Controller {

  function __construct() {
    parent::__construct();
  }
  function index(){
    echo "hi";
  }
  function get(){
    
    $segments = array_values($this->uri->rsegment_array());
    $segCount = count($segments);
    if(count($segments) < 6){
      log_message("error","img->get number of segments is too low - " . $segCount);
      show_error("argument error in img->get - missing arguments");
    }
    array_shift($segments); // controller
    array_shift($segments); // method
    $client = array_shift($segments); // client id
    $width = array_shift($segments); // width or 0
    $height = array_shift($segments); // height or 0
    $path = "";
    while(count($segments)){
      $path .= '/' . array_shift($segments);
    }
    
    if(!is_numeric($client)){
      log_message("debug","img->get client not numeric: " . $width);
      show_error("client not numeric: " . $width);
    }
    if(!is_numeric($width)){
      log_message("debug","img->get width not numeric: " . $width);
      show_error("width not numeric: " . $width);
    }
    if(!is_numeric($height)){
      log_message("debug","img->get height not numeric: " . $height);
      show_error("height not numeric: " . $height);
    }
    $widthFL = (float)$width;
    $heightFL = (float)$height;
    if($widthFL < 0){
      log_message("debug","img->get width must be zero or more: " . $widthFL);
      show_error("img->get width must be zero or more: " . $widthFL);
    }
    if($heightFL < 0){
      log_message("debug","img->get height must be zero or more: " . $heightFL);
      show_error("img->get height must be zero or more: " . $heightFL);
    }
    // GET THE FILE LOCATION
    $this->load->model("mImg");
    $url = $this->mImg->get($client, $widthFL, $heightFL, $path);
    // REDIRECT
    $this->load->helper('url');
    redirect($url, 'location', 301);
  }
}