<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model("Session");    
    if(!($this->isModerator())) die("must be moderator for this client");
  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
    $this->load->model("Conf");
    $data = new stdClass();
    $data->ikc = new stdClass();
    $data->ikc->conf = $this->Conf->get();
		$this->load->view('welcome_message',$data);
	}
  public function st(){
    $this->load->library("rb");
      $clip = R::load("clip",229);
      $mv = $clip->sharedMovement;
      print_r($mv[0]->id);
  }
  private function isModerator(){
    if(false === ($mod = $this->Session->getUserAsModerator())){
      return false;
    }
    return true;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
