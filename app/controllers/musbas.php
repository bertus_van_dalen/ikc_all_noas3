<?php

class Musbas extends CI_Controller {
  
  function __construct() {
    parent::__construct();
    $this->load->model("Session");
    if(false === $this->Session->getUserAsModerator()){
      exit("no moderator");
    }
  }
  /**
   * A template can have a parent. A composition that has a template 
   * can also have settings. A composition that has a template that has 
   * a parent template will load that parent's composition. This allows
   * for the composition to have its own settings. The composition thus is 
   * useless except that its settings will be loaded.
   */
  function createTemplateOfTemplate($id = null){
    show_error("broken deprecated");
    if(!$id){
      $this->load->model('style');
      $this->load->model('conf');
      $this->load->model('fcf/Carabinerwrapper');
      $data = new stdClass();
      $data->ikc = new stdClass();
      $data->ikc->templates = $this->style->getTemplates();
      $data->ikc->conf = $this->conf->get();
      $data->ikc->js = $this->Carabinerwrapper->jsTagsForModule('musbas');
      $this->load->library('table');
      $this->load->view('musbas_createTemplateForTemplate_choose',$data);
    }else{
      $this->load->model('style');
      echo json_encode($this->style->createTemplateForTemplate($id));
    }
  }
  function deleteSubset($id){
    $this->load->model("style");
    echo json_encode($this->style->deleteSubset($id));
  }
  function getColorSeries(){
    $series = R::findAll("colorseries");
    echo json_encode(R::exportAll($series));
  }
  function createMusicPool(){
    $this->load->model('conf');
    $this->load->model('fcf/Carabinerwrapper');
    $data = new stdClass();
    $data->ikc = new stdClass();
    $data->ikc->conf = $this->conf->get();
    $data->ikc->js = $this->Carabinerwrapper->jsTagsForModule('musbas');
    $this->load->view('musbas_createMusicPool', $data);
  }
  function createMusicPoolForDir(){
    $data = $this->_getJson();
    if(!isset($data->directory_name)){
      die("directory_name not set");
    }
    else{
      die("so far so good");
    }
    $this->load->model('conf');
    $this->load->model('fcf/Carabinerwrapper');
    $data = new stdClass();
    $data->ikc = new stdClass();
    $data->ikc->conf = $this->conf->get();
    $data->ikc->js = $this->Carabinerwrapper->jsTagsForModule('musbas');
    $this->load->view('musbas_createMusicPoolSuccess', $data);
  }
  function createTemplateOfSubsetForId($subset_id){
    $this->load->model('style');
    echo json_encode($this->style->createTemplate($subset_id));
  }
  function editTemplate($id = null){
    show_error("broken deprecated");
    if(!$id){
      $this->load->model('style');
      $this->load->model('conf');
      $this->load->model('fcf/Carabinerwrapper');
      $data = new stdClass();
      $data->ikc = new stdClass();
      $data->ikc->templates = $this->style->getTemplates();
      $data->ikc->conf = $this->conf->get();
      $data->ikc->js = $this->Carabinerwrapper->jsTagsForModule('musbas');
      $this->load->library('table');
      $this->load->view('musbas_editTemplate_choose',$data);
    }else{
      $this->load->model('style');
      $this->load->model('conf');
      $this->load->model('fcf/Carabinerwrapper');
      $data = new stdClass();
      $data->ikc = new stdClass();
      $data->ikc->templates = array($this->style->getTemplate($id));
      $data->ikc->conf = $this->conf->get();
      $data->ikc->js = $this->Carabinerwrapper->jsTagsForModule('musbas');
      $this->load->view('musbas_editTemplate_edit',$data);
    }
  }
  function ownTemplateForId($id){
    $this->load->model("style");
    echo json_encode($this->style->ownTemplate($id));
  }
  function deleteTemplate($id){
    $this->load->model("style");
    echo json_encode($this->style->deleteTemplate($id));
  }
  function storeTemplate(){
    $data = $this->_getJson();
    if(!(property_exists($data, "template"))){
      exit("no template");
    }
    if(!(is_object($data->template))){
      exit("wrong format template");
    }
    $this->load->model("style");
    $templateID = "";
    foreach($data->template as $i => $item){
      $templateID = $i;
      if(false === $this->style->storeTemplateDataForId($i,$item)){
        exit("error on template " . $i);
      }
    }
    print(json_encode(array("stored"=>$templateID)));
  }
  private function _getJson(){
    if(!($_POST['json'])){
      exit("no json");
    }
    $data = json_decode($_POST['json']); 
    return $data;
  }
  function emptyTmpZipDir(){
    $this->load->model("style");
    $this->style->emptyTmpZipDir();
  }
  function resampleStyleClips($styleID){
    $this->load->model("style");
    $this->style->resampleStyleClips($styleID);
  }
  function dldStyleClips(){
    $this->load->model("style");
    $this->load->model('conf');
    if(isset($_POST["radio"])){
      $data = array("zip"=>$this->style->getStyleZip($_POST["radio"]),'conf'=>$this->conf->get());
      $this->load->view("musbas_dldStyleClips_zip", $data);
    }
    else{
      $data = array("styles"=>$this->style->glist(),'conf'=>$this->conf->get());
      $this->load->view("musbas_dldStyleClips_list",$data);
    }
  }
  function uploadRawAudioStyleZipTest(){
    $this->load->model("Session");
    if(false === ($this->_mod = $this->Session->getUserAsModerator())){
      exit("permission problem");
    }
    $this->load->model('conf');
    if(!isset($_FILES["zipfile"])){  
      $this->load->helper(array('form', 'url'));
      $data = array('error'=>'','ikc'=>array('conf'=>$this->conf->get()), 'mode'=>'test');
      $this->load->view('musbas_uploadRawAudioStyleZip_getFile', $data);
    }else{
      $config['upload_path'] = $this->config->item('temp_zip_dir');
      $config['allowed_types'] = 'zip';
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('zipfile')) {
        $this->load->helper(array('form', 'url'));
        $data = array();
        $data['error'] = $this->upload->display_errors();
        $data['ikc'] = array('conf'=>$this->conf->get());
        $this->load->view('musbas_uploadRawAudioStyleZip_getFile', $data);
      } else {
        $udata = $this->upload->data();
        echo "<pre>";
        print_r($udata);
        echo "</pre><br/>";
        $this->load->model('style');
        $this->style->extractZip($udata['full_path'], true);
      }
    }
  }
  /*function extractMarijn(){
    $this->load->model('style');
    $this->style->extractZip('/var/www/vhosts/looplala.com/httpdocs/data/cache/mco8/tempzipdir/marijndh.zip',false);
  }*/
  /*function extractStan(){
    $this->load->model('style');
    $this->style->extractZip( '/var/www/vhosts/ikcomponeer.nl/httpdocs/data/cache/mco8/tempzipdir/kgbmorricone3.zip', false);
  }*/
  function uploadRawAudioStyleZip(){
    
    $this->load->model("Session");
    if(false === ($this->_mod = $this->Session->getUserAsModerator())){
      exit("permission problem");
    }
    
    $this->load->model('conf');
    if(!isset($_FILES["zipfile"])){  
      $this->load->helper(array('form', 'url'));
      $data = array('error'=>'','ikc'=>array('conf'=>$this->conf->get()));
      $this->load->view('musbas_uploadRawAudioStyleZip_getFile', $data);
    }else{
      $config['upload_path'] = $this->config->item('temp_zip_dir');
      $config['allowed_types'] = 'zip';
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('zipfile')) {
        $this->load->helper(array('form', 'url'));
        $data = array();
        $data['error'] = $this->upload->display_errors();
        $data['ikc'] = array('conf'=>$this->conf->get());
        $this->load->view('musbas_uploadRawAudioStyleZip_getFile', $data);
      } else {
        $udata = $this->upload->data();
        echo "<pre>";
        print_r($udata);
        echo "</pre><br/>";
        $this->load->model('style');
        $this->style->extractZip($udata['full_path']);
      }
    }
  }
}

?>