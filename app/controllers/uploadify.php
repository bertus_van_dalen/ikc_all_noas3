<?php

class Uploadify extends CI_Controller {
  
  function subpartBackground(){
    
    $this->load->model("file");
    $targetFile = $this->file->getEmptyLocation();
    
    if (!empty($_FILES)) {
      $tempFile = $_FILES['Filedata']['tmp_name'];
      $fileTypes = array('jpg','jpeg','gif','png');
      $fileParts = pathinfo($_FILES['Filedata']['name']);
      if (in_array($fileParts['extension'],$fileTypes)) {
              move_uploaded_file($tempFile,$targetFile);
              echo '1';
      } else {
              echo 'Invalid file type.';
      }
    }
  }
}
?>