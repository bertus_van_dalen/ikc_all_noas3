<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filemanager extends CI_Controller {
  
  function connector(){
    
    /**
    * Protect against sending warnings to the browser.
    * Comment out this line during debugging.
    */
    // error_reporting(0);
    /**
    * Protect against sending content before all HTTP headers are sent (#186).
    */
    ob_start();
    
    /**
     * define required constants
     */
    
    
    define('CKFINDER_CONNECTOR_DEFAULT_USER_FILES_PATH', "/userfiles/");
    define('CKFINDER_CONNECTOR_LANG_PATH',"./lang");
    define('CKFINDER_CONNECTOR_CONFIG_FILE_PATH', FCPATH . "ckfinder/config.php");
    define('CKFINDER_CONNECTOR_LIB_DIR', FCPATH . "ckfinder/core/connector/php/php5");
    // defaults
    require_once FCPATH . "ckfinder/core/connector/php/constants.php";
    
    // @ob_end_clean();
    // header("Content-Encoding: none");

    /**
     * we need this class in each call
     */
    require_once CKFINDER_CONNECTOR_LIB_DIR . "/CommandHandler/CommandHandlerBase.php";
    /**
     * singleton factory
     */
    require_once CKFINDER_CONNECTOR_LIB_DIR . "/Core/Factory.php";
    /**
     * utils class
     */
    require_once CKFINDER_CONNECTOR_LIB_DIR . "/Utils/Misc.php";
    /**
     * hooks class
     */
    require_once CKFINDER_CONNECTOR_LIB_DIR . "/Core/Hooks.php";

    /**
     * Simple function required by config.php - discover the server side path
     * to the directory relative to the "$baseUrl" attribute
     *
     * @package CKFinder
     * @subpackage Connector
     * @param string $baseUrl
     * @return string
     */
    function resolveUrl($baseUrl) {
      $fileSystem = & CKFinder_Connector_Core_Factory::getInstance("Utils_FileSystem");
      $baseUrl = preg_replace("|^http(s)?://[^/]+|i", "", $baseUrl);
      return $fileSystem->getDocumentRootPath() . $baseUrl;
    }
    

    $utilsSecurity = & CKFinder_Connector_Core_Factory::getInstance("Utils_Security");
    $utilsSecurity->getRidOfMagicQuotes();

    /**
     * $config must be initialised
     */
    $config = array();
    $config['Hooks'] = array();
    $config['Plugins'] = array();

    /**
     * Fix cookies bug in Flash.
     */
    if (!empty($_GET['command']) && $_GET['command'] == 'FileUpload' && !empty($_POST)) {
      foreach ($_POST as $key => $val) {
        if (strpos($key, "ckfcookie_") === 0)
          $_COOKIE[str_replace("ckfcookie_", "", $key)] = $val;
      }
    }

    /**
     * read config file
     */
    require_once CKFINDER_CONNECTOR_CONFIG_FILE_PATH;
    $GLOBALS['config'] = $config;

    CKFinder_Connector_Core_Factory::initFactory();
    $connector = & CKFinder_Connector_Core_Factory::getInstance("Core_Connector");
    $GLOBALS['connector'] = $connector;

    if (isset($_GET['command'])) {
      $connector->executeCommand($_GET['command']);
    } else {
      $connector->handleInvalidCommand();
    }
  }
}
?>