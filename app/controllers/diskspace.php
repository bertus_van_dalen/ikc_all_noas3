<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diskspace extends CI_Controller {

	public function index()
	{
		$this->load->helper('directory');
    foreach(directory_map(FCPATH . "download/published") as $k => $v){
      $min = time() - (60 * 60 * 24 * 365);
      $mti = filemtime(FCPATH . "download/published/" . $v);
      if($mti < $min){
        $text = "DELETE ";
        unlink(FCPATH . "download/published/" . $v);
      }else{
        $text = "KEEP ";
      }
      echo $text . $v . " - " . date ("l d F Y", $mti)  . "<br/>";
    }
	}
}

?>
