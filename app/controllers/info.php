<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info extends CI_Controller {
  
  
  
	public function index()
	{
    $this->load->model("Session");
    $this->load->model("Conf");
    $data = new stdClass();
    $data->ikc = new stdClass();
    $data->ikc->conf = $this->Conf->get();
		$this->load->view('info',$data);
	}
  public function show(){
    phpinfo();
  }
  public function nieuwsbrief(){
    if(array_key_exists("uwMailAdres", $_POST)){
      if( false !== filter_var($_POST['uwMailAdres'], FILTER_VALIDATE_EMAIL)){
        $this->load->model("Session");
        $nieuwsbriefadres = R::findOne("nieuwsbriefadres", " mail = ?", array($_POST['uwMailAdres']));
        if($nieuwsbriefadres == null){
          $nieuwsbriefadres = R::dispense("nieuwsbriefadres");
          $nieuwsbriefadres->mail = $_POST['uwMailAdres'];
          R::store($nieuwsbriefadres);
          $message = "Dank u wel voor uw inschrijving. U ontvangt de eerstvolgende nieuwsbrief te zijner tijd.";
          $this->output->set_output($message);
        }else{
          show_error("Dit adres staat bij ons reeds in de nieuwsbrieven lijst. U zult te zijner tijd de nieuwsbrief ontvangen.",400);
        }
      }else{
        show_error("Adres heeft geen geldige indeling.",400);
      }
    }else{
      show_error("Adres werd niet gevonden in het bericht.",400);
    }
  }
  public function contactform(){
    
    $ip = $_SERVER['REMOTE_ADDR'];
    $key = "6LegUQATAAAAAF8iVSSWP6H2fOreVBIRduTsEKdR";
    $resp = $_POST['contactFormGrecaptchaResponse'];
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $full_url = $url.'?secret='.$key.'&response='.$resp.'&remoteip='.$ip;
    $data = json_decode(file_get_contents($full_url));
    if( (!isset($data->success)) || $data->success != true) {
      show_error("recaptcha error");
    }
    
    $contactFormName              =    $_POST['contactFormName'];              
    $contactFormMessage           =    $_POST['contactFormMessage'];           
    $contactFormEmail             =    $_POST['contactFormEmail'];             
    $contactFormPhone             =    $_POST['contactFormPhone'];             
    $contactFormIsTechnicalProblem=    $_POST['contactFormIsTechnicalProblem'] === "true";
    $contactFormIsSuggestion      =    $_POST['contactFormIsSuggestion']  === "true";      
    $contactFormIsInquiry         =    $_POST['contactFormIsInquiry']  === "true";
    if((!$contactFormName) || empty($contactFormName)){
      show_error("U dient een naam in te vullen");
    }
    if((!$contactFormMessage) || empty($contactFormMessage)){
      show_error("U dient een bericht in te vullen");
    }
    
    // The message
    $message = "Naam:\r\n" . $contactFormName . "\r\n\r\n";
    $message .= "Bericht:\r\n" . $contactFormMessage . "\r\n\r\n";
    $message .= "Mail adres:\r\n" . $contactFormEmail . "\r\n\r\n";
    $message .= "Telefoonnummer:\r\n" . $contactFormPhone . "\r\n\r\n";
    $message .= "technisch probleem:\r\n" . ($contactFormIsTechnicalProblem ? "J" : "N") . "\r\n\r\n";
    $message .= "suggestie:\r\n" . ($contactFormIsSuggestion ? "J" : "N") . "\r\n\r\n";
    $message .= "informatieverzoek:\r\n" . ($contactFormIsInquiry ? "J" : "N") . "\r\n\r\n";

    // In case any of our lines are larger than 70 characters, we should use wordwrap()
    $message = wordwrap($message, 70, "\r\n");
    
    $to = "contact.ikcomponeer@gmail.com";
    
    // Send
    if(!mail($to, 'Ikcomponeer Contactformulier', $message)){
      show_error("helaas kon het bericht niet worden bezorgd");
    }
    
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
