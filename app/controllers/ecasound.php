<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Ecasound extends CI_Controller {

  /**
   * 
   * SCENARIO FOR THE USE OF THIS CODE
   * 
   * The Problem 
   * 
   * A user creates a composition and may wish to hear it at a later time as an MP3 or download the MP3.
   * For example, each pupil in the class creates a composition and then the teacher wants to listen and
   * discuss them all in a row. They use a playlist of MP3 files for that. This functionality has been
   * with the composition tool from the start in 2009.
   * 
   * Back then it would, after saving a composition, start a background task in Flash. This has two problems
   * 1) during the background task it would decrease the available CPU cycles potentially to a point where 
   * the music that the user would play in the foreground, would suffer and
   * 2) because the user would not understand not to close the browser, it would be closed and there 
   * would be no MP3 file submitted although it was expected for the playlist.
   * 
   * The second approach we did was, start the web page and the flash tool in a special "agent" mode.
   * It would then occupy one old laptop in the living room of the developer. This tool would poll the
   * the server for the compositions to be rendered, open them, create an MP3 just like the aforementioned 
   * process in the background, and then submit the mp3 to the server when done and then poll for the next.
   * 
   * If there would be a multitude of compositions waiting this could be accepted, they would just wait.
   * Rendering one composition would take two to four minutes per composition thus enabling the entire 
   * platform for a maximum around the 500 compositions per day. 
   * 
   * This would start to become a problem when we were required the ability for more capacity.
   * It would require too many old laptops sitting in the living room of the developer (and an extra air 
   * conditioning system).
   * 
   * So we needed this functionality, where instead of the same client in "agent" mode, the "agent" would
   * use a technology better suited for server-side processing higher quality and scalable. It is Ecasound
   * the linux audio processing library that can be used from the command line and is very accurate and fast.
   * 
   * If it needs to be horizontally scalable, it will also need to integrate with scaling methodologies from
   * cloud services. The most efficient and cost-effective one for this specific use looks like OpenShift.
   * You have to look at all the metrics (traffic, CPU, memory and how this is technically accounted for).
   * These change by the year with the different providers. As of today these are Amazon, Azure, Heroku and 
   * OpenShift of which the last seems to look best for this particular usage metric, at the time of writing.
   * 
   * As I'm studying the scaling methods I can also lay-out the sequential schema of what will happen,
   * the entire chain of events that will lead to "elastic" scaling of processing power.
   * 
   * For the record, the "central" server of the platform is *not* (necessarily) on OpenShift. As per 
   * the writing of this documentation it is still somewhere else. So we have FOUR roles.
   * 
   * S) server hosting the central database and application logic of the platform (one)
   * MX) mixing agents / openshift agents (at least one but endlessly scalable)
   *
   * - to avoid repetition some sections have a (step name) between parentheses
   * 
   * 
   * 
   * MX       (aut) a minutely cron method is triggered
   * MX       (aut) the method should check if this MX instance is not already busy, by querying a local marker file
   * MX       (scale) the cron method will see if it is running on the root gear of the openshift application
   * MX       (scale) if not, proceed to (gettask)
   * MX       (scale) the cron method will read the number of currently active openshift gears
   * MX       (scale) the cron method will retrieve from the ikcomponeer server the number of required gears
   * MX       (scale) if the number of required gears is more than the currently active number of gears it will add a gear
   * MX       (scale) if the number of required gears is equal to or less than the amount of currently active gears it will do nothing
   * MX       (scale) except when the number of required gears is -1, expressing that no agent-tasks are there in which case it will remove a gear
   * 
   * MX -> S  (gettask) MX requests task for itself and communicates a unique ID to the server
   * S        (gettask) will answer with the most urgent task (this is the most recently stored one)
   * S        (gettask) will return a timestamp for the composition to later determine the version of resulting MP3
   *          EXAMPLE "id":12345,"content":"composition content","mtime":1429372380
   * 
   * MX       (task) processes the task
   * MX       (task) checks that it has the necessary MP3 files and if not, downloads them
   * MX       (task) checks that is has the MP3 files as audio and if not, converts them
   * MX       (task) generates an audio file from the processed task
   * MX       (task) converts the result to an MP3
   * MX -> S  (submit) submits the resulting MP3
   * S        (submit) stores the MP3 if the composition does not have a later modified timestamp
   *          (the case with a newer timestamp would that would require a new mixdown task)
   * S        (submit) returns data like the "gettask" method
   * MX       !(task) goes to sleep because there's no open tasks (if there would be, it would start immediately)
   * MX       will be retriggered at the first second of the next minute of the clock
   * 
   */

  
  const TASK_DISTRIBUTION_TIMEOUT = 600;
  const VERSION_INTRODUCED = 1427988686;
  
  private $_s;
  private $_uniqid;
  private $DB_ECASOUND_INSTANCE_DIR;
  private $DB_ECASOUND_LOCK_DIR;
  private $ECASOUND;
  private $MPG123;
  private $LAME;
  
  private $_constructionTime;
  // joblog
  // lock
  
  function __construct(){
    $this->DB_ECASOUND_INSTANCE_DIR = FCF_PRIV_CACHE_DIR . '/ecasound_instance/';
    $this->DB_ECASOUND_LOCK_DIR = FCF_PRIV_CACHE_DIR . '/ecasound_lock/';
    parent::__construct();
    $this->ECASOUND = FCF_DATA_DIR . '/bin/ecasound';
    $this->MPG123 = FCF_DATA_DIR . '/bin/mpg123';
    $this->LAME = FCF_DATA_DIR . '/bin/lame';
    $this->log->setThreshold(4);
    $this->_s = FCF_DATA_DIR . '/clips8/';
    if(!is_dir($this->_s)){
      mkdir($this->_s, 0777, true);
    }
    $this->_uniqid = $this->_retrieveOrCreateServerId();
    $this->_constructionTime = time();
  }
  
  private function lock(){
    if(!is_dir($this->DB_ECASOUND_LOCK_DIR)){
      if(false === mkdir($this->DB_ECASOUND_LOCK_DIR)){
        log_message("error","cannot create directory for lock");
      }
    }
    return mkdir ($this->DB_ECASOUND_LOCK_DIR . "lock");
  }
  
  public function unlock(){
    if(!is_dir($this->DB_ECASOUND_LOCK_DIR . "lock")){
      log_message("error","cannot unlock when lock is not there");
    }
    if(false === rmdir($this->DB_ECASOUND_LOCK_DIR . "lock")){
      log_message("error","could not remove lockfile");
    }
  }
  
  private function _getTaskFromIkcomponeer(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://ikcomponeer.nl/ecasound/gettask/" . $this->_uniqid);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $out = curl_exec($ch);
    curl_close($ch);
    log_message("debug","RECEIVED TASK: " . $out);
    return empty($out) ? false : json_decode($out);
  }
  
  public function testEcasound(){
    echo shell_exec($this->ECASOUND . " --version 2>&1");
  }
  
  public function automaticTask(){
    if($this->lock()){
      $this->_automaticTask();
    }
  }
  
  private function _automaticTask(){
    log_message("debug","starting task");
    $task = $this->_getTaskFromIkcomponeer();
    if($task === false || (!is_object($task)) || $task->id === 0){ 
      $this->unlock(); 
      exit;
    }
    $this->_renderMp3($task);
    
    // todo when we really want to get loose
    $this->unlock();
    
    if($this->_runsOnRootGear()){
      log_message("debug","running on the root gear, return after one job");
      return;
    }
    if($this->_outOfTime()){
      log_message("debug","out of time, return");
      return;
    }
    log_message("debug","repeating task");
    set_time_limit(90);
    $this->lock();
    $this->_automaticTask();
  }
  
  private function _runsOnRootGear(){
    $haProxyDir = getenv('OPENSHIFT_HOMEDIR') . '/haproxy';
    return is_dir($haProxyDir);
  }
  
  private function _outOfTime(){
    return $this->_constructionTime + 240 < time();
  }
  
  private function _renderMp3($task){
    $job = $this->_convertTaskToJob($task);
    $this->_armForMixdown($job->clips, $job->id);
    $cmd = $this->_createEcasoundScript($job->mixins, $job->id);
    if(!empty($cmd)){
      set_time_limit(300);
      shell_exec($cmd);
      $cmd = $this->LAME . " -q3 -b192 " . $this->_s . "mixdowns/" . $job->id . ".WAV" . " " . $this->_s . "mixdowns/" . $job->id . ".MP3" . " 2>&1";
      shell_exec($cmd);
    }
    $this->_submit($job->id, $job->mtime);
    $this->_cleanup($job->id);
    log_message("debug","finished: " . $job->id);
  }
  
  /** 
   * one time (at instance creation by elastic scaling) it will be filled 
   * so it can be identified with the master server 
   */
  private function _retrieveOrCreateServerId(){
    $f = $this->DB_ECASOUND_INSTANCE_DIR . 'instance';
    if(!file_exists($f)){
      if(!is_dir($this->DB_ECASOUND_INSTANCE_DIR)){
        mkdir($this->DB_ECASOUND_INSTANCE_DIR);
      }
      if(false === file_put_contents($f, uniqid("",true))){
        log_message("error","cannot write to " . $f);
      }
    }
    return file_get_contents($f);
  }
  
  public function whoami(){
    echo $this->_uniqid;
  }
  
  public function hello(){
    echo "Ecasound says Hello";
  }
  
  public function info(){
    phpinfo();
  }
  
  public function dbdir(){
    echo "data directory: " . FCF_PRIV_CACHE_DIR;
  }
  
  public function agentStats($frame){
    $this->load->library('rb');
    $r = array();
    $nn = 0;
    if($frame === 'hour'){
      $logs = R::findAll("agentsscalelog", " t >= ? ", array(time() - 3600));
      foreach($logs as $l){
        $nn++;
        $item = new stdClass();
        $item->tasks = $l->numtasks;
        $item->agents = $l->numagents;
        $item->t = $l->t - time();
        $r[] = $item;
      }
    }
    echo json_encode( $r);
  }
  
  public function submit($id, $mtime, $hasMP3 = 0) {
    log_message("debug","ecasound.submit(" . $id . ")");
    
    if (!$id) {
      log_message("error", "ecasound.submit(" . $id . ") - id arg is false");
      return;
    }
    $this->load->library('rb');
    $c = R::load("composition",$id);
    if(!$c->id){
      log_message("error", "cannot load composition for " . $id);
      return;
    }
    
    if($c->mtime > $mtime){
      log_message("debug","not the most recent version");
      return;
    }
    
    if(!($hasMP3)){
      $c->empty = true;
      R::store($c);
      return;
    }
    
    if (false === $data = file_get_contents('php://input')) {
      log_message("error", "no file in input");
      return;
    }
    
    $f = $this->config->item('published_dir') . $id . ".MP3";
    
    $fh;
    if (false === $fh = fopen($f, 'w')) {
      $message = "problem creating write handler " . $f;
      log_message("error", $message);
      return;
    }
    log_message("debug", "file write handler OK");
    if (false === fwrite($fh, $data)) {
      $message = "problem writing to " . $f;
      log_message("error", $message);
      return;
    }
    log_message("debug", "data written OK");
    if (false === fclose($fh)) {
      $message = "cannot close handler for " . $f;
      log_message("error", $message);
      return;
    }
    chmod($f, 0644);
    $c->hasmp3 = true;
    R::store($c);
  }
  
  private function _isLimited($cpBean){
    $cclimits = $cpBean->template->subset->style->ownCclimit;
    return count($cclimits) > 0;
  }
  
  private function _retrieveFirstCCMixTask($tasks){
    $t = array_shift($tasks);
    while($t){
      if($this->_isLimited($t)){
        $t->limited = 1;
        R::store($t);
        $t = array_shift($tasks);
      }else{
        return $t;
      }
    }
    return false;
  }
  
  private function _registerAgent($agentGuid){
    // make sure to register this agent with the DB
    $thisAgt = R::findOne('agent', ' guid = ? ', array($agentGuid));
    if(!$thisAgt){
      $thisAgt = R::dispense('agent');
      $thisAgt->guid = $agentGuid;
    }
    $thisAgt->lasttime = time();
    R::store($thisAgt);
    return $thisAgt;
  }
  
  private function _howManyAgentsDoWeNeed($taskCount){
    if($taskCount == 0) return -1;
    return 1 + floor($taskCount / 10);
  }
  
  public function numtasks(){
    $this->load->library('rb');
    echo R::count("composition", "empty = FALSE AND limited = FALSE AND hasmp3 = FALSE AND mixtime < ? AND mtime > ? ORDER BY mtime DESC", array(time()-self::TASK_DISTRIBUTION_TIMEOUT, self::VERSION_INTRODUCED));
  }
  
  public function targetedAmountOfAgents(){
    $this->load->library('rb');
    $openTasks = R::find("composition", "empty = FALSE AND limited = FALSE AND hasmp3 = FALSE AND mixtime < ? AND mtime > ? ORDER BY mtime DESC", array(time()-self::TASK_DISTRIBUTION_TIMEOUT, self::VERSION_INTRODUCED));
    $countOpenTasks = count($openTasks);
    $lg = R::dispense("agentsscalelog");
    $lg->t = time();
    $lg->numtasks = $countOpenTasks;
    $numAgentsTargeted = $this->_howManyAgentsDoWeNeed($countOpenTasks);
    $lg->numagents = $numAgentsTargeted == -1 ? 1 : $numAgentsTargeted;
    R::store($lg);
    echo $numAgentsTargeted;
  }
  
  public function gettask($agentGuid){
    if(empty($agentGuid)){
      log_message("error","no agentGuid");
      die;
    }
    $this->load->library('rb');
    $openTasks = R::find("composition", "empty = FALSE AND limited = FALSE AND hasmp3 = FALSE AND mixtime < ? AND mtime > ? ORDER BY mtime DESC", array(time()-self::TASK_DISTRIBUTION_TIMEOUT, self::VERSION_INTRODUCED));
    $a = $this->_registerAgent($agentGuid);
    $t = $this->_retrieveFirstCCMixTask($openTasks);
    if($t){
      $t->mixtime = time();
      $t->agent = $a;
      R::store($t);
      echo json_encode(array("id"=>$t->id,"content"=>html_entity_decode($t->content),"mtime"=>$t->mtime));
    }else{
      echo json_encode(array("id"=>0,"content"=>"","mtime"=>0));
    }
  }
  
  private function _getEmptyJob(){
    $job = new stdClass();
    $job->clips = array();
    $job->mixins = array();
    return $job;
  }
  
  private function _convertTaskToJob($t){
    $job = $this->_getEmptyJob();
    $composition = simplexml_load_string(html_entity_decode($t->content));
    foreach ($composition->track as $track) {
      foreach ($track->event as $evt) {
        $id = (string)$evt->source->id;
        if(!in_array($id, $job->clips)){
          $job->clips[] = $id;
        }
        $job->mixins[] = $this->_calcMixin($evt);
      }
    }
    $job->id = $t->id;
    $job->mtime = $t->mtime;
    return $job;
  }
  
  private function _downloadMP3($clip){
    set_time_limit(30);
    $fp = fopen ($this->_s . '_' . $clip . '.MP3', 'w+');
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, 'http://ikcomponeer.nl/data/clips8/' . $clip . '.MP3' );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, false );
    curl_setopt( $ch, CURLOPT_BINARYTRANSFER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
    curl_setopt( $ch, CURLOPT_FILE, $fp );
    curl_exec( $ch );
    curl_close( $ch );
    fclose( $fp );
    rename($this->_s . '_' . $clip . '.MP3', $this->_s . $clip . '.MP3');
  }
  
  private function _convertMP3($clip){
    set_time_limit(30);
    $c = $this->MPG123 . ' -w ' . $this->_s . '_' . $clip . '.WAV' . ' ' . $this->_s . $clip . '.MP3' . ' ' . '2>&1';
    shell_exec($c);
    rename($this->_s . '_' . $clip . '.WAV', $this->_s . $clip . '.WAV');
  }
  
  private function _armForMixdown($clips, $id){
    if(!is_dir($this->_s . "mixdowns/")){
      mkdir($this->_s . "mixdowns/");
    }
    foreach($clips as $clip){
      if (!is_file($this->_s . $clip . '.MP3')){
        $this->_downloadMP3($clip);
      }
      if (!is_file($this->_s . $clip . '.WAV')){
        $this->_convertMP3($clip);
      }
    }
  }
  
  private function _findFirstEventTime($mixins, $id){
    $first = floatval(3600);
    foreach($mixins as $mixin){
      if($first > $mixin->start){
        $first = floatval($mixin->start);
      }
    }
    return $first;
  }
  
  private function _createEcasoundScript($mixins, $id){
    $first = $this->_findFirstEventTime($mixins, $id);
    if($first == 3600){
      return "";
    }
    $cmd = $this->ECASOUND . " -D -z:mixmode,sum ";
    $chainCount = 1;
    foreach($mixins as $mixin){
      $at = floatval($mixin->start) - $first;
      $cmd .= "-a:" . $chainCount . " -i playat," . $at . ",select,0," . floatval($mixin->duration) . "," . $this->_s . $mixin->id . ".WAV ";
      $chainCount++;
    }
    $cmd .= "-a:all -o " . $this->_s . "mixdowns/" . $id . ".WAV 2>&1";
    return $cmd;
  }
  
  private function _submit($id, $mtime){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/octet-stream'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    echo "submit<br/>";
    $mp3 = $this->_s . "mixdowns/" . $id . ".MP3";
    $hasMP3 = is_file($mp3) ? 1 : 0;
    if($hasMP3){
      curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents($mp3));
    }
    curl_setopt($ch, CURLOPT_URL, "http://ikcomponeer.nl/ecasound/submit/" . $id . "/" . $mtime . "/" . $hasMP3);
    $response = curl_exec($ch);
    echo $response;
    curl_close($ch);
  }
  
  private function _cleanup($id){
    $mp3 = $this->_s . "mixdowns/" . $id . ".MP3";
    $wav = $this->_s . "mixdowns/" . $id . ".WAV";
    if(is_file($mp3)){ unlink($mp3); }
    if(is_file($wav)){ unlink($wav); }
  }
  
  private function _calcMixin($evt){
    $r = new stdClass();
    $r->start = intval($evt['startTime']) / 44100;
    $r->duration = intval($evt['totalTime']) / 44100;
    $r->id = (string)$evt->source->id;
    return $r;
  }
}