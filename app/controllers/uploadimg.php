<?php

class Uploadimg extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

  private function isModerator(){
    $this->load->model("Session");
    if(false === ($mod = $this->Session->getUserAsModerator())){
      return false;
    }
    return true;
  }
  
	function index()
	{
		
		$error = '';
		
		if(!(true === $this->isModerator())){
      
      $error = array('error' => "permissie probleem");

			$this->load->view('ci_upload_form', $error);
    
      return;
    }
    
    
    
    $actionUrl = $this->config->item('project_url') . 'uploadimg/do_upload';
    $listUrl = $this->config->item("project_url") . 'uploadimg/list_files';
    $this->load->view('ci_upload_form', array('error' => $error, 'form' => form_open_multipart($actionUrl),'listUrl'=>$listUrl));
	}

	function do_upload()
	{
		if(!(true === $this->isModerator())){
      
      $error = array('error' => "permissie probleem");

			$this->load->view('ci_upload_form', $error);
    
      return;
    }
    
    
    $config['upload_path'] = $this->config->item("uploaded_dir");
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('ci_upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
      $uploadUrl = $this->config->item('project_url') . 'uploadimg';
      $listUrl = $this->config->item("project_url") . 'uploadimg/list_files';
      $data['uploadUrl'] = $uploadUrl;
      $data['listUrl'] = $listUrl;

			$this->load->view('ci_upload_success', $data);
		}
	}
  function list_files(){
    if(!(true === $this->isModerator())){
      
      $error = array('error' => "permissie probleem");

			$this->load->view('ci_upload_form', $error);
    
      return;
    }
    
    
    $fs = scandir($this->config->item("uploaded_dir"));
    $uplUrlBase = $this->config->item("uploaded_dir");
    $base = $this->config->item("base_url");
		$filesFiltered = array();
    foreach($fs as $i => $f){
      if("." == $f || ".." == $f) continue;
      $filesFiltered[] = $f;
    }
    $this->load->view("ci_upload_list_files",array("files"=>$filesFiltered,"url_path"=>$base . $uplUrlBase));
  }
}
?>