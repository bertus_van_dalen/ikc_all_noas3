<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Composition extends CI_Controller {

  private $data;

  function __construct() {
    parent::__construct();
    log_message('debug', '******************');
    log_message('debug', '**composition*****');
    log_message('debug', '******************');
    $this->load->Model('MComposition');
    $this->data = null;
    if (!empty($_POST)) {
      if (array_key_exists('json', $_POST)) {
        $this->data = json_decode($_POST['json']);
        log_message('debug', '$_POST loaded and decoded');
      }
    }
  }

  function playlistdata($id){ 
	  $c = $this->MComposition->read($id);
  }
  
  
  function read() {
    log_message('debug', 'function read for data ' . print_r($this->data, true));
    $r = $this->MComposition->read($this->data);
    echo json_encode($r);
  }
  
  function data($id){
	  $r = $this->MComposition->data($id);
	  echo json_encode($r);
  }

  function write() {
    if ($this->data) {
      $return = $this->MComposition->write($this->data);
    } else {
      show_error("no data!");
    }
    log_message('debug', 'return: ' . json_encode($return));
    echo json_encode($return);
  }

  function delete($id) {
    $return = json_encode($this->MComposition->delete($id));
    log_message('debug', 'return: ' . $return);
    echo $return;
  }
  
  function _getValidParams(){
    
    $params = $this->uri->ruri_to_assoc();
    
    // ADD MAX MTIME
    if(!array_key_exists("maxMtime", $params)){
      $params["maxMtime"] = time();
    }else if((int)$params["maxMtime"] == 0){
      $params["maxMtime"] = time();
    }
    
    // ADD PAGE
    if(!array_key_exists("page", $params)){
      $params["page"] = 0;
    }
    if(0 > (int)$params["page"]){
      $params["page"] = 0;
    }
    
    // ADD PAGE LENGTH
    if(!array_key_exists("pageLength", $params)){
      $params["pageLength"] = 5;
    }
    
    // ADD ORDER BY
    if(!array_key_exists("order", $params)){
        $params["order"] = "date";
    }
    
    // ADD MINE
    if(!array_key_exists("mine",$params)){
        $params["mine"] = "y";
    }
    
    // ADD OTHERS
    if(!array_key_exists("others",$params)){
        $params["others"] = "y";
    }
    
    // DEFINE VALID KEYS AND OPTIONS
    $validKeys = $this->SEARCH_KEYS();
    $validValues = $this->SEARCH_VALUES();
    $validReplacementValues = $this->SEARCH_VALUES_REPLACEMENTS();
    
    // VALIDATE SOME
    $validatedParams = array();
    foreach($params as $k => $v){
      if(in_array($k, $validKeys)){
        if(in_array($k, $validValues)){
          if(in_array($v,$validValues[$k])){
            $index = array_search($v, $validReplacementValues[$k]);
            $validatedParams[$k] = $validReplacementValues[$k][$index];
          }else{
            $validatedParams[$k] = false;
          }
        }else{
          $validatedParams[$k] = urldecode($v);
        }
      }
    }
    
    // SOME TO LOWER
    if(array_key_exists("name", $validatedParams)){
      $lwr = strtolower($validatedParams["name"]);
      $validatedParams["name"] = $lwr;
    }
    
    return $validatedParams;
  }
  
  private function SEARCH_KEYS(){
    return array(
        "name",
        "user",
        "order",
        "maxMtime",
        "page",
        "pageLength",
        "style",
        "reset",
        "mine",
        "others",
        "composition"
        );
  }
  private function SEARCH_VALUES(){
    return array(
        "order" => array(
            "date",
            "user_name",
            "composition_name",
            "id"
            ),
        "mine" => array(
            "y",
            "n"
            ),
        "others" => array(
            "y",
            "n"
            )
        );
  }
  private function SEARCH_VALUES_REPLACEMENTS(){
    return array(
        "order" => array(
            "mtime",
            "user_name",
            "composition_name",
            "id"
            )
    );
  }
  function searchParams(){
    echo "<pre>" . print_r($this->SEARCH_KEYS(),true) . '</pre>';
    echo "<pre>" . print_r($this->SEARCH_VALUES(),true) . '</pre>';
	echo "<pre>" . "example http://localhost/mco1307/mco9/composition/search/name/we/order/id/maxMtime/1383570000/pageLength/1/page/2/userId/2433" . "</pre>";
  }
  function search(){
    
    $this->load->model("session");
    $projid = $this->session->projectId();
    $uid = $this->session->getUser()->id;
    
    $p = $this->_getValidParams();
    /* COUNT ROWS FROM SUBQUERY
    SELECT ( SELECT COUNT(id) FROM aTable ) as count FROM table
    */
    //, 
    //  (SELECT COUNT (id) FROM composition WHERE user_id == u.id) as composition_user_numCompositions 
    // p - project
    // cp - composition_playlist
    // c - composition
    // u - user
    // t - template
    // ss - subset
    // s - style
    $select = "SELECT SQL_CALC_FOUND_ROWS c.name as composition_name, 
      u.name as composition_user_name, 
      up.username as composition_user_profile_name, 
      c.id as composition_id, 
      c.mtime as composition_time, 
      c.user_id as composition_user_id,
      c.published as composition_published, 
      c.hasmp3 as composition_hasmp3, 
      t.name as composition_template_name, 
      s.id as composition_template_subset_style_id, 
      s.iconurl as composition_template_subset_style_styleIcon, 
      s.name as composition_template_subset_style_name, 
      (SELECT COUNT(id) FROM composition WHERE user_id = u.id AND hasmp3 = 1 AND published = 1) as composition_user_numCompositions,
      (SELECT c.user_id = ?) as composition_writable 
      FROM project p 
      JOIN playlist pl ON pl.project_id = p.id 
      JOIN composition_playlist cp ON cp.playlist_id = pl.id 
      JOIN composition c ON c.id = cp.composition_id 
      JOIN user u ON c.user_id = u.id 
      JOIN template t ON c.template_id = t.id 
      JOIN subset ss ON t.subset_id = ss.id 
      JOIN style s ON ss.style_id = s.id 
      LEFT JOIN profile up ON u.profile_id = up.id ";
    
    $where = " WHERE p.id = ? "; 
    $vals = array($uid, $projid);
    
    $where .= " AND (( " . ( isset($p['others']) && $p['others'] == "y" ? "TRUE" : "FALSE" ) . " ";
    
    $limit = "";
    
    if(isset($p["composition"]) && $p['composition'] != '0'){
      $where .= " AND c.id = ?";
      $vals[] = $p["composition"];
    }else{
    
      if(isset($p["name"])){
        $where .= " AND (c.name LIKE ? OR u.name LIKE ? OR s.name LIKE ?)";
        $likeName = '%' . $p['name'] . '%';
        $vals[] = $likeName;
        $vals[] = $likeName;
        $vals[] = $likeName;
      }

      if(isset($p["maxMtime"])){
        $where .= " AND c.mtime <= ?";
        $vals[] = $p["maxMtime"];
      }

      if(isset($p["user"]) && $p['user'] != '0'){
        $where .= " AND c.user_id = ?";
        $vals[] = $p["user"];
      }
      
      if(isset($p['style']) && (int)$p['style'] != 0){
        $where .= ' AND s.id = ?';
        $vals[] = $p['style'];
      }
      
      $where .= " AND c.published = 1 AND c.hasmp3 = 1 ";
    }
    
    if(isset($p['user']) && $p['user'] != '0' && (int)$p['user'] == $this->session->getUser()->id){
      $p['mine'] = 'y';
    }
    
    if($p["mine"] == "y"){
      $where .= " ) OR c.user_id = ? ) ";
      $vals[] = $uid;
    }else{
      $where .= " )) ";
    }
    
    if(isset($p['order'])){
      $ob = $p['order'];
      if($ob == 'composition_name'){
        $where .= " ORDER BY c.name ASC";
      }else if($ob == 'user_name'){
        $where .= " ORDER BY u.name ASC";
      }else if($ob == "date"){
        $where .= " ORDER BY c.mtime DESC";
      }else if($ob == "id"){
        $where .= " ORDER BY c.id DESC";
      }
    }
    
    $limit .= " LIMIT ?,?";
    $vals[] = (int) $p['page'] * (int) $p['pageLength'];
    $vals[] = (int) $p['pageLength'];
    
    log_message("debug",vsprintf(str_replace("?", "%s", $select.$where.$limit), $vals));
    
    $qr = R::getAll($select.$where.$limit, $vals);
    $fr = R::getAll('SELECT FOUND_ROWS()');
    $rowCount = (int)reset(reset($fr));
    
    if($rowCount < (int) $p['page'] * (int) $p['pageLength']){
      $p['page'] = 0;
      $vals[count($vals)-2] = 0;
      $qr = R::getAll($select.$where.$limit, $vals);
      $fr = R::getAll('SELECT FOUND_ROWS()');
      $rowCount = (int)reset(reset($fr));
    }
    
    $return = array();
    $return['query'] = $p;
    $return['count'] = $rowCount;
    $return['result'] = $qr;
    
    echo json_encode($return);
  }
  function stylesForProject(){
    $this->load->model('session');
    echo '<pre>' . print_r($this->session->getStylesForProject(),true) . '</pre>';
  }
}

?>
