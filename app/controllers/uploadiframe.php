<?php

class Uploadiframe extends CI_Controller {
  /**
   * 
   * @param string $fileType clipbuttons, 
   */
  function iframe($fileType, $id){
    $action = $this->config->item("project_url") . "uploadiframe/upload/" . $fileType . "/" . $id;
    $this->load->helper("form");
    $this->load->view("uploadiframe_iframe",array("action" => $action));
  }
  function generate_guid($length){
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }
  function upload($fileType,$id){
    switch($fileType){
      case "clipbuttons":
        
        $dir = $this->config->item("download_dir_path") . "clipbuttons/";
        $randName = $this->generate_guid(8);
        while(file_exists($dir . $randName)){
          $randName = $this->generate_guid(8);
        }
        
        $cf['upload_path'] = $dir;
        $cf['allowed_types'] = 'png';
        $cf['max_size'] = '100';
        $cf['max_width'] = '1024';
        $cf['max_height'] = '768';
        $cf['file_name'] = $randName;
        $this->load->library('upload', $cf);

        $url = $this->config->item("project_url") . "uploadiframe/iframe/" . $fileType;
        $this->load->helper('url');
        $again = anchor($url, 'back');
        
        if (!$this->upload->do_upload()) {
          echo $this->upload->display_errors();
          echo $again;
          exit();
        } else {
          $ud = $this->upload->data();
          $durl = $this->config->item("download_base_url") . "clipbuttons/" . $ud["file_name"];
          $vd = array("name"=>$ud["file_name"],"src"=>$durl,"again"=>$again,"id"=>$id);
          $this->load->view('uploadiframe_success',$vd);
        }
        break;
    }
  }
}
?>