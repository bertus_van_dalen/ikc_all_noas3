<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class placeHolderLogin extends CI_Controller {

  private $_ORIGIN_WHITE_LIST = array("http://adeptive.nl");

  public function getKeyHash() {
    if (array_key_exists('HTTP_ORIGIN', $_SERVER) && in_array($_SERVER['HTTP_ORIGIN'], $this->_ORIGIN_WHITE_LIST)){
      header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
    }
    if (!isset($_GET['token'])) {
      die("you must set the token in the query string");
    }
    $token = $_GET['token'];
    log_message("debug","retrieved token from GET query string: " . $token);
    $this->load->model("Session");
    $ret = array();
    $ret['hash'] = $this->Session->getKeyHash($token);
    $ret['userGuid'] = "superFakeUser";
    echo json_encode($ret);
  }
}
?>