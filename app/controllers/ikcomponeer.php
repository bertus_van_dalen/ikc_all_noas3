<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This file contains the applications main / default / startup page controller class.
 * It includes functionality for starting up and logging in i.e. accessors to the
 * respective models for that.
 *
 * @author E.K. van Dalen <ek.vandalen@gmail.com>
 * @license http://ikcomponeer.nl/ikcomponeer_api_license.html Ikcomponeer License 2012
 * @copyright Copyright (c) 2010-2012, E.K. van Dalen
 * @package ikcomponeer_api
 */

/**
 * The ikcomponeer controller is the main / default / startup page controller class.
 * It includes functionality for starting up and logging in i.e. accessors to the
 * respective models for that.
 *
 * @author E.K. van Dalen <ek.vandalen@gmail.com>
 * @license http://ikcomponeer.nl/ikcomponeer_api_license.html Ikcomponeer License 2012
 * @copyright Copyright (c) 2010-2012, E.K. van Dalen
 * @package ikcomponeer_api
 */
class ikcomponeer extends CI_Controller {

    /**
     * the default index function loads the basic ikc.config object to javascript and it will
     * determine if we have in the context of this project a valid user logged in (in respect
     * to client and project). Depending on the login method (NPO method will each time need
     * a client-login) a valid session will be containing the componeertool immediately, or
     * it will first present the user with tabs containing the login method(s).
     *
     * The index method must be named including the application segment, the controller seg-
     * ment and the method segment e.g. mco4, ikcomponeer and index, to be able to "deep-
     * link" with parameters. The parameters are not confined to a specific number of them.
     *
     * For example http://ikcomponeer.nl/mco4/ikcomponeer/index/een/eins/twee/zwei will give
     * you an array with parameters like Array ( [een] => eins [twee] => zwei )
     * 
     * @author E.K. van Dalen <ek.vandalen@gmail.com>
     * @license http://ikcomponeer.nl/ikcomponeer_api_license.html Ikcomponeer License 2012
     * @copyright Copyright (c) 2010-2012, E.K. van Dalen
     * @package ikcomponeer_api
     */
    public function index() {
        
        $urlParams = $this->uri->uri_to_assoc(4);
        
        log_message('debug', '--------------------');
        log_message('debug', 'ikcomponeer->index()');

        $this->load->model("Session");
        $this->load->model("Conf");
        
        $data = new stdClass();
        $data->ikc = new stdClass();
        $data->ikc->s = $this->Session->getStatus();
        $data->ikc->conf = $this->Conf->get();
        
        $ogCpId;
        $ogCompositionTitle;
        $ogUrl;
        $ogMp3;
        $ogUserName;
        $ogCompositionPublishedDate;
        
        if(isset($urlParams["composition"])){
            $cp = R::load('composition',$urlParams["composition"]);
            if($cp->id){
                if($cp->published){
                    $u = $cp->user;
                    $this->load->model("Session");
                    $uName = $this->Session->getUser()->name;
                    $uName = preg_replace('/&(?![A-Za-z0-9#]{1,7};)/', '&amp;', $uName);
                    $publicationDate = date("Y-m-d", $cp->created);
                    $name = preg_replace('/&(?![A-Za-z0-9#]{1,7};)/', '&amp;', $cp->name);
                    
                    $ogCpId = $cp->id;
                    $ogCompositionTitle = $name;
                    $ogUrl = $this->config->item("project_url") . "ikcomponeer/index/composition/" . $ogCpId;
                    $ogMp3 = $this->config->item('published_dir') . $ogCpId . ".MP3";
                    $ogUserName = $uName;
                    $ogCompositionPublishedDate = $publicationDate;
                    $ogAlbumTitle = "IkComponeer / " . $data->ikc->conf->project_title;
                    $ogAlbumUrl = $this->Session->projectUrl();
                    
                    $data->og = new stdClass();
                    $data->og->title = $ogCompositionTitle;
                    $data->og->type = "music.song";
                    $data->og->url = $ogUrl;
                    $data->og->image = 'http://ikcomponeer.nl/ikcomponeerfb.gif';
                    $data->og->site_name = $ogAlbumTitle;
                    $data->og->description = 'Dit liedje is gecomponeerd door ' . $ogUserName . ' op ' . $ogCompositionPublishedDate;
                    $data->open_social_headers = $this->load->view("open_social_headers",$data,true);
                }
            }
            if(!(isset($ogCpId))){
                $data->og = new stdClass();
                $data->og->title = "onbekend";
                $data->og->type = 'song';
                $data->og->image = 'http://ikcomponeer.nl/ikcomponeerfb.gif';
                $data->og->url = $this->config->item("project_url") . "ikcomponeer/index/composition/" . $urlParams["composition"];
                //$data->og->audio = $ogMp3;
                $data->og->description = 'De compositie bestaat niet of is verwijderd.';
                //$data->music = new stdClass();
                //$data->music->song = new stdClass();
                $data->open_social_headers = $this->load->view("open_social_headers",$data,true);
            }
        }
        if($this->Session->projectAllowsOpenIdLogin()){
          $data->janrain_headers = $this->load->view("janrain_headers", $data, true);
        }
        $candidateViewFile = APPPATH . "views/ikcomponeer_" . FCF_CONF . ".php";
        if (@file_exists($candidateViewFile))
        {
           $this->load->view("ikcomponeer_" . FCF_CONF, $data);
        }else{
           $this->load->view('ikcomponeer', $data);
        }
    }

    /**
     * A port to this CI based environment of the janrain engage sample.
     * 
     * @copyright Copyright 2011, Janrain Inc. All rights reserved.
     * @author E.K. van Dalen <ek.vandalen@gmail.com>
     * @license http://ikcomponeer.nl/ikcomponeer_api_license.html Ikcomponeer License 2012
     * @copyright Copyright (c) 2010-2012, E.K. van Dalen
     * @package ikcomponeer_api
     */
    public function jrtoken() {
        log_message('debug', '--------------------');
        log_message('debug', 'ikcomponeer->jrtoken()');
        require_once(APPPATH . "third_party/janrain/janrain_engage_lib.php");
        $debug_array = array('Debug out:');
        $token = $_POST['token'];
        $format = ENGAGE_FORMAT_JSON;
        $extended = true;
        $api_key = $this->config->item("janrain_api_key");
        $result = engage_auth_info($api_key, $token, $format, $extended);
        if ($result === false) {
            $errors = engage_get_errors();
            foreach ($errors as $error => $label) {
                $debug_array[] = 'Error: ' . $error;
            }
        } else {
            $array_out = true;
            $auth_info_array = engage_parse_result($result, $format, $array_out);
            $debug_array[] = print_r($auth_info_array, true);
        }
        if (is_array($auth_info_array)) {
            $this->load->model("Session");
            $this->Session->setJanrainAuth($auth_info_array);
        }
        $debugs = engage_get_errors(ENGAGE_ELABEL_DEBUG);
        foreach ($debugs as $debug => $label) {
            $debug_array[] = 'Debug: ' . $debug;
        }
        $the_buffer = ob_get_contents();
        if (!empty($the_buffer)) {
            $debug_array[] = 'Buffer: ' . $the_buffer;
        }
        $the_debug = implode("\n", $debug_array);
        //log_message("debug", "janrain debug array - " . $the_debug);
        log_message("debug", "URI " . $_SERVER["REQUEST_URI"]);
        
        $this->load->model("Conf");
        $data = new stdClass();
        $data->ikc = new stdClass();
        $data->ikc->conf = $this->Conf->get();
        $this->load->view("ikcomponeer_jrtoken", $data);
    }
    /**
     * it will connect the session to a new anonymous user
     */
    public function signout() {
        log_message('debug', '--------------------');
        log_message('debug', 'ikcomponeer->signout()');
        $this->load->model("Session");
        $r = new stdClass();
        $r->exit = $this->Session->setNewAnonUser() ? 0 : -1;
        $r->s = $this->Session->getStatus();
        log_message("info","status: " . print_r($r,true));
        
        $this->output->set_header("Access-Control-Allow-Origin: " . $this->input->get_request_header('Origin', TRUE));
        $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
        $this->output->set_header("Access-Control-Allow-Credentials: true");
        $this->output->set_status_header('200');
        $this->output->set_header("Content-Type: text/plain charset=UTF-8");
        
        $this->output->set_output(json_encode($r));
    }

    public function clientlogin($responseString, $userName, $userGuid) {
        
        log_message('debug', '--------------------');
        log_message('debug', 'ikcomponeer->clientlogin');
        
        $userName = urldecode($userName);
        log_message("debug","decoded username: " . $userName);
        
        $this->load->model("Session");
        $valid = $this->Session->validateResponseString($responseString);
        if ($valid === true) {
            $this->Session->setNpoUser($userName, $userGuid);
        }
        $r = new stdClass();
        $r->exit = 0;
        $r->s = $this->Session->getStatus();
        
        $this->output->set_header("Access-Control-Allow-Origin: " . $this->input->get_request_header('Origin', TRUE));
        $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
        $this->output->set_header("Access-Control-Allow-Credentials: true");
        $this->output->set_status_header('200');
        $this->output->set_header("Content-Type: text/plain charset=UTF-8");
        
        echo json_encode($r);
    }
    public function openTemplateForUser($responseString,$templateCompositionId,$userId){
      log_message("debug","ikcomponeer->openTemplateForUser");
      $this->load->model("Session");
      $valid = $this->Session->validateResponseString($responseString);
      if($valid !== true){
        log_message("debug","login failed: " . $valid);
        echo "FAIL TO LOGIN: " . $valid;
        return;
      }
      log_message("info","logging in the client authenticated user id " . $userId);
      $this->Session->setNpoUser("",$userId);
      $u = $this->Session->getUser();
      $t = $this->Session->refreshToken();
      log_message("info","Session has user " . $u->id);
      log_message("info","loading template for composition " . $templateCompositionId);
      $comp = R::load("composition", $templateCompositionId);
      
      $template = $comp->template;
      $this->load->model("mcomposition");
      $conf_override = $this->mcomposition->settingsForTemplate($comp->template->sharedConf);
      
      log_message("info","template is " . $comp->template->id);
      if($template->template){
        $template = $template->template;
        $comp = $template->composition;
      }
      $onePerTemplate = R::find("composition"," user_id = ? AND template_id = ?", array($u->id, $template->id));
      if(count($onePerTemplate) != 0){
        $comp = reset($onePerTemplate);
        log_message("info","user already has one of this template, return it: " . $comp->id);
      }else{
        log_message("info","user does not yet own a composition for this template, return template original");
      }
      
      echo json_encode(
              array(
                  "recent_cp"=>$comp->id,
                  "user"=>$u,
                  "token"=>$t,
                  "conf_override"=>$conf_override
              ));
    }
    
}

?>