<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stats extends CI_Controller {
	
	public function RmpLvNnnfdWsXbXjUpUz(){
    $this->load->model("Session");
    $d = json_decode($_POST['json']);
    log_message('info', 'TRACEPOST: ' .$d->message);
  }
  
  public function index()
	{
		exit("there is a problem in the controller stats");
    
    $this->load->helper('url');
		$data->project_url = $this->config->item('project_url');
		$data->project = $this->config->item('project');
		
		$this->load->model("User");
		
		$sessionStartTimes = $this->User->getSessionStartTimesForUsersOfClient($this->config->item('project_client_id'));
		
		
		// we hebben nu alle sessies van alle gebruikers van deze client
		
		function create_empty_array_entry(){
			$entry = new stdClass();
			$entry->authenticated_users = 0;
			$entry->stored_compositions = 0;
			$entry->published_compositions = 0;
			return $entry;
		}
		
		// nu gaan we de sessies per maand indelen omdat elke sessie een timestamp heeft
		$data->months = new stdClass();
		
		$numResults = count($sessionStartTimes);
		$iter = 0;
		while($iter < $numResults){
			$m = date("Y-m", $sessionStartTimes[$iter]);
			if(!property_exists($data->months,$m)){
				$data->months->{$m} = create_empty_array_entry();
			}
			$data->months->{$m}->authenticated_users++;
			$iter++;
		}
		
		$compositionTimes = $this->User->getCompositionCreatedAndPublishedTimesForUsersOfClient($this->config->item('project_client_id'));
		
		// we hebben nu de authenticated visitors, dan nu nog de compositions stored waaronder de compositions published
		
		$numResults = count($compositionTimes);
		$iter = 0;
		while($iter < $numResults){
			$m = date("Y-m", $compositionTimes[$iter][0]);
			if(!property_exists($data->months,$m)){
				$data->months->{$m} = create_empty_array_entry();
			}
			$data->months->{$m}->stored_compositions++;
			if($compositionTimes[$iter][1]){
				$data->months->{$m}->published_compositions++;
			}
			$iter++;
		}
		log_message('debug','stats controller');
		$this->load->view('stats',$data);
	}
}
