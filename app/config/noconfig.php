<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This config file is loaded if the "manage" controller is selected - it will have no configured project.
 * The config file will be loaded BEFORE the normal config file (because of the merge direction).
 * So here, you need to define what you want to "override" for the case we are in the "none" config.
 */
$config['log_threshold'] = 5;
$config['log_path']                     = FCF_LOG_DIR         . "/" . FCF_CONF . "/";