#!/bin/bash

#
# SAY HELLO
#
clear
echo "Program starts now."

echo "Hello, $USER"
echo


#
# USER MUST BE ROOT
#
if [ $USER = "root" ]; then
  echo Running the program as root? OK, continue...
else
  echo ERROR - the program should be run as root. Use sudo to run this program!
  exit 3
fi

#
# ARE WE IN THE RIGHT DIRECTORY
#
CURRENT_WORKING_DIR=$(pwd)
echo We are in the directory $CURRENT_WORKING_DIR

CURRENT_WD_PATH=$(echo $CURRENT_WORKING_DIR | sed "s/\(.*\)app$/\1/")
echo Path is $CURRENT_WD_PATH

PLEN=${#CURRENT_WD_PATH}
NOPATH=${CURRENT_WORKING_DIR:PLEN}
echo Without path: $NOPATH

if [ $NOPATH = "app" ]; then
  echo The directory is named \'app\', continue...
else
  echo The directory has not the expected name
  exit 4
fi

#
# GET THE PATH OF THE ASSUMED WEB DIR
#
WEB_DIR_PATH=${CURRENT_WD_PATH:0:PLEN-1}
echo Web dir: $WEB_DIR_PATH
echo

#
# GET THE NAME OF THE PHP USER
#
echo The program wants to change the ownership of the ikcomponeer root directory \($WEB_DIR_PATH\) to the system user of the PHP thread. If you do not know the exact name of this user, please exit and run the whoami.php script in the web root directory. Type \'exit\' to exit or \'cont\' to continue...
read EXIT_ON_USER_QUESTION

if [ $EXIT_ON_USER_QUESTION != "cont" ]; then
  echo You didn\'t type \'cont\', exiting...
  exit 0
fi

echo
echo Please type the name of the system user of the PHP thread:
read PHP_USER
echo
echo You told us that the system user of the PHP thread is named \'$PHP_USER\'. Is this correct? \(type \'y\' or \'n\'\)
read PHP_USER_CONFIRM

if [ $PHP_USER_CONFIRM != "y" ]; then
  echo To try again, restart the program. Bye!
  exit 0;
fi

#
# GET THE NAME OF THE HUMAN OWNER
#
echo The program wants to set the ownership of code files like PHP, CSS and JS to YOU so that you can edit these files more easily. If you are not sure about your own username, type \'whoami\' in the command line. If you want to exit, type \'exit\' and to continue, type \'cont\'...
read EXIT_ON_USERNAME

if [ $EXIT_ON_USERNAME != "cont" ]; then
  echo You didn\'t type \'cont\', exiting...
  exit 0
fi

echo
echo Please type your own username:
read HUMAN_USER
echo
echo You told us that your username is \'$HUMAN_USER\'.
echo Is this correct? \(type \'y\' or \'n\'\)
read HUMAN_USER_CONFIRM

if [ $HUMAN_USER_CONFIRM != "y" ]; then
  echo To try again, restart the program. Bye!
  exit 0;
fi



echo
echo Registering all files to ownership of the server php user \'$PHP_USER\'
echo ...

chown \-R $PHP_USER $WEB_DIR_PATH

echo Regestering css, js and php files to ownership of \'$HUMAN_USER\'
echo ...

NG_DIR="ng"
JS_DIR="data/js"
PHP_DIR="app"
CSS_DIR="data/css"
SWF_DIR="data/swf"

chown \-R $HUMAN_USER $CURRENT_WD_PATH$NG_DIR
chown \-R $HUMAN_USER $CURRENT_WD_PATH$JS_DIR
chown \-R $HUMAN_USER $CURRENT_WD_PATH$PHP_DIR
chown \-R $HUMAN_USER $CURRENT_WD_PATH$CSS_DIR
chown \-R $HUMAN_USER $CURRENT_WD_PATH$SWF_DIR

echo Emptying the directory at data/img
echo ...

echo
echo END OF PROGRAM
exit 0
