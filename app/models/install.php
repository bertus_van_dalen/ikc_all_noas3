<?php 

class Install extends CI_Model{ 
  
  private $DB_CF_FILE;
  private $DB_CF_DIR;
  private $_conn;
  
  function __construct() {
    parent::__construct();
    $this->DB_CF_FILE = FCF_PRIV_CACHE_DIR . '/dbconf/db_conf.json';
    $this->DB_CF_DIR = FCF_PRIV_CACHE_DIR . '/dbconf/';
    $this->ADMINS_DIR = FCF_PRIV_CACHE_DIR . '/admins/';
    $this->ADMIN_EDITED_FILE = FCF_PRIV_CACHE_DIR . '/admins/admin_edited.json';
    $this->PROJECTS_FILE = FCF_PRIV_CACHE_DIR . '/projects.phpd';
    if (!is_dir($this->DB_CF_DIR)) {
      if (!is_dir(FCF_DATA_DIR) && !is_writable(FCPATH)) {
        $usr = exec('whoami');
        $fcpath = substr(FCPATH, strlen(FCPATH) -1) == '/' ? substr(FCPATH, 0, strlen(FCPATH) -1) : FCPATH;
        $rootDirName = substr($fcpath, strrpos($fcpath, '/') + 1);
        $m = "You must make the server the owner of the webroot!";
        $m .= " Go to the website directory by typing 'cd " . $fcpath . "' (enter) ";
        $m .= "and type 'cd ..' (enter) and then 'sudo chown ".$usr.":".$usr." " . $rootDirName . "' (enter)";
        show_error($m);
      }
      if(!is_dir(FCF_DATA_DIR) && false === mkdir(FCF_DATA_DIR)){
        show_error("could not make dir " . FCF_DATA_DIR);
      }
      if (false === mkdir($this->DB_CF_DIR, 0777, true)) {
        show_error("could not make dir " . $this->DB_CF_DIR);
      }
      if (false === file_put_contents($this->DB_CF_DIR . '.htaccess', "deny from all")) {
        show_error("could not secure dir " . $this->DB_CF_DIR);
      }
    }
    if (!is_dir($this->ADMINS_DIR)) {
      if (false === mkdir($this->ADMINS_DIR, 0777, true)) {
        show_error("could not make dir " . $this->ADMINS_DIR);
      }
      if (false === file_put_contents($this->ADMINS_DIR . '.htaccess', "deny from all")) {
        show_error("could not secure dir " . $this->ADMINS_DIR);
      }
    }
  }
  public function isAuthorizedAdmin(){
    $this->_auth();
  }
  public function button(){
    // researched how to export related shared beans
//    $house = R::dispense('aahouse');
//    $house->ownAawindow[] = R::dispense('aawindow');
//    $view = R::dispense('aaview');
//    $view->has = "tree";
//    $view2 = R::dispense('aaview');
//    $view2->has = "street";
//    $house->ownAawindow[0]->sharedAaview[] = $view;
//    $house->ownAawindow[0]->sharedAaview[] = $view2;
//    $house->ownAawindow[] = R::dispense('aawindow');
//    $house->ownAawindow[1]->sharedAaview[] = $view;
//    $house->ownAawindow[1]->sharedAaview[] = $view2;
//    R::store($house);
    $houses = array(R::load("aahouse",2));
    R::preload($houses, 'ownAawindow|aawindow,*.sharedAaview|aaview');
    show_error('<pre>' . print_r(R::beansToArray($houses),true) . '</pre>');
  }
  public function selectDbConfig($filename){
    $this->_auth();
    $realDbConfigFile = $this->_validateDbConfigFileName($this->DB_CF_DIR . $filename);
    copy($realDbConfigFile, $this->DB_CF_FILE);
    return $this->_dbConfViewModel();
  }
  public function deleteDbConfig($filename){
    $this->_auth();
    $realDbConfigFile = $this->_validateDbConfigFileName($this->DB_CF_DIR . $filename);
    if(file_get_contents($realDbConfigFile) === file_get_contents($this->DB_CF_FILE)){
      unlink($this->DB_CF_FILE);
    }
    unlink($realDbConfigFile);
    return $this->_dbConfViewModel();
  }
  public function selectAdmin($filename){
    $this->_auth();
    $realAdminFile = $this->_validateAdminFileName($this->ADMINS_DIR . $filename);
    copy($realAdminFile, $this->ADMIN_EDITED_FILE);
    return $this->_adminViewModel();
  }
  public function deleteAdmin($filename){
    $this->_auth();
    $realAdminFile = $this->_validateAdminFileName($this->ADMINS_DIR . $filename);
    $deletedAdminFileContents = file_get_contents($realAdminFile);
    $deletedAdminObj = json_decode($deletedAdminFileContents);
    if($deletedAdminObj->name === $this->_getAdminName()) { show_error("cannot delete the current admin"); }
    if(file_get_contents($this->ADMIN_EDITED_FILE) === $deletedAdminFileContents){
      unlink($this->ADMIN_EDITED_FILE);
    }
    unlink($realAdminFile);
    return $this->_adminViewModel();
  }
  public function Status($post = null){
    return $this->_viewModel();
  }
  public function installDirs($dirs){
    $this->_auth();
    foreach($dirs as $dir){
      $this->_installDir($dir);
    }
  }
  public function setDbCredentials($db){
    $this->_auth();
    $dbv = $this->_validateDbCredentials($db);
    $this->_mustConnect($dbv["hostname"], $dbv["username"], $dbv["password"], $dbv["database"]);
    if(array_key_exists("test", $dbv) && $dbv["test"]) { 
      return $this->_dbConfViewModel();
    }
    unset($dbv["test"]);
    $this->_storeDbCredentials($dbv);
    return $this->_dbConfViewModel();
  }
  public function writeAdmin($admin){
    if($this->countAdministrators() > 0 ) { $this->_auth(); }
    $adminValidatedInput = $this->_validateAdmin($admin);
    $CI =& get_instance();
    $CI->load->model("Session");
    $CI->Session->trySetFirstAdmin($adminValidatedInput["name"], $this);
    $adminValidatedInput["file"] = $this->_storeAdminToOptions($adminValidatedInput);
    $this->selectAdmin($adminValidatedInput["file"]);
    return $this->_adminViewModel();
  }
  public function countAdministrators(){
    return count($this->_getAdministrators());
  }
  public function runDbBackup($fl){
    $this->_auth();
    $dc = json_decode(file_get_contents($this->DB_CF_FILE));
    $this->_importDb($fl, $dc->hostname, $dc->username, $dc->password, $dc->database);
    return $this->_dbConfViewModel();
  }
  public function dropTables(){
    $this->_auth();
    $tables = $this->_listTables();
    $queries = array_map(function($v){ return "DROP TABLE " . $v["tableName"]; }, $tables);
    $dc = json_decode(file_get_contents($this->DB_CF_FILE));
    $this->_prepareAndRunQueries($queries, $dc->hostname, $dc->username, $dc->password, $dc->database);
    return $this->_dbConfViewModel();
  }
  public function adminNameHasPassword($name, $pass){
    if(false === ($adminData = $this->_loadAdminDataForName($name)) ){ show_error("unknown admin: " . $name); }
    if($adminData->password !== $pass){ show_error("invalid password"); }
    return true;
  }
  public function syncProjects(){
    $this->_auth();
    $this->_syncProjectsCache();
    $n = $this->_getProjectNames();
    return array(
        "projects" => $this->_listProjects()
    );
  }
  public function getProjectForEdit($d){
    if(!isset($d['name'])){ show_error("project name not set"); }
    if(!($p = R::find( 'project', ' name = ? ', array($d['name'])))){ show_error("no project exists named " . $d['name']);}
    R::preload($p, 'client|client,sharedDomain|domain,domain|domain,ownPlaylist|playlist,*.sharedProject|project');
    $projectArray = reset(R::beansToArray($p));
    if($projectArray["ownPlaylist"]){
      $playlistId = $projectArray["ownPlaylist"][0]["id"];
      $entriesCount = R::count( 'composition_playlist', ' playlist_id = ? ', array($playlistId) );
      $projectArray["ownPlaylist"][0]["count"] = $entriesCount;
    }
    return $projectArray;
  }
  public function writeProject($projectData){
    $this->_auth();
    $this->_validateWriteProjectData($projectData);
    $p = isset($projectData['id']) ? R::load('project', $projectData['id']) : R::dispense( 'project' );
    if($p->id === 0 && !isset($projectData['id'])){
      $p->name = $projectData['name'];
      $p->client = R::load('client', $projectData['client']['id']);
    }
    $p->title = $projectData['title'];
    $p->domain = reset(R::findOrDispense('domain', ' baseurl = ? ', array($projectData['domain']['baseurl'])));
    if($p->domain->id === 0) { $p->domain->baseurl = $projectData['domain']['baseurl']; }
    if(isset($projectData['sharedDomain'])){
      $p->sharedDomain = array();
      foreach($projectData['sharedDomain'] as $k => $v){
        $p->sharedDomain[] = R::load('domain', $v['id']);
      }
    }
    $p->cbmode = $projectData['cbmode'];
    $p->logthresh = $projectData['logthresh'];
    if(isset($projectData['ownPlaylist'])){ $this->_storeOwnPlaylistToProjectBean($projectData['ownPlaylist'][0], $p); }
    R::store($p);
    $this->_syncProjectsCache();
    return $this->_projectsViewModel();
  }
  public function addPlaylistForProjectId($id){
    $project = R::load('project',$id);
    if(!($project->id)){ show_error("no project found for id " . $id); }
    $beans = R::findAll('playlist', ' project_id = ? ', array($id));
    if(count($beans) > 0){ show_error("project " . $id . " already has a playlist. It can have only one.");}
    $pl = R::dispense('playlist');
    $pl->project = $project;
    R::store($pl);
  }
  public function buildPlaylistForProjectId($id){
    
    // 1. get the playlist of this project
    $pl = $this->_playlistForProjectId($id);
    // 2. get the ids of the projects that feed this project's playlist
    $inputProjectIds = array_map(function($v){ return $v['id']; }, $pl->sharedProject);
    // 3. get the styles of the compositions of these projects
    $q  = "SELECT DISTINCT s.id ";
    $q .= "FROM composition c ";
    $q .= "JOIN template t ON c.template_id = t.id ";
    $q .= "JOIN subset ss ON ss.id = t.subset_id ";
    $q .= "JOIN style s ON ss.style_id = s.id ";
    $q .= "WHERE c.project_id IN (?) ";
    $r = R::getAll($q, array(implode(',', $inputProjectIds)));
    $presentStyleIds = array_map(function($v){ return $v['id']; }, $r);
    
    // 4. if one of the styles is mentioned in cclimits 
    // AND none of these records associates the style with this project,
    // then it is forbidden for the style to appear in this playlist
    $q  = "SELECT ccl.id FROM cclimit ccl ";
    $q .= "WHERE ccl.style_id IN(?) ";
    $q .= "AND ccl.project_id != ? ";
    $r = R::getAll($q, array(implode(',',$presentStyleIds), $id));
    $forbiddenStyleIds = array_map(function($v){ return $v['id']; }, $r);
    $allowedStyleIds = array_filter($presentStyleIds, function($v) use ($forbiddenStyleIds) { return !in_array($v, $forbiddenStyleIds); });
    
    // 5. get raw connection
    $c = $this->_tryConnect();
    
    // 6. delete all the current entries for the playlist
    $delQ = "DELETE FROM composition_playlist WHERE playlist_id = " . $pl->id;
    $c->query($delQ);
    
    // 7. prepare statement to insert all at once, all entries to the playlist
    $q  = "INSERT INTO composition_playlist ( composition_id, playlist_id ) "; 
    $q .= "SELECT c.id, ? ";
    $q .= "FROM composition c ";
    $q .= "JOIN template t ON c.template_id = t.id ";
    $q .= "JOIN subset ss ON t.subset_id = ss.id ";
    $q .= "JOIN style s ON s.id = ss.style_id ";
    $q .= "WHERE c.project_id IN (?) ";
    $q .= "AND s.id IN (?) ";
    
    // FILL SELECTION VALUES FOR THE WHERE-IN CLASES
    $vals = array($pl->id, implode(',', $inputProjectIds), implode(',', $allowedStyleIds));
    
    // DO THE REPLACEMENT AND THE ESCAPING
    $qr =  vsprintf(str_replace("?", "%s", $q), $vals);
    $esc = $c->real_escape_string($qr);
    
    // WRITE IT TO THE DB
    $c->query($esc);
  }
  private function _playlistForProjectId($id){
    $beans = R::findAll('playlist', ' project_id = ? ', array($id));
    if(count($beans) > 1){ show_error("project " . $id . " can have only one playlist");}
    if(count($beans) === 0){ show_error("project " . $id . " has no playlist");}
    return reset($beans);
  }
  public function createDomain($d){
    $this->_auth();
    if(!isset($d['baseurl'])){ show_error("baseurl not set"); }
    $this->_validateDomainName($d['baseurl']);
    if(R::find( 'domain', ' baseurl = ? ', array($d['baseurl']))){ show_error("there is already a domain with baseurl " . $d['baseurl']);}
    $b = R::dispense('domain');
    $b->baseurl = $d['baseurl'];
    R::store($b);
    return array(  "domains" => $this->_listDomains() );
  }
  private function _getAdmin(){
    $CI =& get_instance();
    $CI->load->model("Session");
    return $CI->Session->getAdmin();
  }
  private function _getAdminName(){
    $admin = $this->_getAdmin();
    return $admin["admin"]["name"];
  }
  private function _isAuth(){
    $n = $this->_getAdminName();
    return gettype($n) === 'string' && !empty($n);
  }
  private function _auth(){
    if(!$this->_isAuth()) { show_error("permission problem"); }
    return true;
  }
  private function _validateDomainName($v)
  { 
    if(!filter_var($v, FILTER_VALIDATE_URL)){ show_error("invalid domain: " . $v); }
  }
  private function _dbConfViewModel(){
    return array(
        "db_confs" => $this->_listDbConfs(), 
        "db_conf" => $this->_getDbConf(), 
        "tables" => $this->_listTables()
            );
  }
  private function _adminViewModel(){
    return array(
        "administrators" => $this->_listAdministrators(), 
        "authorized" => $this->_isAuth(), 
        "admin_edit" => $this->_retrieveAdminFromFile(),
        "currentAdmin" => $this->_getAdmin()
            );
  }
  private function _projectsViewModel(){
    $projects = $this->_listProjects();
    return array(
        "projects" => $projects,
        "installDirsByProject" => $this->_installDirsForProjects($projects),
        "clients" => $this->_listClients(),
        "domains" => $this->_listDomains(),
        "log_levels" => $this->log->getLevels(),
        "cb_modes" => $this->_listCbModes()
            );
  }
  public function getAdmins(){
    return $this->_adminViewModel();
  }
  private function _validateDbConfigFileName($fl){
    $realDbConfigFile = realpath($fl);
    if(false === strpos($realDbConfigFile, $this->DB_CF_DIR)) { show_error("invalid path: " . $realDbConfigFile); }
    if(!file_exists($realDbConfigFile)) { show_error("inexistent db config: " . $realDbConfigFile); }
    return $realDbConfigFile;
  }
  private function _validateAdminFileName($f){
    $realp = realpath($f);
    if(false === strpos($realp, $this->ADMINS_DIR)) { show_error("invalid path: " . $realp); }
    if(!file_exists($realp)) { show_error("inexistent admin: " . $realp); }
    return $realp;
  }
  private function _loadAdminDataForName($name){
    $files = $this->_getAdministrators();
    $adminData = null;
    foreach($files as $f){
      $iterData = json_decode(file_get_contents($this->ADMINS_DIR . $f));
      if($iterData->name === $name){
        $adminData = $iterData;
        break;
      }
    }
    return $adminData !== null ? $adminData : false;
  }
  private function _listCbModes(){
    return array("Ymd", "YmdHis");
  }
  private function _isValidCbMode($val){
    return in_array($val, $this->_listCbModeS());
  }
  private function _arrErr($r){
    show_error('<pre>'.print_r($r,true).'</pre>');
  }
  private function _validateWriteProjectData($d){
    if(!($this->_tableExists('client') && $this->_tableExists('project'))) { show_error("tables must be present for client and project"); }
    if(!isset($d['name'])) { show_error("no name"); }
    if(!preg_match('/^[_a-zA-Z0-9]*$/iD', $d["name"])) { show_error("invalid argument, project name has illegal characters");}
    if(!isset($d['id']) && R::find( 'project', ' name = ? ', array($d['name']))){ show_error("there is already another project named " . $d['name']);}
    if(isset($d['id']) && R::load('project',$d['id'])->id === 0) { show_error("no project exists for id " . $d['id']); }
    if(!isset($d['client'])){ show_error('no client'); }
    if(R::load('client',$d['client']['id'])->id === 0) { show_error("no client exists for id " . $d['client']['id']); }
    if(!isset($d['title'])) { show_error("no title"); }
    if(strlen($d['title']) < 1) { show_error("the title cannot be empty"); }
    if(isset($d['sharedDomain'])){ $this->_validateDomains($d['sharedDomain']); }
    if(!isset($d['logthresh'])){ show_error("no log level was set"); }
    if(!$this->log->isValidLogLevel($d['logthresh'])){ show_error("invalid log level: " . $d['logthresh']); }
    if(!isset($d['cbmode'])){ show_error("no cache buster mode (cbmode) was set"); }
    if(!$this->_isValidCbMode($d['cbmode'])) { show_error('invalid cache buster mode (cbmode): ' . $d['dbMode']); }
    if(isset($d['ownPlaylist'])){ $this->_validateProjectPlaylist(reset($d['ownPlaylist'])); }
  }
  private function _validateProjectPlaylist($pl){
    foreach($pl["sharedProject"] as $incProj){
      if(R::load('project',$incProj['id'])->id === 0){ show_error("invalid project included in playlist for project id " . $incProj['id']); }
    }
  }
  private function _storeOwnPlaylistToProjectBean($playlistDataToBeStored, $projectBean){
    if(!($projectBean->ownPlaylist)){ show_error("the project does not have a playlist (project id " . $projectBean->id . ")"); }
    $pl = reset($projectBean->ownPlaylist);
    $entriesToBeStored = array_map(function($v){ return $v['id']; }, $playlistDataToBeStored['sharedProject']);
    $pl->sharedProject = array();
    foreach($entriesToBeStored as $id){ $pl->sharedProject[] = R::load('project', $id); }
    R::store($pl);
  }
  
  private function _validateDomains($ds){
    if(!is_array($ds)){ show_error("domains are not of type array"); }
    foreach($ds as $k => $v){
      if(!isset($v['id'])) { show_error("no id for domain"); }
      if(R::load('domain',$v['id'])->id === 0) { show_error("no domain in database for id " . $v['id']); }
    }
  }
  private function _getDbConf(){
    if(!is_file($this->DB_CF_FILE)) { return false; }
    $c = json_decode(file_get_contents($this->DB_CF_FILE));
    unset($c->password);
    return $c;
  }
  private function _retrieveAdminFromFile(){
    if(!is_file($this->ADMIN_EDITED_FILE)) { return false; }
    $c = json_decode(file_get_contents($this->ADMIN_EDITED_FILE));
    $c->password = "";
    return $c;
  }
  private function _storeDbCredentials($cred){
    $cred["file"] = $this->_storeDbCredentialsToOptions($cred);
    $this->_storeDbCredentialsToCurrentConfig($cred);
  }
  private function _storeDbCredentialsToCurrentConfig($cred){
    if(false === file_put_contents($this->DB_CF_FILE,json_encode($cred))){ show_error("could not write to " . $this->DB_CF_FILE); }
  }
  private function _storeDbCredentialsToOptions($cred){
    if(!isset($cred["file"])){ $cred["file"] = $this->_getNextDbConfigFileName(); }
    if(false === file_put_contents($this->DB_CF_DIR . $cred["file"] ,json_encode($cred))){ show_error("could not write to " . $this->DB_CF_DIR . $cred["file"]); }
    return $cred["file"];
  }
  private function _storeAdminToOptions($a){
    if(isset($a["file"])){
      $adminInFile = json_decode(file_get_contents($this->ADMINS_DIR . $a["file"]));
      if($adminInFile->name !== $a["name"]){
        $a["file"] = $this->_getNextAdminFileName();
      }
    }else{
      $a["file"] = $this->_getNextAdminFileName(); 
    }
    if(false === file_put_contents($this->ADMINS_DIR . $a["file"] ,json_encode($a))){ show_error("could not write to " . $this->ADMINS_DIR . $a["file"]); }
    return $a["file"];
  }
  private function _getNextAdminFileName(){
    $admins = $this->_getAdministrators();
    $max = 0;
    foreach($admins as $a){
      $num = substr($a, 6, strpos($a, '.json') -6 );
      $comp = intval($num);
      if($comp > $max){ $max = $comp; }
    }
    return 'admin_' . ($max + 1) . '.json';
  }
  private function _getNextDbConfigFileName(){
    $dbConfs = $this->_getDbConfs();
    $max = 0;
    foreach($dbConfs as $c){
      $num = substr($c, 8, strpos($c, '.json') -8 );
      $comp = intval($num);
      if($comp > $max){ $max = $comp; }
    }
    return 'db_conf_' . ($max + 1) . '.json';
  }
  private function _mustConnect($h, $u, $p, $d){
    $mysqli = mysqli_init();
    if (!$mysqli) { show_error('mysqli_init failed');}
    if (!$mysqli->options(MYSQLI_INIT_COMMAND, 'SET AUTOCOMMIT = 0')){ show_error('Setting MYSQLI_INIT_COMMAND failed'); }
    if (!$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5)) { show_error('Setting MYSQLI_OPT_CONNECT_TIMEOUT failed'); }
    if (!$mysqli->real_connect($h, $u, $p, $d)) { show_error('Connect Error (' . mysqli_connect_errno() . ') '  . mysqli_connect_error()); }
    return 'Success... ' . $mysqli->host_info . "\n";
  }
  private function _tryConnect(){
    if(isset($this->_conn)){ return $this->_conn; }
    if(!file_exists($this->DB_CF_FILE)){ return false; }
    mysqli_report(MYSQLI_REPORT_STRICT);
    $this->_conn = mysqli_init();
    if(!is_file($this->DB_CF_FILE)){ return false; }
    $dc = json_decode(file_get_contents($this->DB_CF_FILE));
    if (!$this->_conn) { return false;}
    try{ if (!$this->_conn->real_connect($dc->hostname, $dc->username, $dc->password, $dc->database)) { return false; }}
    catch (Exception $e) { return false; }
    return $this->_conn;
  }
  private function _validateDbCredentials($db){
    if(!is_array($db)) show_error("invalid argument, db is not an array");
    if(!array_key_exists('username', $db)) { show_error("invalid argument, db object has no username property");}
    if(!array_key_exists('hostname', $db)) { show_error("invalid argument, db object has no hostname property");}
    if(!array_key_exists('database', $db)) { show_error("invalid argument, db object has no database property");}
    if(!array_key_exists('password', $db)) { show_error("invalid argument, db object has no password property");}
    if (!preg_match('/^[_a-zA-Z0-9]*$/iD', $db["username"])) { show_error("invalid argument, username has illegal characters");}
    if (!preg_match('/^[\/:\.a-zA-Z0-9]*$/iD', $db["hostname"])) { show_error("invalid argument, hostname has illegal characters");}
    if (!preg_match('/^[a-zA-Z0-9_]*$/iD', $db["database"])) { show_error("invalid argument, database has illegal characters");}
    if (!preg_match('/^[a-zA-Z0-9]*$/iD', $db["password"])) { show_error("invalid argument, password has illegal characters");}
    if(array_key_exists('file', $db)){ if(!preg_match('/^db_conf_\d+\.json$/i', $db["file"])) { show_error("invalid filename: " . $db["file"]); }}
    return $db;
  }
  private function _validateAdmin($a){
    if(!is_array($a)){ show_error("invalid argument, admin is not an array");}
    if(!array_key_exists('name', $a)) { show_error("invalid argument, admin array has no name property");}
    if(!array_key_exists('password', $a)) { show_error("invalid argument, admin array has no password property");}
    if(!preg_match('/^[a-zA-Z0-9]*$/iD', $a["name"])) { show_error("invalid argument, name has illegal characters");}
    if(!preg_match('/^[a-zA-Z0-9]*$/iD', $a["password"])) { show_error("invalid argument, password has illegal characters");}
    if(array_key_exists('editFile', $a)){ if(!preg_match('/^admin_\d+\.json$/i', $a["file"])) { show_error("invalid filename: " . $a["file"]); }}
    return $a;
  }
  private function _viewModel(){
    if(!$this->_isAuth()){
      return array();
    }
    $viewModel["directories"] = $this->_getDirectoriesWithStatuses();
    $viewModel["db_backups"] = $this->_listDbBackups();
    $withDbs = array_merge($viewModel, $this->_dbConfViewModel());
    $withProjects = array_merge($withDbs, $this->_projectsViewModel());
    return $withProjects;
  }
  private function _getAdministrators(){
    $dir = $this->ADMINS_DIR;
    $allFiles = scandir($dir);
    $res = preg_grep('/^admin_\d+\.json$/i', $allFiles);
    return $res;
  }
  private function _listDbBackups(){
    $dir = FCPATH . 'db_install';
    return is_dir($dir) ? preg_grep('/^.*\.sql$/i', scandir($dir)) : array();
  }
  private function _getDbConfs(){
    $confsDir = $this->DB_CF_DIR;
    $allFiles = scandir($confsDir);
    $dbConfs = preg_grep('/^db_conf_\d+\.json$/i', $allFiles);
    return $dbConfs;
  }
  private function _listDbConfs(){
    $dbConfs = $this->_getDbConfs();
    $r = array();
    foreach( $dbConfs as $conf ){
      $confVals = json_decode(file_get_contents($this->DB_CF_DIR . $conf));
      $r[] = array("file"=>$conf, "username" => $confVals->username, "database" => $confVals->database, "hostname" => $confVals->hostname);
    }
    return $r;
  }
  private function _listAdministrators(){
    $adms = $this->_getAdministrators();
    $r = array();
    foreach( $adms as $a ){
      $a = json_decode(file_get_contents($this->ADMINS_DIR . $a));
      unset($a->password);
      $r[] = $a;
    }
    return $r;
  }
  private function _getDirectoriesWithStatuses(){
    $out = array();
    $dirs = $this->config->item("fcf_install_dirs");
		foreach($dirs as $dir){
			$out[] = array("dir" => $dir, "exists" => is_dir($dir));
		}
    return $out;
  }
  private function _tableExists($tableName){
    if (false === $mysqli = $this->_tryConnect()) {
      return false;
    }
    $q = $mysqli->query("SHOW TABLES");
    while ($r = $q->fetch_assoc()) {
      if ($r[key($r)] === $tableName) {
        return true;
      }
    }
    return false;
  }
  private function _listTables(){
    if(false === $mysqli = $this->_tryConnect()) { return false; }
    $res = $mysqli->query("SHOW TABLES");
    $out = array();
    while ($tab = $res->fetch_assoc())
    {
        $out[] = array("tableName"=>$tab[key($tab)],"selected"=>false);
    }
    $this->_syncProjectsCache(); 
    return $out;
  }
  private function _syncProjectsCache() {
    if (false === $mysqli = $this->_tryConnect()) { show_error("_syncProjectsCache could not connect"); }
    if(false === $res = $mysqli->query("SELECT name FROM project")){
      $this->_deleteProjectCache();
      return false;
    }
    $p = array();
    while ($rw = $res->fetch_assoc()) { $p[] = $rw['name']; }
    if (false === file_put_contents($this->PROJECTS_FILE, serialize($p))) { show_error("could not write to " . $this->PROJECTS_FILE); }
  }

  private function _deleteProjectCache(){
    $f = $this->PROJECTS_FILE;
    if(is_file($f)) {
      if(false === unlink($f)){
        show_error("could not delete projects file " . $f);
      }
    }
  }
  private function _getProjectNames(){
    $f = $this->PROJECTS_FILE;
    if(!is_file($f)){ return false; }
    $p = unserialize(file_get_contents($f));
    return $p;
  }
  private function _listClients(){
    if(!$this->_tableExists('client')) { return false; }
    $clients = R::findAll('client');
    return R::beansToArray($clients);
  }
  private function _listDomains(){
    if(!$this->_tableExists('domain')){ return false; }
    $domains = R::findAll('domain');
    return R::beansToArray($domains);
  }
  private function _listProjects(){
    if(!$this->_tryConnect()){ return array(); }
    $projects = R::findAll('project');
    return R::beansToArray($projects);
  }
  private function _installDirsForProjects($projects){
    $pi = $this->config->item("pr_install_dirs");
    $pa = array();
    foreach($projects as $v){
      $dirs = array();
      foreach($pi as $d){
        $dirs[] = $this->_getProjectDirectoryData($d, $v["name"]);
      }
      $pa[$v["name"]] = array("install_dirs" => $dirs);
    }
    return $pa;
  }
  private function _getProjectDirectoryData($dirForNone, $projName){
    $dir = str_replace('/none/', '/' . $projName . '/', $dirForNone);
    $x = is_dir($dir);
    return array("dir" => $dir, "exists" => $x);
  }
  private function _pathIsValidForAnyProject($path, $projectNames){
    $pi = $this->config->item("pr_install_dirs");
    foreach($pi as $d){
      foreach($projectNames as $n){
        $dirdata = $this->_getProjectDirectoryData($d, $n);
        if($path === $dirdata["dir"]){
          return true;
        }
      }
    }
    return false;
  }
  private function _isCreatableDirectory($path){
    $dirs = $this->config->item("fcf_install_dirs");
    if(in_array($path, $dirs)){ return true; }
    if(false === $projectNames = $this->_getProjectNames()){ return false; }
    return $this->_pathIsValidForAnyProject($path, $projectNames) && !is_dir($path);
  }
  private function _installDir($path){
    if(!$this->_isCreatableDirectory($path)){
      show_error("invalid directory " . $path);
    }if(is_dir($path)){
      show_error("directory already exists: " . $path);
    }if(false === mkdir($path, 0777, true)){
      show_error("could not create dir " . $path);
    }
  }
  private function _prepareAndRunQueries($queries, $mysql_host, $mysql_username, $mysql_password, $mysql_database){
    $opt = array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION
    );
    $db = new PDO("mysql:dbname=".$mysql_database.";host=".$mysql_host, $mysql_username, $mysql_password, $opt );
    foreach($queries as $q){
      $s = $db->prepare($q);
      try{
        $s->execute();
      }catch(Exception $e){
        
      }
    }
  }
  private function _importDb($filename, $mysql_host, $mysql_username, $mysql_password, $mysql_database) {
    $f = $f = $this->_getFileForDbImport($filename);
    $e = "mysql --host=" . $mysql_host . " --user=" . $mysql_username . " --password=" . $mysql_password . " -D " . $mysql_database . " < " . $f . " 2>&1";
    $output = exec($e);
    if(trim($output) != ""){
      show_error($output);
    }
  }
  private function _getFileForDbImport($f){
    $this->_validateDbImportFile($f);
    $dir = FCPATH . 'db_install';
    return $dir . '/' . $f;
  }
  private function _validateDbImportFile($f){
    $dir = FCPATH . 'db_install';
    $files = preg_grep('/^.*\.sql$/i', scandir($dir));
    if(!in_array($f, $files)){
      show_error("invalid file " . $f);
    }
  }
}