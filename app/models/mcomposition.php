<?php

class MComposition extends CI_Model {
  
  private $ci; // code igniter instance
  private $_mem; // object to keep composition in memory for specific mem_ functions

  /*
    |--------------------------------------------------------------------
    |--------------------------------------------------------------------
    | CONSTRUCTOR
    |--------------------------------------------------------------------
    |--------------------------------------------------------------------
    |
   */

  function __construct() {
    parent::__construct();
    $this->ci = & get_instance();
    if (!( property_exists($this->ci, "Session"))) {
      $this->ci->load->model("Session");
    }
    log_message('debug', 'Composition added to Model');
  }
  public function getComposition($id){ 
    $c = R::load('composition', $id);
    if($c->id === 0){
      show_error("No composition found for id " . $id);
    } 
    return $c->export();
  }
  
  function read($data) {
    log_message('debug', 'Composition->read');
    //
    // IF KEY IS SPECIFIED, RETURN THE COMPOSITION
    //
    if ($data != null) {
      if (property_exists($data, 'id')) {
        log_message('info', 'read composition ' . $data->id);
        $return = array("composition" => null, "environment" => null);
        $cp = R::load("composition",$data->id);
        if(!$cp->id){
          return $return;
        }
        if($this->styleLimited($cp->template->subset->style)){
          log_message("debug","style[" . $cp->template->subset->style->id . "] is not allowed to be read within project[" . FCF_CONF . "]");
          $cp = $this->formatComposition(R::load("composition",5967));
        }else{
          $cp = $this->formatComposition($cp);
        }
        $this->ci->load->Model('Musicbase');
        $return["composition"] = $cp->getProperties();
        $template = $cp->template;
        if(null == $template){
          log_message("error","COMPOSITION[" . $cp->id . "] HAS NO TEMPLATE!!");
        }
        $return["environment"] = $this->ci->Musicbase->environmentForTemplate($template);
        $return["scrollitems"] = $this->getScrollItems($cp);
        $projectSettings = $this->getProjConfs();
        $templateSettings = $this->settingsForTemplate($template->sharedConf);
        $return["conf_override"] = array_merge($projectSettings, $templateSettings);
      }
    }
    //
    // RETURN EMPTY PROJECTS AND USER-OWNED COMPOSITIONS IF NOTHING IS SPECIFIED
    //
    else {
      $return = array("usercompositions" => null);
      $return["scrollitems"] = $this->getScrollItems();
      $return["usercompositions"] = $this->getUserCompositions();
      log_message('info', 'cra');
    }
    return $return;
  }
  public function getProjConfs(){
    $cfs = $this->ci->Session->getConfs();
    $ret = array();
    foreach ($cfs as $c) {
      $confName = "conf_";
      if(is_bool($c->value)){
        $val = $c->value == true;
      } else if (is_int ($c->value)) {
        $val = (int) $c->value;
      } else if (is_float ($c->value)) {
        $val = (float) $c->value;
      } else if (is_string($c->value)) {
        $val = (string) $c->value;
      }
      $confName .= $c->name;
      $ret[$confName] = $val;
    }
    return $ret;
  }
  private function getUserCompositions(){
    $ret = array();
    $u = $this->ci->Session->getUser();
    if($u){
      $uComps = $u->ownComposition;
      foreach ($uComps as $id => $cp) {
        if($this->styleLimited($cp->template->subset->style)){
          continue;
        }
        $cp->content = html_entity_decode($cp->content);
        $ret[] = $cp->getProperties();
      }
      function mostRecent($a, $b) {
        return ($a["created"] < $b["created"]) ? 1 : -1;
      }
      usort($ret, 'mostRecent');
    }
    return $ret;
  }
  public function getScrollItems($cpBean = null){
    
    $added = array();
    $scrollItemBeans = $this->ci->Session->projectBean()->ownScrollitem;
    $ret = array();
    
    foreach($scrollItemBeans as $scrollItem){
      $compID = $scrollItem->template->composition->id;
      if(!$this->styleLimited($scrollItem->template->subset->style) && !array_search($compID, $added)){
        $cp = $scrollItem->template->composition->getProperties();
        $cp["content"] = html_entity_decode($cp["content"]);
        $cp["order"] = $scrollItem->order;
        $ret[] = $cp;
        $added[] = $compID;
      }
    }
    
    if($cpBean != null && !$this->styleLimited($cpBean->template->subset->style) && !array_search($cpBean->id, $added)){
      $cp = $cpBean->getProperties();
      $cp["content"] = html_entity_decode($cp["content"]);
      $cp["order"] = 999;
      $ret[] = $cp;
    }
    return $ret;
  }
  private function styleLimited($style){
    if ($this->Session->userHasRole($this->Session->ROLE_NAME_AGENT)) {
      return false;
    }
    $cclimits = $style->ownCclimit;
    $limited = count($cclimits) > 0;
    if($limited){
      foreach($cclimits as $cclimitId => $cclimit){
        if($cclimit->project->id == $this->ci->Session->projectId()){
          $limited = false;
          break;
        }
      }
    }
    return $limited;
  }
  private function formatComposition($cp) {
    log_message("debug","mcomposition->read/formatComposition getting composition contents properly...");
    if ($this->Session->userHasRole($this->Session->ROLE_NAME_AGENT)) {
      log_message("debug","loading cp content for agent role");
      $cp->content = html_entity_decode($cp->content);
    }else if($this->Session->getUser()->id != $cp->user->id){
      log_message("debug","loading cp content for other user");
      $cp->content = html_entity_decode($cp->content);
    }else{
      log_message("debug","load youngest of autosave or composition itself");
      if(count($cp->ownAutosave)){
        $autosave = reset($cp->ownAutosave);
        log_message("debug","autosave[" . $autosave->id . "] loaded");
        if($autosave->mtime > $cp->mtime){
          log_message("debug","autosave is younger");
          $cp->content = html_entity_decode($autosave->content);
        }else{
          log_message("debug","own composition content is younger");
          $cp->content = html_entity_decode($cp->content);
        }
      }else{
        log_message("debug","no autosave does exist, load composition itself");
        $cp->content = html_entity_decode($cp->content);
      }
    }
    return $cp;
  }
  
  private function _logDie($m){
    $m = "error in mcomposition->write() - " . $m;
    log_message("error",$m);
    show_error($m);
  }

  private function _getUser(){
    $u = $this->Session->getUser();
    if(!is_a($u, "RedBeanPHP\OODBBean")){
      $this->_logDie("session user is not a bean");
    }
    if(!($u->getMeta('type') == "user")){
      $this->_logDie("session getUser returned no bean of type user");
    }
    log_message("debug","session user loaded");
    return $u;
  }
  private function _validateWrite($d){
    $r = new stdClass();
    if(!is_object($d)){ $this->_logDie("can only process object data"); }
    if(!isset($d->id)){ $this->_logDie("missing property id"); }
    if(!is_numeric($d->id)){ $this->_logDie("id is not numeric"); }
    $r->orig = R::load('composition', $d->id);
    if($r->orig->id === 0) { $this->_logDie("must be based on an existent composition for id " . $d->id); }
    if($this->styleLimited($r->orig->template->subset->style)){ $this->_logDie("project has no right for style"); }
    if(isset($d->content)){ 
      $this->load->helper('xml');
      $r->content = xml_convert($d->content);
    }
    if(isset($d->name) && strlen($d->name) > 0){
      $r->name = $d->name;
    }
    $r->published = isset($d->for_publication) && $d->for_publication;
    return $r;
  }
  private function _writeProperties($vd){
    $r = array("written" => array());
    if(isset($vd->name)){
      $vd->orig->name = $vd->name;
      $r['written']['name'] = $vd->name;
    }
    if($vd->orig->published !== $vd->published){
      $vd->orig->published = $vd->published;
      $r['written']['published'] = $vd->published;
    }
    if($vd->orig->template->subset->ownNoshare){
      $vd->orig->published = false;
    }
    $r['written']['id'] = R::store($vd->orig);
    $this->_updatePlaylist($vd->orig);
    return $r;
  }
  private function _write($u, $vd){
    if(isset($vd->content)){
      if(!isset($vd->name)){
        $this->_logDie("cannot update content without name");
      }else{
        return $this->_writeContent($u, $vd);
      }
    }else{
      if($u->id !== $vd->orig->user->id){
        $this->_logDie("cannot update properties without ownership");
      }else{
        return $this->_writeProperties($vd);
      }
    }
  }
  public function duplicate($old, $user, $published = false, $name = '', $content = ''){
    $cp = R::dispense("composition");
    $cp->created = time();
    $cp->mtime = time();
    $cp->hasmp3 = false;
    $cp->pubtime = time();
    if(false !== ($p = $this->Session->projectBean())){
      $cp->project = $p;
    }
    $cp->user = $user;
    $cp->name = $name === '' ? $old->name . "DPL" : $name;
    $cp->template = $old->template;
    $cp->content = $content === '' ? $old->content : $content;
    $cp->published = $published;
    return $cp;
  }
  private function _writeContent($u, $vd){
    if($u->id != $vd->orig->user->id || $vd->name != $vd->orig->name){
      $cp = $this->duplicate($vd->orig, $u, $vd->published, $vd->name, $vd->content);
    } else {
      $cp = $vd->orig;
      if(isset($vd->autoSave) && $vd->autoSave){
        if(count($cp->ownAutosave)){
          $autoSave = reset($cp->ownAutosave);
        }else{
          $autoSave = R::dispense("autosave");
          $cp->ownAutosave[] = $autoSave;
        }
        $autoSave->content = $vd->content;
        $autoSave->mtime = time();
      }else{
        $cp->content = $vd->content;
        $cp->mtime = time();
        $cp->published = $vd->published;
        $cp->hasmp3 = false;
        $cp->empty = false;
        $cp->pubtime = time();
      }
    }
    if($cp->template->subset->ownNoshare){
      $cp->published = false;
    }
	
    $r = array("written" => array("id" => R::store($cp), "name" => $cp->name, "published" => $cp->published));
	  
	// update to blob (candidate to replacement update playlist)
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://mixdowntasks.queue.core.windows.net/tasks/messages?sv=2015-12-11&ss=bfqt&srt=sco&sp=rwdlacup&se=2022-12-13T07:45:45Z&st=2016-12-11T23:45:45Z&spr=https&sig=OahPH6XgCFhJWNGS9ZNGvsUyeFnxRkTBgxsTQYf4VqE%3D",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "<QueueMessage><MessageText>" . "&lt;document&gt;&lt;id&gt;" . $cp->id . "&lt;/id&gt;" . "&lt;mtime&gt;" . time() . "&lt;/mtime&gt;" . $vd->content . "&lt;/document&gt;</MessageText></QueueMessage>",
		CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/x-www-form-urlencoded"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		log_message("error", "cURL Error #:" . $err);
	}
	
    $this->_updatePlaylist($cp);
    return $r;
  }
  private function _updatePlaylist($c){
    $this->load->model('pubxml');
    $this->pubxml->updatePlaylist($c);
  }
  public function write($data) {
    log_message('debug', 'Composition->write');
    $vd = $this->_validateWrite($data);
    $u = $this->_getUser();
    return $this->_write($u, $vd);
  }
  public function settingsForTemplate($settings){
    $r = array();
    foreach($settings as $k => $v){
      $r['conf_' . $v->name] = $v->value;
    }
    return $r;
  }
  public function delete($id) {
    log_message('debug', 'mcomposition->delete(' . $id . ")");
    $cp = R::load('composition', $id);
    if(!$cp->id){
      log_message("error","cannot delete composition that does not exist: " . $id);
      return;
    }
    log_message("debug","composition loaded");
    if($cp->template->composition->id == $cp->id){
      log_message("error","cannot delete composition which is the template: " . $id);
      return;
    }
    $u = $this->Session->getUser();
    if (!($cp->user->id == $u->id)) {
      log_message("error", "composition->delete() - deleted composition not owned");
      return;
    }
    log_message("debug","composition owner: " . $u->id);
    $trash = R::findOne("user"," provtab = ?",array("userprovtrash"));
    if($trash == null){
      log_message("debug","no trash user found, creating trash user...");
      $trash = R::dispense("user");
      $trash->provtab = "userprovtrash";
      R::store($trash);
      log_message("debug","trash user stored: " . $trash->id);
    }else{
      log_message("debug","trash user loaded");
    }
    $cp->user = $trash;
    $cp->mtime = time();
    $cp->published = false;
    $cp->hasmp3 = false;
    $mp3File = $this->config->item('published_dir') . $cp->id . ".MP3";
    if(file_exists($mp3File)){
      log_message("debug","found mp3, deleting " . $mp3File);
      if(false === unlink($mp3File)){
        log_message("error","COULD NOT DELETE " . $mp3File);
      }else{
        log_message("debug","deletion ok");
      }
    }else{
      log_message("debug","no mp3 file present at " . $mp3File);
    }
    R::store($cp);
    return array("deleted" => $id);
  }
  
  /**
   * loads a composition for the memcomp usage
   * @return boolean false if it does not exist / could not be loaded
   */
  public function mem_load($id){
    $this->_mem = R::load("composition",$id);
    if(!$this->_mem->id){
      $this->_mem = null;
      return false;
    }
    return true;
  }
  /**
   * stores the memcomp composition if loaded
   * @param for id integrity
   * may throw
   * - "ikc-mcomp 1" meaning no mem composition loaded
   * - "ikc-mcomp 2" meaning cannot store, wrong id
   */
  public function mem_store($id){
    if(!$this->_mem){
      throw new Exception("ikc-mcomp 1");
      die();
    }
    if(!$this->_mem->id == $id){
      throw new Exception("ikc-mcomp 2");
      die();
    }
    $this->_mem->mtime = time();
    if($this->_mem->template->subset->ownNoshare){
      $this->_mem->published = false;
    }
    R::store($this->_mem);
  }
  
  /**
   * may throw
   * - "ikc-mcomp 1" meaning no mem composition loaded
   *
   * @return bool Wether or not this is a published composition.
   */
  function mem_published(){
    if(!$this->_mem){
      throw new Exception("ikc-mcomp 1");
      die();
    }
    return $this->_mem->published;
  }
  /**
   * may throw
   * - "ikc-mcomp 1" meaning no mem composition loaded
   *
   * @return bool Wether or not this is has an mp3
   */
  function mem_hasmp3(){
    if(!$this->_mem){
      throw new Exception("ikc-mcomp 1");
      die();
    }
    return $this->_mem->hasmp3;
  }
  /**
   * may throw
   * - "ikc-mcomp 1" meaning no mem composition loaded
   * @param boolean $bool set true if there is an mp3 file for this composition, false if there's none
   * @return nothing
   */
  function mem_set_hasmp3($bool){
    if(!$this->_mem){
      throw new Exception("ikc-mcomp 1");
      die();
    }
    $this->_mem->hasmp3 = $bool;
  }
  /**
   * may throw
   * - "ikc-mcomp 1" meaning no mem composition loaded
   * @param boolean $bool to set the published bit
   * @return nothing
   */
  function mem_set_published($bool){
    if(!$this->_mem){
      throw new Exception("ikc-mcomp 1");
      die();
    }
    $this->_mem->published = $bool;
  }
  /**
   * updates pubtime
   * 
   * may throw
   * - "ikc-mcomp 1" meaning no mem composition loaded
   * @return nothing
   */
  function mem_set_pubtime(){
    if(!$this->_mem){
      throw new Exception("ikc-mcomp 1");
      die();
    }
    $this->_mem->pubtime = time();
  }
}

?>
