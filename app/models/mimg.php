<?php

class mImg extends CI_Model {

  function __construct() {
    if(FCF_CONF === 'none') die('no conf');
    parent::__construct();
  }
  /**
   * Will return the file name to a PNG image in the img directory.
   * The file for the name will be derived from the original path.
   * It can be resized. If one of the dimensions is zero,
   * the other dimension will be used to resize and aspect ratio is kept.
   * If both dimensions are zero, the original image size will be used.
   * If both dimensions are nonzero, scaling will be applied.
   * 
   * @param float $w
   * @param float $h
   * @param string $path
   * @return string The name of the file. Use it with the $dir property to get the file. 
   */
  public function get($client, $w, $h, $path){
    
    
    // is the path legal?
    $expectedSuperDir = $this->config->item("clients_dir");
    
    $file = $expectedSuperDir . $client . $path;
    $rfile = realpath($file);
    if(strpos($rfile,$expectedSuperDir) !== 0){
      show_error("file " . $file . " does not exist",404);
    }
    
    // is extension valid?
    $valid_exts = array("png","jpg","jpeg","bmp","svg","gif");
    $ext = substr($rfile, strripos($rfile,".")+1);
    if(!in_array($ext, $valid_exts)){
      show_error("invalid file extension",400);
    }
    
    $imgDir = FCPATH . 'img';
    if(!is_dir($imgDir)){
      mkdir($imgDir, 0777, true);
      file_put_contents($imgDir . '/.htaccess', "AddType image/png .svg .png .jpg .jpeg .bmp .gif");
    }
    
    // load the image
    $identify = exec('identify ' . $rfile . ' 2>&1');
    //idenfity inputfile
    $ispl = explode(" ", $identify);
    $srcType = $ispl[1];
    $srcDim = $ispl[2];
    $dimspl = explode("x",$srcDim);
    $srcWidth = $dimspl[0];
    $srcHeight = $dimspl[1];
    
    if(!in_array(strtolower($srcType),$valid_exts)){
      show_error("no valid format from identify command " . $srcType);
    }
    
    // create the new file path
    $newFile = FCPATH . 'img/get/' . $client . '/' . $w . '/' . $h . $path;
    $newDir = substr($newFile, 0, strrpos($newFile, '/'));
    
    if(!is_dir($newDir)){
      mkdir($newDir, 0777, true);
    }
    
    $newFilePath = $this->getForSize($srcType, $srcWidth, $srcHeight, $w, $h, $rfile, $newFile);
    
    return $this->config->item("base_url") . substr($newFilePath, strlen(FCPATH));
  }
  private function getForSize($srcType, $srcWidth, $srcHeight, $w, $h, $path, $newFile){
    $origPath = $path;
    $targetFile = $newFile;
    $scale = true;
    $dw = 100000;
    $dh = 100000;
    if($w == 0 && $h == 0){
      // original dimensions
      $dw = $srcWidth;
      $dh = $srcHeight;
    }else if($w == 0){
      $scale = false;
      $dh = $h;
    }else if($h == 0){
      $scale = false;
      $dw = $w;
    }else{
      $dw = $w;
      $dh = $h;
    }
    // do the conversion
    $est = 'convert -background none -density 1200 -resize ' . $dw . "x" . $dh;
    $est .= $scale ? "! " : " ";
    $est .= strtolower($srcType) . ":" . $origPath;
    $est .= " png:" . $targetFile;
    //show_error($est);
    $imagickReturns = exec($est . ' 2>&1');
    if($imagickReturns !== ""){
      log_message("error","image magick error " . $imagickReturns);
      show_error("conversion error",500);
    }
    return $targetFile;
  }
}