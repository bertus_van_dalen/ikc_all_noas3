<?php

class File extends CI_Model {
  function __construct(){
    if(FCF_CONF === 'none') die('no conf');
    parent::__construct();
  }
  function getEmptyLocation(){
    $dir = $this->config->item("uploaded_dir");
    $filename = $this->generateRandomString();
    while(file_exists($dir.$filename)) $filename = $this->generateRandomString();
    return $dir.$filename;
  }
  function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }
}
?>