<?php

class Musicbase extends CI_Model {

  private $dal; // databass access layer
  private $ci; // code igniter instance
  
  function __construct() {
    if(FCF_CONF === 'none') die('no conf');
    parent::__construct();
    $this->ci = & get_instance();
    log_message('debug', 'Musicbase added to Model');
  }
  public function environmentForTemplate($template) {
    $subset = $template->subset;
    $style = $subset->style;
	  $return = new stdClass();
    $return->projectRules = $this->rulesForStyle($style->id);
    $return->projectMaterial = $this->materialForTemplate($template);
    return $return;
  }
  private function rulesForStyle($styleId) {
    $style = R::load("style", $styleId);
    $return = array();
    foreach ($style->ruleset->sharedRule as $ruleId => $rule) {
      $return[] = $rule->getProperties();
    }
    return $return;
  }
  private function materialForTemplate($template){
    $ret = new stdClass();
    $ret->id = $template->id;
    $ret->name = $template->name;
    $client_url = $this->config->item("clients_url") . $this->ci->Session->projectClientId() . '/';
    $ret->textSlogan = $template->textslogan;
    $ret->background = $client_url . $template->slogan;
    $ret->type = "template";
    $ret->parts = new stdClass();
    $subset = $template->subset;
    $subparts = $subset->ownSubpart;
    foreach($subparts as $spID => $subpart){
      if(!$subpart->active) continue;
      $ret->parts->$spID = new stdClass();
      $ret->parts->$spID->id = $subpart->id;
      $ret->parts->$spID->name = $subpart->name;
      $ret->parts->$spID->marginx = $subpart->marginx;
      $ret->parts->$spID->marginy = $subpart->marginy;
      $ret->parts->$spID->order = $subpart->order;
      $ret->parts->$spID->width = $subpart->width;
      $ret->parts->$spID->clips = new stdClass();
      $subclips = $subpart->ownSubclip;
      foreach($subclips as $scID => $subclip){
        if(!$subclip->active) continue;
        $ret->parts->$spID->clips->$scID = new stdClass();
        $ret->parts->$spID->clips->$scID->id = $subclip->clip->id;
        $ret->parts->$spID->clips->$scID->exitpoint = $subclip->clip->exitpoint;
        $ret->parts->$spID->clips->$scID->entrypoint = $subclip->clip->entrypoint;
        $ret->parts->$spID->clips->$scID->name = $subclip->name;
        $ret->parts->$spID->clips->$scID->tag = $subclip->tag;
        $ret->parts->$spID->clips->$scID->icon = $subclip->icon;
        $ret->parts->$spID->clips->$scID->ficon = $subclip->ficon;
        $ret->parts->$spID->clips->$scID->ficonroll = $subclip->ficonroll;
        $ret->parts->$spID->clips->$scID->order = $subclip->order;
        $ret->parts->$spID->clips->$scID->color = $subclip->color;
        $ret->parts->$spID->clips->$scID->width = $subclip->width;
        $ret->parts->$spID->clips->$scID->height = $subclip->height;
      }
    }
    return $ret;
  }
}

?>
