<?php

class Conf extends CI_Model {

  function __construct(){
    if(FCF_CONF === 'none') die('no conf');
    parent::__construct();
  }
  public function get() {
    log_message("debug", "Conf->get()");
    $ci = get_instance();
    if (!$ci->Session) {
      log_message("error", "conf required without session?");
      exit;
    }
    $loginoid = $ci->Session->projectAllowsOpenIdLogin();
    $logincli = $ci->Session->projectAllowsNpoLogin();
    $confs = $ci->Session->getConfs();
    $ret = new stdClass();
    $ret->project_title = $ci->Session->projectTitle();
    $ret->project_login = array();
    if ($logincli)
      $ret->project_login[] = "npo";
    if ($loginoid)
      $ret->project_login[] = "janrain";
    $ret->project_url = $this->config->item('project_url');
    $ret->base_url = $this->config->item('base_url');
    $ret->projectCss = file_exists(FCPATH . 'data/css/styles_' . FCF_CONF . '.css') ? 'data/css/styles_' . FCF_CONF . '.css' : 'data/css/styles.css';
    $ret->css_url_shared = $this->config->item('css_url_shared');
    $ret->cache_url = $this->config->item('cache_url');
    $ret->js_url = $this->config->item('js_url');
    $ret->swf_url_shared = $this->config->item('swf_url_shared');
    $ret->swf_file = $this->config->item('swf_file');
    $ret->dwn_url = $this->config->item('download_base_url');
    $ret->published_url = $this->config->item('published_url');
    $ret->clips_url = $this->config->item('clips_url');
    $ret->uploaded_url = $this->config->item('uploaded_url');
    
    $clientId = $ci->Session->projectClientId();
    $ret->client_url = $this->config->item('clients_url') . $clientId . '/';
    $ret->client_id = $clientId;
    $ret->cb = date($ci->Session->projectCbmode());
    $ret->app = FCF_CONF;
    $ret->prod_locs = $ci->Session->projectProdLocs();
    $ret->prod_redirect = $ci->Session->projectUrl();
    $ret->install_flash_href = $this->config->item('install_flash_href');
    $ret->min_flash_version = $this->config->item('min_flash_version');
    $ret->user_name = $ci->Session->getUser()->name;
    $ret->janrain = array(
        "app_domain" => $this->config->item("janrain_app_domain"),
        "app_id" => $this->config->item("janrain_app_id")
    );
    foreach ($confs as $c) {
      $confName = "conf_";
      if(is_bool($c->value)){
        $val = $c->value == true;
      } else if (is_int ($c->value)) {
        $val = (int) $c->value;
      } else if (is_float ($c->value)) {
        $val = (float) $c->value;
      } else if (is_string($c->value)) {
        $val = (string) $c->value;
      }
      $confName .= $c->name;
      $ret->$confName = $val;
    }
    return $ret;
  }
}

?>