<?php

class Pubxml extends CI_Model {

  private $ci; // code igniter instance

  /*
    |--------------------------------------------------------------------
    |--------------------------------------------------------------------
    | CONSTRUCTOR
    |--------------------------------------------------------------------
    |--------------------------------------------------------------------
    |
   */

  function __construct() {
    if(FCF_CONF === 'none') die('no conf');
    parent::__construct();
    $this->ci = & get_instance();
    $this->ci->load->model("Session");
    log_message('debug', 'Pubxml added to Model');
  }
  
  public function removeEntriesForCompositionId($id){
    $q = 'DELETE FROM composition_playlist WHERE composition_id = ?';
    R::getAll($q,array($id));
    log_message("debug","removeEntriesForCompositionId(" . $id . ")");
  }
  
  public function updatePlaylist($cp){
    $optsQ = "";
    $optsQ .= " SELECT c.id composition, pp.playlist_id playlist, s.id style, pp.project_id project ";
    $optsQ .= "FROM composition c ";
    $optsQ .= "JOIN playlist_project pp ON c.project_id = pp.project_id ";
    $optsQ .= "JOIN template tpl ON c.template_id = tpl.id ";
    $optsQ .= "JOIN subset ss ON tpl.subset_id = ss.id ";
    $optsQ .= "JOIN style s ON ss.style_id = s.id ";
    $optsQ .= "WHERE c.id = ? " ;
    $params = array($cp->id);
    log_message("debug",vsprintf(str_replace("?", "%s", $optsQ), $params));
    
    $opts = R::getAll($optsQ, $params);
    foreach($opts as $o){
      if($this->_limitationsAllowMixInThisProject($o['style'], $o['project'])){
        $pl = R::load('playlist', $o['playlist']);
        $cp->sharedPlaylist[] = $pl;
      }
    }
    R::store($cp);
  }
  
  private $_limits = null;
  private function _getLimits(){
    if(null == $this->_limits){
      $limits = R::find('cclimit');
      $this->_limits = array();
      foreach($limits as $l){
        if(!array_key_exists($l->style->id, $this->_limits)){
          $this->_limits[$l->style->id] = array();
        }
        $this->_limits[$l->style->id][] = $l;
      }
    }
    return $this->_limits;
  }
  private function _limitationsAllowMixInThisProject($styleID, $idOfProjectThatHasPlaylist){
    $ls = $this->_getLimits();
    if(!array_key_exists($styleID, $ls)){
      // style is not limited
      return true;
    }
    foreach($ls[$styleID] as $l ){
      if($l->project->id == $idOfProjectThatHasPlaylist){
        if($l->mix){
          return true;
        }
      }
    }
    return false;
  }
}

?>
