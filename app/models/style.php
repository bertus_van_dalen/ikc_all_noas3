<?php

include_once(APPPATH . "models/apimodel.php");

class Style extends Apimodel {
  
  private $_mod;
  private $ci;
  
  function __construct(){
    parent::__construct();
    $this->ci = get_instance();
    $this->ci->load->model("Session");
  }
  
  public function getStylesAndRuleTypes(){
    $d = array();
    $d["styles"] = R::getAll( 'SELECT id, name FROM style' );
    return $d;
  }
  
  public function uploadSubclip($subclipId, $tempFile, $uploadFileName, $type, $subsetId){
    $d = $this->_validateClipUploadData($subclipId,$tempFile,$uploadFileName,$type);
    $clip = R::dispense('clip');
    $clip->name = $d['name'];
    $clip->exitpoint = 0;
    $clip->entrypoint = 0;
    R::store($clip);
    $dir = $this->config->item('clips_dir');
    $pth = $dir . $clip->id . '.MP3';
    if(true === move_uploaded_file($tempFile, $pth)){
      $sc = $d['subclip'];
      $sc->clip = $clip;
      $sc->name = $d['name'];
      R::store($sc);
      if($subsetId){
        return array(
          "subset" => $this->getFullSubsetAggregation($subsetId)
        );
      }else{
        show_error("no subset id");
      }
    }else{
      show_error("upload sub clip error 1");
    }
  }
  
  public function createStyle($name){
    if(!preg_match('/^[a-z0-9_]+$/i', $name)){ show_error("name invalid, it may contain only alphanumeric characters and underscores, no spaces"); }
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator");}
    $exists = R::findOne('style', ' name = ? ', array($name));
    if($exists){
      show_error("This name already exists for another style. Use your fantasy and think of another name.");
    }
    $newStyle = R::dispense('style');
    $newStyle->name = $name;
    $ruleSet = R::dispense('ruleset');
    $newStyle->ruleset = $ruleSet;
    $ruleSet->sharedRule = array();
    $rulesToCopy = R::getAll( 'SELECT description, type FROM rule GROUP BY(type)' );
    foreach($rulesToCopy as $ruleToCopy){
      $newRule = R::dispense('rule');
      $newRule->type = $ruleToCopy['type'];
      $newRule->description = $ruleToCopy['description'];
      $ruleSet->sharedRule[] = $newRule;
    }
    R::store($newStyle);
    return $this->loadStyle($name);
  }
  
  public function loadStyle($name){
    $exists = R::findOne('style', ' name = ? ', array($name));
    if(!$exists){
      show_error("Cannot find a style for name " . $name);
    }
    $beanArr = array($exists);
    R::preload($beanArr,'ruleset|ruleset,*.sharedRule|rule');
    $return = $this->getStylesAndRuleTypes();
    $asArray = R::beansToArray($beanArr);
    $return['style'] = $asArray[0];
    return $return;
  }
  
  public function loadScrollForProject($id){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator");}
    $p = R::load('project', $id);
    if(!$p->id){ show_error("no project found for " . $id); }
    R::preload(array($p), 'ownScrollitem|scrollitem,*.template|template,*.composition|composition');
    if(empty($p->ownScrollitem)){
      $p->ownScrollitem[] = R::dispense('scrollitem');
      R::store($p);
    }
    return reset(R::beansToArray(array($p)));
  }
  
  public function saveStyle($d){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator");}
    $original = $this->_validateSaveStyleData($d);
    $original->name = $d['name'];
    $original->iconurl = $d['iconurl'];
    foreach($d['ruleset']['sharedRule'] as $ruleData){
      $dbRule = $original->ruleset->sharedRule[$ruleData['id']];
      $dbRule->value = $ruleData['value'];
    }
    R::store($original);
  }
  
  public function deleteStyle($name){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator");}
    $exists = R::findOne('style', ' name = ? ', array($name));
    if(!$exists){
      show_error("Cannot find a style for name " . $name);
    }
    $rulesetsToDelete = array();
    $rulesToDelete = array();
    // see for the ruleset if it has other styles
    $ruleSet = $exists->ruleset;
    if( count($ruleSet->ownStyle) == 1 ){
      $rulesetsToDelete[] = $ruleSet;
      // see for each rule if it has other rulesets
      foreach($ruleSet->sharedRule as $rule){
        if(count($rule->sharedRuleset) == 1){
          $rulesToDelete[] = $rule;
        }
      }
    }
    
    // it is not allowed to delete the style if more than one subset is associated
    
    $subsetToDelete = null;
    $templateToDelete = null;
    $compositionToDelete = null;
    $subpartsToDelete = array();
    $subclipsToDelete = array();
    $clipsToDelete = array();
    
    $numSubsets = count($exists->ownSubset);
    if($numSubsets > 1){
      show_error("Cannot delete this style, there is more than one subsets associated with it. Sorry.");
    }else if($numSubsets == 1){
      $subsetToDelete = reset($exists->ownSubset);
    }
    if($subsetToDelete){
      $numTemplates = count($subsetToDelete->ownTemplate);
      if($numTemplates > 1){
        show_error("Cannot delete a style that has a subset with more than one template associated to it. Sorry.");
      }else if($numTemplates == 1){
        $templateToDelete = reset($subsetToDelete->ownTemplate);
      }
    }
    if($subsetToDelete){
      foreach($subsetToDelete->ownSubpart as $subpartToDelete){
        $subpartsToDelete[] = $subpartToDelete;
      }
    }
    if($subpartsToDelete){
      foreach($subpartsToDelete as $subpToDel){
        foreach($subpToDel->ownSubclip as $subclipToDelete){
          $subclipsToDelete[] = $subclipToDelete;
        }
      }
    }
    if($subclipsToDelete){
      foreach($subclipsToDelete as $scToDel){
        $clipToDelete = $scToDel->clip;
        if($clipToDelete){
          $numSubclips = count($clipToDelete->ownSubclip);
          if($numSubclips > 1){
            show_error("cannot delete a style that has a subset with a subpart with a subclip with a clip with more than one subclips, sorry.");
          }else if($numSubclips == 1){
            $clipsToDelete[] = $clipToDelete;
          }
        }
      }
    }
    if($templateToDelete){
      $numCompositions = count($templateToDelete->ownComposition);
      if($numCompositions > 1){
        show_error("Cannot delete a style that has a subset that has a template with more than one compositions associated to it. Sorry.");
      }else if($numCompositions == 1){
        $compositionToDelete = reset($templateToDelete->ownComposition);
      }
    }
    
    // subset w rules
    R::trash($exists);
    R::trashAll($rulesetsToDelete);
    R::trashAll($rulesToDelete);
    
    // subset w template & composition
    if($subsetToDelete){ R::trash($subsetToDelete); }
    if($templateToDelete){ R::trash($templateToDelete); }
    if($compositionToDelete){ R::trash($compositionToDelete); }
    
    // subparts, subclips, clips
    R::trashAll($subpartsToDelete);
    R::trashAll($subclipsToDelete);
    R::trashAll($clipsToDelete);
    
    foreach($rulesetsToDelete as $ruleSetToDelete){
      R::trash($ruleSetToDelete);
    }
    foreach($rulesToDelete as $ruleToDelete){
      R::trash($ruleToDelete);
    }
    
    return $this->getStylesAndRuleTypes();
  }
  
  public function getStyles(){
    show_error("broken deprecated");
    if (false === $moderator = $this->_get_moderator())
      return;
    return R::$f->begin()->select('*')->from('style')->get();
  }
  public function getTemplatesWithSubsetAndStyle($filters = array()){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator");}
    
    $params = array();
    $q = "SELECT ";
    $q .= "t.name template_name, ";
    $q .= "t.id template_id, ";
    $q .= "t.composition_id composition_id, ";
    $q .= "c.name composition_name, ";
    $q .= "ss.name subset_name, ";
    $q .= "ss.id subset_id, ";
    $q .= "s.name style_name, ";
    $q .= "s.id style_id, ";
    $q .= "(SELECT COUNT(id) FROM composition WHERE template_id = t.id AND t.composition_id != composition.id) num_user_compositions ";
    $q .= "FROM template t ";
    $q .= "JOIN subset ss ON ss.id = t.subset_id ";
    $q .= "JOIN style s ON s.id = ss.style_id ";
    $q .= "JOIN composition c ON c.id = t.composition_id ";
    $q .= "WHERE TRUE ";
    
    if(isset($filters['subset_id'])){
      $q .= "AND ss.id = ? ";
      $params[] = $filters['subset_id'];
    }
    if(isset($filters['style_id'])){
      $q .= "AND s.id = ? ";
      $params[] = $filters['style_id'];
    }
    
    //show_error(vsprintf(str_replace("?", "%s", $q), $params));
    
    return R::getAll($q, $params);
  }
  public function getFullSubsetAggregation($id){
    $ss = R::load('subset',$id);
    if(!$ss->id){ show_error("subset could not be loaded because it does not exist for id ". $id); }
    $ssArr = array($ss);
    R::preload($ssArr,'ownSubpart|subpart,*.ownSubclip|subclip,*.clip|clip');
    return reset(R::beansToArray($ssArr));
  }
  public function getFullSubsetAggregationAndAllColorSeries($id){
    return array(
        "colorSeries" => $this->getAllColorSeries(),
        "subset" => $this->getFullSubsetAggregation($id)
        );
  }
  public function getAllColorSeries(){
    $cs = R::findAll('colorseries');
    R::preload($cs, 'ownColor|color');
    return R::beansToArray($cs);
  }
  public function duplicateFullSubsetAggregation($d){
    if(false === $this->_get_moderator()){ show_error("permission problem"); }
    $this->_validateSubsetData($d);
    $old = R::load('subset', $d['id']);
    $new = $this->_createDuplicatedSubsetForInputDataForExistingSubset($d, $old);
    $new->ownSubpart = array();
    foreach($d['ownSubpart'] as $spd){
      $this->_validateSubpartData($spd);
      $this->_addSubpartDataToDuplicatedSubset($spd, $new);
    }
    $this->_createTemplateForSubset($new);
    R::store($new);
    $this->_copyClipsForParentClips($new);
  }
  public function storeFullSubsetAggregation($d){
    if(false === $this->_get_moderator()){ show_error("permission problem"); }
    $this->_validateSubsetData($d);
    $s = R::load('subset', $d['id']);
    $s->ownSubpart = array();
    $this->_addSubsetDataToSubset($d, $s);
    foreach($d['ownSubpart'] as $spd){
      $this->_validateSubpartData($spd);
      $this->_addSubpartDataToSubset($spd, $s);
    }
    R::store($s);
  }
  public function generateSubset($name){
    if (false === $moderator = $this->_get_moderator())
      return;
    $style = R::findOne('style', ' name = ? ', array($name));
    if($style == null){
      show_error("no style could be found for name " . $name);
    }
    
    $subset = R::dispense("subset");
    $subset->mtime = time();
    $subset->ctime = time();
    $subset->name = $style->name . time();
    $style->ownSubset[] = $subset;
    R::store($style);
    
    $this->createTemplate($subset->id);
    
  }
  public function createTemplateForTemplate($id){
    if (false === $moderator = $this->_get_moderator())
      return;
    $existing = R::load("template", $id);
    $template = R::dispense("template");
    $template->template = $existing;
    $template->mtime = time();
    $template->ctime = time();
    $template->slogan = "none";
    $template->name = "template from template " . $existing->name;
    $composition = R::dispense("composition");
    $template->ownComposition[] = $composition;
    $composition->created = time();
    $composition->published = false;
    $composition->hasmp3 = false;
    $composition->name = "composition for template " . $template->name;
    $composition->pubtime = 0;
    $composition->mtime = time();
    $template->composition = $composition;
    $subset->ownTemplate[] = $template;
    $composition->user = $this->Session->getUser();
    R::store($template);
    return array("id"=>$template->id);
  }
  public function createTemplate($subset_id){
    if (false === $moderator = $this->_get_moderator())
      return;
    $subset = R::load("subset",$subset_id);
    if(!$subset->id) exit("subset does not exist");
    log_message("debug","subset loaded for id " . $subset->id);
    $template = $this->_createTemplateForSubset($subset);
    R::store($subset);
    return array("id"=>$template->id);
  }
  public function addScrollitem($templateIdAndProjectId){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator"); }
    $tpl = R::load('template', $templateIdAndProjectId['template_id']);
    $p = R::load('project', $templateIdAndProjectId['project_id']);
    if(!$tpl->id){ show_error("template not found for " . $templateIdAndProjectId['template_id']); }
    if(!$p->id){ show_error("project not found for " . $templateIdAndProjectId['project_id']); }
    $si = R::dispense("scrollitem");
    $si->project = $p;
    $si->template = $tpl;
    $si->order = time();
    R::store($si);
    $hack = $si->template->composition->name;
    return reset(R::beansToArray(array($si)));
  }
  public function addSubpart($subsetId){
    if (false === $moderator = $this->_get_moderator())
      return;
    $subset = R::load('subset', $subsetId);
    if(!$subset->id){
      show_error("could not load subset");
    }
    $sp = R::dispense("subpart");
    $sp->name = $subset->style->name  . "SP" . time();
    $sp->active = 0;
    $sp->marginx = 2;
    $sp->marginy = 2;
    $sp->order = 0;
    $sp->width = 190;
    $subset->ownSubpart[] = $sp;
    R::store($subset);
    return array(
        "subset" => $this->getFullSubsetAggregation($subset->id)
        );
  }
  public function addClip($subpartId){
    if (false === $moderator = $this->_get_moderator())
      return;
    $subpart = R::load('subpart',$subpartId);
    if(!$subpart->id){
      show_error("could not load subpart");
    }
    $subset = $subpart->subset;
    $sc = R::dispense("subclip");
    $sc->name = "";
    $sc->active = 0;
    $sc->tag = "";
    $sc->icon = "";
    $sc->order = 0;
    $sc->color = "#000000";
    $sc->width = "";
    $sc->height = "";
    $subpart->ownSubclip[] = $sc;
    R::store($subpart);
    return array(
        "subset" => $this->getFullSubsetAggregation($subset->id)
        );
  }
  public function removeMp3($subclipId){
    if (false === $moderator = $this->_get_moderator())
      return;
    $sc = R::load('subclip', $subclipId);
    if(!$sc->id){
      show_error("could not load subclip for id " . $subclipId);
    }
    if(!$sc->clip){
      show_error("no clip could be found for subclip " . $subclipId);
    }
    $cl = $sc->clip;
    if(count($cl->ownSubclip) === 1){
      $clipPath = $this->config->item("clips_dir");
      $clipFile = $clipPath . $cl->id . ".MP3";
      if(false === unlink($clipFile)){
        show_error("could not delete " . $clipFile);
      }
      R::trash($cl);
    }
    $sc->clip = null;
    R::store($sc);
    return array(
        "subset" => $this->getFullSubsetAggregation($sc->subpart->subset->id)
        );
  }
  public function ownTemplate($id){
    if (false === $moderator = $this->_get_moderator())
      return;
    $template = R::load("template",$id);
    $cp = $template->composition;
    $cp->user = $this->_mod;
    return "template owned for id " . $id;
  }
  public function deleteTemplate($id){
    if (false === $moderator = $this->_get_moderator())
      return;
    $template = R::load("template",$id);
    if(!$template->id) exit("template does not exist");
    log_message("debug","template loaded for id " . $template->id);
    $composition = $template->composition;
    $deleted_id = $template->id;
    R::trash($composition);
    R::trash($template);
    return "deleted template " . $deleted_id;
  }
  public function searchExistingConfigKeys($viewVal){
    $q = " SELECT * ";
    $q .= "FROM conf ";
    $q .= "WHERE name LIKE ? ";
    $q .= "LIMIT 20";
    $rows = R::getAll($q, array('%'.$viewVal.'%'));
    return $rows;
  }
  public function listConfValOpts($name){
    $q = " SELECT value ";
    $q .= "FROM conf ";
    $q .= "WHERE name = ? ";
    $q .= "LIMIT 20";
    $rows = R::getAll($q, array($name));
    return $rows;
  }
  public function searchExistingConfigValuesForKey($nameAndViewValue){
    $name = $nameAndViewValue["name"];
    $viewValue = $nameAndViewValue["viewValue"];
    $q = " SELECT value ";
    $q .= "FROM conf ";
    $q .= "WHERE name = ? ";
    $q .= "AND value LIKE ? ";
    $q .= "LIMIT 20";
    $rows = R::getAll($q, array($name, '%' . $viewValue . '%'));
    return $rows;
  }
  public function addTemplateConfig($d){
    if (false === $moderator = $this->_get_moderator())
      return;
    $tpl = $this->_loadValidTemplate($d["templateId"]);
    $this->_validateAddTemplateConfigData($d);
    $cf = R::findOne("conf", " name = ? AND value = ? ", array($d["name"], $d["value"]));
    if(null == $cf){
      $cf = R::dispense("conf");
      $cf->name = $d["name"];
      $cf->value = $d["value"];
    }
    $cf->sharedTemplate[] = $tpl;
    R::store($cf);
    return $this->getTemplateNg($d["templateId"]);
  }
  public function deleteTemplateConfig($d){
    if (false === $moderator = $this->_get_moderator())
      return;
    $this->_validateDeleteTemplateConfigData($d);
    $t = R::load('template', $d['templateId']);
    if(!$t->id){
      show_error("no template for id " . $d['templateId']);
    }
    $c = R::load('conf', $d['id']);
    if(!$c->id){
      show_error("no conf for id " . $d['id']);
    }
    unset($c->sharedTemplate[$t->id]);
    R::store($c);
    if(empty($c->sharedTemplate) && empty($c->sharedProject)){
      R::trash($c);
    }
  }
  public function getCompositionByCompositionIdUserNameCompositionNameTemplateNameSubsetName($viewVal){
    $q = " SELECT c.id composition_id, ";
    $q .= "c.name composition_name, ";
    $q .= "t.name template_name, ";
    $q .= "u.name user_name, ";
    $q .= "s.name subset_name, ";
    $q .= "CONCAT_WS(' | ',c.id,u.name,c.name,t.name,s.name) viewVal ";
    $q .= "FROM composition c ";
    $q .= "JOIN user u ON c.user_id = u.id ";
    $q .= "JOIN template t ON c.template_id = t.id ";
    $q .= "JOIN subset s ON t.subset_id = s.id ";
    $q .= "WHERE CONCAT_WS(' | ',c.id,u.name,c.name,t.name,s.name) LIKE ?";
    $q .= "LIMIT 20";
    $rows = R::getAll($q, array('%'.$viewVal.'%'));
    return $rows;
  }
  public function getSubsetBySubsetIdSubsetName($viewVal){
    $q = " SELECT s.id subset_id, ";
    $q .= "s.name subset_name, ";
    $q .= "CONCAT_WS(' | ',s.id,s.name) viewVal ";
    $q .= "FROM subset s ";
    $q .= "WHERE CONCAT_WS(' | ',s.id,s.name) LIKE ?";
    $q .= "LIMIT 20";
    $rows = R::getAll($q, array('%'.$viewVal.'%'));
    return $rows;
  }
  public function createTemplateForCompositionId($id){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator"); }
    if(!is_numeric($id)){ show_error("id must be numeric"); }
    $c = R::load('composition', $id);
    if(!$c->id){ show_error("composition not found"); }
    $duplicatedTemplate = $this->_createDuplicate($c);
    R::store($duplicatedTemplate);
    return array(
        "created" => $this->_exportTemplate($duplicatedTemplate),
        "all" => $this->getTemplatesWithSubsetAndStyle()
    );
  }
  public function createTemplateForSubsetId($id){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator"); }
    if(!is_numeric($id)){ show_error("id must be numeric"); }
    $s = R::load('subset', $id);
    if(!$s->id){ show_error("subset not found"); }
    $t = $this->_createTemplateForSubset($s);
    R::store($s);
    return array(
        "created" => $this->_exportTemplate($t),
        "all" => $this->getTemplatesWithSubsetAndStyle()
    );
  }
  public function deleteTemplateNg($d){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator"); }
    $t = $this->_validateSaveTemplateData($d);
    $this->_authorizeSaveTemplate($t);
    if(count($t->ownComposition) > 1){ show_error("cannot delete template because it has multiple compositions. Think hard if you may want to delete those compositions first."); }
    R::trash($t->composition);
    R::trash($t);
    return $this->getTemplatesWithSubsetAndStyle();
  }
  public function deleteScrollItem($id){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator"); }
    $si = R::load('scrollitem', $id);
    if(!$si->id){ show_error("scroll item not found for " . $id); }
    R::trash($si);
  }
  public function getTemplates(){
    show_error("broken deprecated");
    if (false === $moderator = $this->_get_moderator())
      return;
    return R::$f->begin()->select('*')->from('template')->get();
  }
  public function getTemplate($id){
    if (false === $moderator = $this->_get_moderator())
      return;
    $template = R::load("template",$id);
    if(!$template->id){
      exit("template does not exist");
    }
    $return = R::exportAll($template,false,array("iets"));
    $return[0]['compositionName'] = $template->composition->name;
    return $return;
  }
  public function getTemplateNg($id){
    if (false === $moderator = $this->_get_moderator())
      return;
    $template = R::load("template",$id);
    if(!$template->id){
      show_error("template does not exist");
    }
    return $this->_exportTemplate($template);
  }
  public function saveTemplateNg($d){
    $t = $this->_validateSaveTemplateData($d);
    $this->_authorizeSaveTemplate($t);
    $t->name = $d['name'];
    $t->textslogan = $d['textslogan'];
    $t->composition->name = $d['composition']['name'];
    $t->mtime = time();
    R::store($t);
  }
  public function saveScrollForProject($d){
    if (false === $moderator = $this->_get_moderator()) { show_error("n omoderator"); }
    foreach($d['ownScrollitem'] as $otnd){
      $si = R::load('scrollitem', $otnd['id']);
      $si->order = intval($otnd['order']);
      R::store($si);
    }
  }
  public function clearEditedTemplateCompositionContents($d){
    if (false === $moderator = $this->_get_moderator()) { show_error("n omoderator"); }
    $t = $this->_validateSaveTemplateData($d);
    $this->_authorizeSaveTemplate($t);
    $t->composition->content = "";
    R::store($t);
  }
  public function duplicateTemplate($d){
    if (false === $moderator = $this->_get_moderator()) { show_error("n omoderator"); }
    $t = $this->_validateSaveTemplateData($d);
    $duplicatedTemplate = $this->_createDuplicate($t->composition);
    R::store($duplicatedTemplate);
    return array(
        "duplicated" => $this->_exportTemplate($duplicatedTemplate),
        "all" => $this->getTemplatesWithSubsetAndStyle()
    );
  }
  public function storeTemplateDataForId($i,$d){
    if (false === $moderator = $this->_get_moderator())
      return;
    $template = R::load("template",$i);
    if(!$template->id){
      exit("template does not exist: " . $i);
    }
    if(!is_string($d->name)){
      exit("template " . $i . " name must be string");
    }
    if(!is_string($d->compositionName)){
      exit("template " . $i . " compositionName must be string");
    }
    
    $cliUrl = $this->config->item("clients_url") . $this->ci->Session->projectClientId() . '/';
    $pos = strpos($d->slogan,$cliUrl);
    if(false === $pos){
      $d->slogan = "none";
    }else if(0 != $pos){
      $d->slogan = "none";
    }else{
      $d->slogan = substr($d->slogan, strlen($cliUrl));
    }
    $template->name = $d->name;
    $template->slogan = $d->slogan;
    $template->composition->name = $d->compositionName;
    R::store($template);
  }
  /**
   * when $r is null, generates a form with an input for a style id
   * when a style_id is set in the form it gives you a zip file
   * @param type $r
   * @return type
   */
  public function getStyleZip($styleID){
    $this->_getStyleZip($styleID);
  }
  public function emptyTmpZipDir(){
    if(false === $moderator = $this->_get_moderator()) return;
    $this->_remDir($this->config->item('temp_zip_dir'));
    mkdir($this->config->item('temp_zip_dir'));
  }
  public function resampleStyleClips($styleID){
    if(false === $moderator = $this->_get_moderator()) return;
    $this->tlg("resampleStyleClips");
    $st = R::load("style",$styleID);
    if(!$st->id){
      $this->dlg("no existing style found for that id");
      return;
    }
    $this->tlg("style found: " . $st->name);
    $clipPath = $this->config->item("clips_dir");
    $clipFiles = array();
    $clipCopies = array();
    foreach( $st->sharedPart as $id => $part){
      $this->tlg("part found: " . $part->name);
      foreach($part->ownClip as $cid => $clip){
        $this->tlg("clip: " . $clip->id);
        $clipFile = $clipPath . $clip->id . ".MP3";
        $copyFile = $clipPath . $clip->id . '_tmp.MP3';
        if(is_file($clipFile)){
          if(true === copy($clipFile, $copyFile)){
            $clipFiles[] = $clipFile;
            $clipCopies[] = $copyFile;
            $this->tlg("file copied: " . $clipFile);
          }
        }else{
          $this->dlg("no file: " . $clipFile);
        }
      }
    }
    $lame = $this->config->item("lame_full_path");
    foreach($clipFiles as $i => $clipFile){
      $sourceFileName = $clipCopies[$i];
      $targetFileName = $clipFile;
      $execStr = $lame . ' --resample 44.1 ' . $sourceFileName . ' ' . $targetFileName . ' > /dev/null; echo $?';
      $exitStatus = shell_exec($execStr);
      if ($exitStatus != 0) {
        $this->dlg('lame encoder FAIL: ' . $execStr);
      }else{
        $this->tlg('lame encoder SUCCESS');
      }
      unlink($sourceFileName);
    }
  }
  public function glist(){
    $ret = array();
    $ret = R::findAll('style');
    return $ret;
  }
  public function getProjectList(){
    if(false === $moderator = $this->_get_moderator()) { show_error("no moderator"); }
    $q = "SELECT * FROM project";
    return R::getAll($q);
  }
  public function getStyleToCreateSet($r = null){
    
    log_message("debug", "style->getOptionsToCreateSet()");
    
    $f = '';
    
    $f .= form_label('style id: ', 'style_id');
    
    $data = array(
        'name' => 'style_id',
        'id' => 'style_id',
        'maxlength' => '4',
        'size' => '50'
    );
    $f .= form_input($data);
    
    $f .= form_submit('submit', 'aanmaken');
    
    return $f;
  }
  public function createSetForm($r){
    if(!(parent::setData($r))) return;
    if(false === $moderator = $this->_get_moderator()) return;
    if(false === ($id = parent::getIntValue("style_id"))){
      return;
    }
    if($r == null){
      return $this->_createSetForm();
    }else{
      $this->_getStyleZip($r);
    }
  }
  public function extractZip($file, $test = false){
    if(false === $moderator = $this->_get_moderator()){
      log_message("error","problem must be moderator");
    }
    if(false === ($extracted = $this->_extractZip($file))){
      log_message("error","problem extracting zip file " . $file);
      return;
    }
    $this->_importMusicStyle($extracted, $test);
    $this->_remDir($extracted);
    unlink($file);
  }
  public function removeStyleForId($id){
    
  }
  public function readLameTag($r){
    if(!(parent::setData($r))) return;
    log_message("debug","data set");
    if(false === $moderator = $this->_get_moderator()) return;
    $this->tlg("is moderator");
    if(false === $mp3ID = parent::getIntValue("mp3ID")) return;
    $this->tlg("mp3ID defined: " . $mp3ID);
    $fullPath = $this->config->item('fcf_download_clips_dir') . $mp3ID . '.MP3';
    $this->tlg("full path: " . $fullPath);
    if(!file_exists($fullPath)){
      parent::_error("23","mp3ID",$fullPath);
      return;
    }
    $this->tlg("file exists");
    if(false === $handle = fopen($fullPath, "r")) $this->dlg("could not obtain read handle for " . $fullPath);
    //    ZONE A - Traditional Xing VBR Tag data 
    //    4 bytes for Header Tag 
    //    4 bytes for Header Flags 
    //  100 bytes for entry (NUMTOCENTRIES) 
    //    4 bytes for FRAME SIZE 
    //    4 bytes for STREAM_SIZE 
    //    4 bytes for VBR SCALE. a VBR quality indicator: 0=best 100=worst
    
    // cum = 120
    
    //   ZONE B - Initial LAME info 
    //   20 bytes for LAME tag.  for example, "LAME3.12 (beta 6)" 
    // ___________ 
    //  140 bytes 
    //
    
    // 140 + 208 = max 348
    
    //   ZONE C - LAME Tag 
    //   208 bytes unused in 128k frame (in 48kHz case) 
    // 
    //   using 
    //   FrameLengthInBytes = 144 * BitRate / SampleRate + Padding 
    // 
    //   this gives 
    //   Layer III, BitRate=128000, SampleRate=44100, Padding=0 
    //        ==>  FrameSize=417 bytes 
    //   Layer III, BitRate=128000, SampleRate=48000, Padding=0 
    //        ==>  FrameSize=384 bytes 
    // 
    //   so this would make the minimal frame size 384 bytes ($0-$17F), hence the available bytes for this field are not 241 as in this 44100Hz case, but at most 208 bytes.  
    echo "<br/>---START---<br/>";
    $lameVersion = "0000";
    $lameTagByteNum = 0;
    $inLameTag = false;
    $lameTagBytes = array(0,0,0,0);
    while (! (feof($handle) || ($pos = ftell($handle)) > 348 + 50)) {
      $contents = fread($handle, 1);
      $bitStr = decbin(ord($contents));
      $intStr = "" . ord($contents);
      while(strlen($bitStr) < 8){
        $bitStr = "0" . $bitStr;
      }
      if($lameTagByteNum == 0){
        $lameVersion = substr($lameVersion,1) . $contents;
        array_shift( $lameTagBytes ); 
      }
      $lameTagBytes[] = $contents;
      if($lameVersion == "LAME"){
        $lameTagByteNum = 4;
        $inLameTag = true;
        $lameVersion = "0000";
      }
      if($inLameTag){
        echo $lameTagByteNum . " -> " . $bitStr . "<br/>";
        switch($lameTagByteNum){
          case(9):
            echo "---------- LAME VERSION: ";
            print_r($lameTagBytes);
            echo "<br/>";
            $min = 0;
            $max = 9;
            $lameVBits = '';
            while(!($max == $min)){
              $ia = "" . decbin(ord($lameTagBytes[$min]));
              while(strlen($ia) < 8){
                $ia = "0" . $ia;
              }
              $lameVBits .= $ia;
              $min++;
            }
            echo "---------- version: " . $lameVBits . "<br/>";
            break;
          case(10):
            $strLo = substr($bitStr,4);
            $strHi = substr($bitStr,0,4);
            echo "---------- lame tag revision / vbr method: " . $strHi . " / " . $strLo . "<br/>";
            break;
          case(11):
            echo "---------- low pass: " . decbin(ord($contents)) . " aka " . $intStr . "<br/>";
            break;
          case(19):
            $min = 11;
            $max = 19;
            $replayGainBits = '';
            while(!($max == $min)){
              $ia = "" . decbin(ord($lameTagBytes[$min]));
              while(strlen($ia) < 8){
                $ia = "0" . $ia;
              }
              $replayGainBits .= $ia;
              $min++;
            }
            $rplGn = substr($replayGainBits,0,32);
            $rplGn2 = substr($replayGainBits,32,16);
            $rplGn3 = substr($replayGainBits,48,16);
            echo "---------- peak signal amplitude: " . $rplGn . " aka " . sprintf("%f",bindec($rplGn)) . "<br/>";
            echo "---------- replayGain2: " . $rplGn2 . " aka " . bindec($rplGn2) . "<br/>";
            echo "---------- replayGain3: " . $rplGn3 . " aka " . bindec($rplGn3) . "<br/>";
            break;
          case(24):
            $min = 21;
            $max = 24;
            $encDelayBits = '';
            while(!($max == $min)){
              $ia2 = "" . decbin(ord($lameTagBytes[$min]));
              while(strlen($ia2) < 8){
                $ia2 = "0" . $ia2;
              }
              $encDelayBits .= $ia2;
              $min++;
            }
            echo "BITSTRING ENCDELAYS: " . $encDelayBits . "<br/>";
            $encDel1 = substr($encDelayBits,0,12);
            $encDel2 = substr($encDelayBits,12,12);
            echo "---------- encoderDelays1: " . $encDel1 . " aka " . bindec($encDel1) . "<br/>";
            echo "---------- encoderDelays2: " . $encDel2 . " aka " . bindec($encDel2) . "<br/>";
            break;
        }
        
        $lameTagByteNum++;
        
      }
    }
    echo "<br/>---END---<br/>";
    fclose($handle);
  }
  private function _authorizeSaveTemplate($t){
    $CI =& get_instance();
    $u = $CI->Session->getUser();
    if($t->composition->user->id !== $u->id){
      show_error("user " . $u->id . " is not the owner of the template composition " . $t->composition->id);
    }
  }
  private function _validateSaveTemplateData($d){
    if(false === $moderator = $this->_get_moderator()){ show_error("no moderator");}
    if(!isset($d['id'])){ show_error("template id missing"); }
    if(!is_numeric($d['id'])){ show_error("template id must be numeric"); }
    if(!isset($d['composition_id'])){ show_error("template composition_id missing"); }
    if(!is_numeric($d['composition_id'])){ show_error("template composition_id must be numeric"); }
    if(!isset($d['composition'])){ show_error("missing composition"); }
    if(!isset($d['composition']['name'])){ show_error("missing composition.name"); }
    if(gettype($d['composition']['name']) !== "string" ){ show_error("composition.name must be string"); }
    if(!(strlen($d['composition']['name']) > 0)){ show_error("composition.name must not be empty"); }
    if(!isset($d['name'])){ show_error("name missing"); }
    if(gettype($d['name']) !== 'string'){ show_error("name must be string"); }
    if(strlen($d['name']) < 1){ show_error("name must not be empty"); }
    if(!isset($d['subset_id'])){ show_error("template subset_id missing"); }
    if(!is_numeric($d['subset_id'])){ show_error("template subset_id must be numeric"); }
    if(!isset($d['textslogan'])){ show_error("textslogan missing"); }
    if(gettype($d['textslogan']) !== 'string'){ show_error("textslogan must be string"); }
    if(isset($d['template_id']) && ($d['template_id'])){
      if(!is_numeric($d['template_id'])){ show_error("template_id when set must be numeric"); }
    }
    $t = R::load('template', $d['id']);
    if(!$t->id){ show_error("template could not be found for " . $d['id']); }
    if($t->composition->id !== $d['composition_id']){ show_error("composition id mismatch");}
    if($t->subset->id !== $d['subset_id']){ show_error("subset id mismatch"); }
    if($t->template){
      if($t->template->id !== $d['template_id']){ show_error("related template id mismatch"); }
    }
    return $t;
  }
  private function _validateClipUploadData($subclipId, $tempFile, $uploadFileName, $type){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator");}
    if (!is_numeric($subclipId)){ show_error("error - subclip id must be numeric"); }
    $sc = R::load('subclip', $subclipId);
    if(!$sc->id) { show_error("null reference exception 12"); }
    if (!is_file($tempFile)){ show_error("error - not a file: " . $tempFile); }
    if ($type !== "audio/mp3"){ show_error("error - not type audio/mp3: " . $type); }
    if (strrpos($uploadFileName, ".") !== (strlen($uploadFileName) -4)){ show_error("format error 1"); }
    $ext = substr($uploadFileName, strlen($uploadFileName) -4);
    if (!strtolower($ext) == ".mp3"){ show_error("format error 2"); }
    $name = substr($uploadFileName, 0, strlen($uploadFileName) -4);
    if(!(strlen($name) > 0)){ show_error("format error 3"); }
    return array("name"=>$name,"subclip"=>$sc);
  }
  private function _validateSaveScrollForProjectData($d){
    if(!isset($d['id'])){ show_error("project id not set"); }
    $p = R::load('project',$d['id']);
    if(!$p->id){ show_error("no project found for " . $d['id']); }
    
    if(!isset($d['ownScrollitem'])){ show_error("ownScrollitem not set"); }
    if(!is_array($d['ownScrollitem'])){ show_error("ownScrollitem not an array"); }
    
    // each element must have a template id and an order
    foreach($d['ownScrollitem'] as $otnd){
      if(!isset($otnd['id'])){ show_error("scrollitem id not set"); }
      $otn = R::load('scrollitem', $otnd['id']);
      if(!$otn->id) { show_error("scrollitem not found for " . $otnd['id']); }
      if(!isset($otnd['order'])){ show_error("scrollitem order not set"); }
      if(!is_numeric($otnd['order'])){ show_error("scrollitem order not numeric"); }
    }
    
    return $p;
  }
  private function _validateSaveStyleData($d){
    
    if(!(isset($d['id']))){ show_error("validating save style data missing property id"); }
    if(!(is_numeric($d['id']))){ show_error("validating save style data property id must be numeric"); }
    if(!(isset($d['name']))){ show_error("validating save style data missing property name"); }
    if(!(preg_match('/^[\w_]+$/i', $d['name']))) { show_error("validating save style data name cannot contain other than alphanumeric and underscore characters"); }
    
    $exists = R::findOne('style', ' id = ? AND name = ? ', array($d['id'], $d['name']));
    if(null == $exists){ show_error("no style can be stored to this id and name, it must exist first"); }
    
    //
    // -> if it has any subsets already it is impossible to change the data
    //
    
    if(count($exists->ownSubset) > 0){
      foreach($exists->ownSubset as $subset){
        if(count($subset->ownTemplate) > 0){
          foreach($subset->ownTemplate as $template){
            if(count($template->ownComposition) > 1){
              //show_error("you are not allowed to alter the values of a style that already has a subset with templates that has more than one composition");
            }
          }
        }
      }
    }
    
    
    if(!(isset($d['ruleset_id']))){ show_error("validating save style data missing property ruleset_id"); }
    if(!(is_numeric($d['ruleset_id']))){ show_error("validating save style data ruleset_id must be numeric"); }
    
    if($exists->ruleset->id !== $d['ruleset_id']){ show_error("validating save style data ruleset_id invalid value. " . $exists->ruleset->id . " vs " . $d['ruleset_id']); }
    
    if(!(isset($d['ruleset']))){ show_error("validating save style data missing property ruleset"); }
    if(!(is_array($d['ruleset']))){ show_error("validating save style data invalid type for ruleset property"); }
    
    if( $d['ruleset']['id'] !== $exists->ruleset->id ) { show_error("validating save style data ruleset.id invalid value. " . $d['ruleset']['id'] . " vs " . $exists->ruleset->id); }
    
    $dids = array();
    foreach($d['ruleset']['sharedRule'] as $dr){
      $dids[] = $dr['id'];
      if(!array_key_exists($dr['id'], $exists->ruleset->sharedRule)){ show_error("validating save style data rule not found in db version for id " . $dr['id']); }
      $dbrule = $exists->ruleset->sharedRule[$dr['id']];
      if(!($dbrule->type === $dr['type'])){ show_error("validating save style data rule types do not match for input rule id " . $dr['id'] . " alledgedly of type " . $dr['type']); }
    }
    
    if(count($exists->ruleset->ownStyle) > 1){
      show_error("you are not allowed to alter the properties of a ruleset that has more than one style attached to it");
    }
    
    foreach($exists->ruleset->sharedRule as $id => $v){
      if(!(in_array($id, $dids))){ show_error("validating save style data missing rule in input for id " . $id); }
      if(count($v->sharedRuleset) > 1){
        show_error("you are not allowed to alter the properties of rules that are used in more than one ruleset");
      }
    }
    return $exists;
  }
  private function _validateSubsetData($d){
    if(!isset($d['id'])){ show_error("subset id is not set"); }
    if(!is_numeric($d['id'])){ show_error("subset id invalid"); }
    if(!isset($d['name'])){ show_error("subset name is not set"); }
    if(!isset($d['ownSubpart'])){ show_error("subset has no subparts"); }
    if(!is_array($d['ownSubpart'])){ show_error("subset ownSubpart wrong type"); }
  }
  private function _addSubsetDataToSubset($d, $s){
    $s->mtime = time();
    $s->name = $d['name'];
  }
  private function _createDuplicatedSubsetForInputDataForExistingSubset($d, $old){
    $new = R::dispense('subset');
    $new->style = $this->_duplicateStyleWithRulesAndCopyrightForExistingSubset($old);
    $new->ctime = time();
    $new->mtime = time();
    $new->name = $d['name'];
    if($old->ownNoshare){
      $new->ownNoshare[] = R::dispense('noshare');
    }
    return $new;
  }
  private function _extractStyleBaseName($name){
    // _yyyyddmmhhmmss
    return strrpos( $name, '_') === strlen($name) - 15 ? substr($name, 0, strlen($name) - 15) : $name;
  }
  private function _generateAutomaticStyleName($baseName){
    return $baseName . '_' . date("YmdHis");
  }
  private function _duplicateStyleWithRulesAndCopyrightForExistingSubset($ss){
    if (false === $moderator = $this->_get_moderator()){ show_error("no moderator");}
    $newStyle = R::dispense('style');
    $newStyle->name = $this->_generateAutomaticStyleName( $this->_extractStyleBaseName($ss->style->name) );
    $newStyle->ruleset = R::dispense('ruleset');
    $oldRuleset = $ss->style->ruleset;
    $oldRules = $oldRuleset->sharedRule;
    foreach($oldRules as $ruleToCopy){
      $newRule = R::dispense('rule');
      $newRule->type = $ruleToCopy->type;
      $newRule->description = $ruleToCopy->description;
      $newRule->value = $ruleToCopy->value;
      $newStyle->ruleset->sharedRule[] = $newRule;
    }
    $newStyle->iconurl = $ss->style->iconurl;
    foreach($ss->style->ownCclimit as $ccl){
      $ncl = R::dispense('cclimit');
      $ncl->project = $ccl->project;
      $ncl->mix = $ccl->mix;
      $newStyle->ownCclimit[] = $ncl;
    }
    return $newStyle;
  }
  private function _validateSubpartData($d){
    if(!isset($d['id'])){ show_error("subpart id is not set"); }
    if(!is_numeric($d['id'])){ show_error("subpart id not numeric"); }
    if(!isset($d['name'])){ show_error("subpart name not set"); }
    if(!isset($d['active'])){ show_error("subpart active not set"); }
    if($d['active'] !== "0" && $d['active'] !== "1" && $d['active'] !== ""){ show_error("subpart active wrong format"); }
    if(!isset($d['marginx'])){ show_error("subpart marginx not set"); }
    if(!is_numeric($d['marginx'])){ show_error("subpart marginx not numeric"); }
    if(!isset($d['marginy'])){ show_error("subpart marginy not set"); }
    if(!is_numeric($d['marginy'])){ show_error("subpart marginy not numeric"); }
    if(!isset($d['order'])){ show_error("subpart order not set"); }
    if(!is_numeric($d['order'])){ show_error("subpart order not numeric"); }
    if(!isset($d['width'])){ show_error("subpart width not set"); }
    if(!is_numeric($d['width'])){ show_error("subpart width not numeric"); }
    if(!isset($d['ownSubclip'])){ show_error("subpart ownSubclip not set"); }
    if(!is_array($d['ownSubclip'])){ show_error("subpart ownSubclip wrong type"); }
  }
  private function _addSubpartDataToDuplicatedSubset($d, $s){
    if($d['active'] !== '1'){ return; }
    $p = R::dispense('subpart');
    $s->ownSubpart[] = $p;
    $p->name = $d['name'];
    $p->active = $d['active'] === "1";
    $p->marginx = intval($d['marginx']);
    $p->marginy = intval($d['marginy']);
    $p->order = intval($d['order']);
    $p->width = intval($d['width']);
    $p->ownSubclip = array();
    foreach($d['ownSubclip'] as $scd){
      $this->_validateSubclipData($scd);
      $this->_addSubclipDataToDuplicatedSubpart($scd, $p);
    }
  }
  private function _addSubpartDataToSubset($d, $s){
    $p = R::load("subpart", $d['id']);
    $p->name = $d['name'];
    $p->active = $d['active'] === "1";
    $p->marginx = intval($d['marginx']);
    $p->marginy = intval($d['marginy']);
    $p->order = intval($d['order']);
    $s->ownSubpart[] = $p;
    $p->width = intval($d['width']);
    $p->ownSubclip = array();
    foreach($d['ownSubclip'] as $scd){
      $this->_validateSubclipData($scd);
      $this->_addSubclipDataToSubpart($scd, $p);
    }
  }
  private function _validateSubclipData(&$d){
    if(!isset($d['id'])){ show_error("subclip id not set"); }
    if(!is_numeric($d['id'])){ show_error("subclip id wrong type"); }
    if(!isset($d['name'])){ show_error("subclip name not set"); }
    if(!isset($d['active'])){ show_error("subclip active not set"); }
    if($d['active'] !== "0" && $d['active'] !== "1"){ show_error("subclip active wrong format"); }
    if(!isset($d['tag'])){ show_error("subclip tag not set"); }
    if(!isset($d['order'])){ show_error("subclip order not set"); }
    if(!is_numeric($d['order'])){ show_error("subclip order not numeric"); }
    if(!isset($d['color'])){ show_error("subclip color not set"); }
    if((!preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/",$d['color'])) && ($d['color'] != "#------")){ show_error("subset color wrong format"); }
    if(isset($d['clip'])){
      if(!is_array($d['clip'])){ show_error("subclip clip wrong type"); }
      if(!isset($d['clip']['id'])){ show_error("subclip id not set"); }
      if(!is_numeric($d['clip']['id'])){ show_error("subclip id not numeric"); }
      if(!isset($d['clip']['exitpoint'])){ show_error("subclip exitpoint not set"); }
      if(!isset($d['clip']['entrypoint'])){ show_error("subclip entrypoint not set"); }
      if(!is_numeric($d['clip']['exitpoint'])){ show_error("subclip exitpoint not numeric"); }
      if(!is_numeric($d['clip']['entrypoint'])){ show_error("subclip entrypoint not numeric"); }
    }
    if(!isset($d['width'])){ $d['width'] = ''; }
    if(!isset($d['height'])){ $d['height'] = ''; }
    if(!$this->_is_valid_subclip_width_or_height($d['width'])){ show_error("subclip width bad format"); }
    if(!$this->_is_valid_subclip_width_or_height($d['height'])){ show_error("subclip height bad format"); }
  }
  private function _is_valid_subclip_width_or_height($v){
    // empty is allowed
    if($v === ''){ return true; }
    // can be percentage of clip dimension like 105% or 40%
    if(preg_match('/^\d+%$/', $v)){ return true; }
    // can be absolute value like 40px or relative value like +=40px or -=40px
    if(preg_match('/^((\+|\-)\=)?\d+px$/',$v)) { return true; }
    return false;
  }
  private function _validateAddTemplateConfigData($d){
    if(!array_key_exists("name", $d)){
      show_error("name property missing");
    }
    if(empty($d["name"])){
      show_error("name is empty");
    }
    if(!array_key_exists("value", $d)){
      show_error("value property missing");
    }
  }
  private function _validateDeleteTemplateConfigData($d){
    if(!array_key_exists("id", $d)){
      show_error("id property missing");
    }
    if(empty($d["id"])){
      show_error("id is empty");
    }
    if(!array_key_exists("templateId", $d)){
      show_error("templateId property missing");
    }
    if(empty($d["templateId"])){
      show_error("templateId is empty");
    }
    if(!is_numeric($d['id'])){
      show_error("id bad format");
    }
    if(!is_numeric($d['templateId'])){
      show_error('templateId bad format');
    }
  }
  private function _validateClipData($d){
    if(!isset($d['id'])){ show_error("clip id not set"); }
    //if(!is_file($this->config->item("clips_dir") . $d['id'] . '.MP3')){ show_error("no mp3 file for clip id " . $d['id']); }
    if(!isset($d['name'])){ show_error("clip name not set for clip id " . $d['id'] . " -> " . print_r($d,true)); }
    if(!isset($d['exitpoint'])){ show_error("clip exitpoint not set"); }
    if(!is_numeric($d['exitpoint'])){ show_error("clip exitpoint not numeric"); }
    if(intval($d['exitpoint']) < 0){ show_error("clip exitpoint must be positive value"); }
  }
  private function _addClipDataToSubclip($d, $s){
    $c = R::load("clip",$d['id']);
    if(!($c->id)){ show_error("no clip found in database for id " . $d['id']); }
    $c->name = $d['name'];
    $c->exitpoint = intval($d['exitpoint']);
    $c->entrypoint = intval($d['entrypoint']);
    $s->clip = $c;
  }
  private function _addSubclipDataToDuplicatedSubpart($d, $p){
    if($d['active'] !== "1"){ return; }
    $c = R::dispense("subclip");
    $p->ownSubclip[] = $c;
    $c->name = $d['name'];
    $c->active = $d['active'] === "1";
    $c->tag = $d['tag'];
    $c->icon = $d['icon'];
    $c->order = intval($d['order']);
    $c->color = $d['color'];
    $c->width = $d['width'];
    $c->height = $d['height'];
    if(isset($d['clip']['id'])){
      $this->_validateClipData($d['clip']);
      $c->clip = R::dispense('clip');
      $c->clip->exitpoint = $d['clip']['exitpoint'];
      $c->clip->entrypoint = $d['clip']['entrypoint'];
      $c->clip->name = $d['clip']['name'];
      $c->clip->parentid = $d['clip']['id'];
    }
  }
  private function _addSubclipDataToSubpart($d,$p){
    $c = R::load("subclip", $d['id']);
    $p->ownSubclip[] = $c;
    $c->name = $d['name'];
    $c->active = $d['active'] === "1";
    $c->tag = $d['tag'];
    $c->icon = isset($d['icon']) ? $d['icon'] : "";
    $c->ficon = isset($d['ficon']) ? $d['ficon'] : "";
    $c->ficonroll = isset($d['ficonroll']) ? $d['ficonroll'] : "";
    $c->order = intval($d['order']);
    $c->color = $d['color'];
    $c->width = $d['width'];
    $c->height = $d['height'];
    if(isset($d['clip']['id'])){
      $c->clip->exitpoint = $d['clip']['exitpoint'];
      $c->clip->entrypoint = $d['clip']['entrypoint'];
      $this->_validateClipData($d['clip']);
      $this->_addClipDataToSubclip($d['clip'], $c);
    }
  }
  private function _getStyleZip($r){
    $id = $r;
    
    $dir = FCF_DATA_DIR . "/tmp/";
    
    log_message("debug","checking tmp dir " . $dir);
    
    if(!is_dir($dir)){
      log_message("debug","does not exist, create: " . $dir);
      mkdir($dir);
    }
    
    shell_exec("rm -r " . $dir . "*");

    $style = R::load("style",$id);
    
    if(!$style->id){
      exit("style does not exist");
    }

    $styleName = $style->name;
    $styleNameRepl = str_replace(' ', '_', $styleName);
    $styleDir = $dir . $styleNameRepl . "/";

    mkdir($styleDir);
    
    $styleIconFile = $styleDir . "styleIcon.png";
    
    $CI =& get_instance();
    $styleIconSrc = $this->config->item('clients_url') . $CI->Session->projectClientId() . '/' . $style->iconurl;
    copy($styleIconSrc,$styleIconFile);
    
    $ruleSet = $style->ruleset;
    $rules = $ruleSet->sharedRule;
    $styleRules = array();
    foreach($rules as $id => $rule){
      $styleRules[] = array("type"=>$rule->type,"description"=>$rule->description,"value"=>$rule->value);
    }
    $rulesFile = $styleDir . "rules.json";
    
    $jsonRules = defined("JSON_PRETTY_PRINT") ? json_encode($styleRules, JSON_PRETTY_PRINT) : json_encode($styleRules);
    file_put_contents($rulesFile, $jsonRules);
    
    $parts = $style->sharedPart;
    foreach($parts as $id => $part){
      $partName = $part->name;
      $partDir = $styleDir . $partName . "/";
      mkdir($partDir);

      $clips = $part->ownClip;
      foreach($clips as $id => $clip){
        
        $clipName = $clip->name;
        
        $movement = $clip->sharedMovement;
        $movementName = $movement[key($movement)]->name;
        
        $clipFile = $partDir . $clipName . "-" . $movementName . "-" . $clip->exitpoint . ".MP3";
        $clipFileSrc = $this->config->item("clips_dir") . $clip->id . ".MP3";
        if(!file_exists($clipFileSrc)){
          $message = "clip file does not exist: " . $clip->id;
          log_message("error",$message);
          exit($message);
        }
        copy($clipFileSrc, $clipFile);
      }
    }
    chdir($dir);
    log_message("debug","files are in dir " . $dir);
    
    $exec = "zip -r " . $styleNameRepl . " " . $styleNameRepl . " 2>&1";
    log_message("debug","shell exec: " . $exec);
    
    shell_exec($exec);
    $this->load->helper('download');
    $data = file_get_contents($styleNameRepl . ".zip");
    $name = $styleNameRepl . ".zip";
    
    force_download($name, $data);
  }
  private function _get_moderator(){
    if(isset($this->_mod)) return $this->_mod;
    if(false === ($this->_mod = $this->ci->Session->getUserAsModerator())){
      parent::_error("11");
      return false;
    }
    parent::ok();
    return $this->_mod;
  }
  private function _loadValidTemplate($id){
    $tpl = R::load("template", $id);
    if(!$tpl->id){
      show_error("could not load template");
    }
    return $tpl;
  }
  private function _importMusicStyle($loc, $test = false) {
    $this->tlg($test ? "test mode" : "write mode");
    $this->tlg("suggested search keywords: created, existing, stored, not storing");
    if(substr($loc, strlen($loc)-1) != '/'){
      $loc .= '/';
    }
    //
    // hello, where are we?
    //
    $this->dlg('example error looks like this',true);
    $this->tlg('import music style from: ' . $loc);
    $slashSplit = explode('/', $loc);
    $dirname = $slashSplit[count($slashSplit)-2];
    $this->tlg('dir without path: ' . $dirname);
    //
    // one deeper?
    //
    if(is_dir($loc . $dirname)){
      $loc .= $dirname . '/';
      $this->tlg('extracted material loc: ' . $loc);
    }
    //
    // load style bean
    //
    $styleName = $dirname;
    $this->tlg("style name: " . $styleName);
    $st = R::findOrDispense("style"," name = ?",array($styleName));
    $st = $st[key($st)];
    $this->tlg("loaded bean");
    if(!($st->id)){
      $st->name = (string)$styleName;
      $this->tlg("created new style bean for " . $st->name);
      if($test){
        $this->tlg("test mode, not storing style bean");
      }else{
        R::store($st);
        $this->tlg("style bean stored to db, id: " . $st->id);
      }
    }else{
      $this->tlg("existing style bean, id = " . $st->id);
    }
    //
    // scan dir
    //
    $this->tlg("scanning style dir: " . $loc);
    $styleDirHandle = opendir($loc);
    while (FALSE !== ($partDir = readdir($styleDirHandle))) 
    {
      //
      // retrieve directories for parts
      //
      if( (!is_dir($loc . $partDir)) || $partDir == '.' || $partDir == '..'){
        $this->tlg("--skip: " . $partDir);
        continue;
      }
      $this->tlg('--PART: ' . $partDir);
      //
      // load part bean
      //
      $pt = null;
      foreach($st->sharedPart as $id => $ipt){
        if($ipt->name == $partDir){
          $pt = $ipt;
          $this->tlg("loaded existing part bean: " . $pt->id);
          break;
        }
      }
      if(!($pt)){
        $pt = R::dispense("part");
        $pt->name = $partDir;
        $this->tlg("created new part bean for " . $partDir);
        $st->sharedPart[] = $pt;
        if($test){
          $this->tlg("test mode, not storing part bean");
        }else{
          R::store($st);
          $this->tlg("write mode, part bean stored, id: " . $pt->id);
        }
      }
      //
      // retrieve audio files for clips
      //
      if (!($partDirHandle = opendir($loc . $partDir))) {
        $this->dlg('could not open directory: ' . $loc . $partDir);
      }else{
        $this->tlg("scanning part dir: " . $loc . $partDir);
      }
      while (FALSE !== ($clipFile = readdir($partDirHandle))) {
        log_message("debug","file found " . $clipFile);
        if (!(preg_match("/^[a-zA-Z0-9]+(-[0-9a-zA-Z]+)?(-[0-9]+)?.(wav|aiff|aif)$/", $clipFile))){
          $this->tlg("----skip: " . $clipFile);
          continue;
        }
        $this->tlg('----CLIP: ' . $clipFile);
        //
        // retrieve info from file name explode ('-')
        // (name, movement (optional, string), exitpoint (optional, numeric)
        //
        if ($dotPosition = strrpos($clipFile, '.wav')) {
          $nameWithoutExt = substr($clipFile, 0, $dotPosition);
        } elseif ($dotPosition = strrpos($clipFile, '.aif')) {
          $nameWithoutExt = substr($clipFile, 0, $dotPosition);
        } elseif ($dotPosition = strrpos($clipFile, '.aiff')) {
          $nameWithoutExt = substr($clipFile, 0, $dotPosition);
        }
        $this->tlg('noext: ' . $nameWithoutExt);
        $nameElements = explode("-", $nameWithoutExt);
        for ($i = 0; $i < count($nameElements); $i++) {
          switch ($i) {
            case(0):
              $clipName = $nameElements[$i];
              $this->tlg('name: ' . $clipName);
              break;
            case(1):
              if (preg_match("/^[0-9]+$/", $nameElements[$i])) {
                $clipExitPoint = $nameElements[$i];
                $this->tlg('exitpoint: ' . $clipExitPoint);
                $clipMovement = null;
              } else {
                $clipMovement = $nameElements[$i];
                $this->tlg('movement: ' . $clipMovement);
                $clipExitPoint = null;
              }
              break;
            case(2):
              $clipExitPoint = $nameElements[$i];
              $this->tlg('exitpoint: ' . $clipExitPoint);
              break;
          }
        }
        //
        // load clip bean
        //
        $cp = R::findOrDispense("clip"," part_id = ? AND name = ?", array($pt->id, $clipName));
        $cp = $cp[key($cp)];
        if(!$cp->id){
          $cp->name = $clipName;
          $cp->part = $pt;
          $cp->exitpoint = $clipExitPoint;
          $this->tlg("created clip bean for " . $clipName);
        }else{
          $this->tlg("loaded existing clip bean: " . $cp->id);
          $cp->exitpoint = $clipExitPoint;
        }
        if($test){
          $this->tlg("test mode, not storing clip bean");
        }else{
          R::store($cp);
          $this->tlg("stored clip bean for id: " . $cp->id);
        }
        //
        // write (replace) mp3 extraction
        //
        $lame = $this->config->item("lame_full_path");
        $sourceFileName = $loc . $partDir . '/' . $clipFile;
        $targetFileName = $this->config->item('clips_dir') . $cp->id . '.MP3';
        if($test){
          $this->tlg("test mode, not replacing " . $targetFileName);
        }else{
          if(file_exists($targetFileName)){
            if(false === unlink($targetFileName)){
              $this->dlg("could not remove " . $targetFileName);
            }else{
              $this->tlg("removed for replacement: " . $targetFileName);
            }
          }
        }
        $execStr = $lame . ' --resample 44.1 ' . $sourceFileName . ' ' . $targetFileName . ' > /dev/null; echo $?';
        $this->tlg( ($test ?"test mode, not executing: " : "write mode, will execute: ") . $execStr );
        if($test){
          $this->tlg("test mode, not executing");
        }else{
          $exitStatus = shell_exec($execStr);
          if ($exitStatus != 0) {
            $this->tlg('executing: ' . $execStr);
            $this->dlg('lame encoder FAIL: ' . $exitStatus);
          }else{
            $this->tlg('lame encoder SUCCESS');
          }
        }
        //
        // if set, associate to movement
        //
        if($clipMovement){
          $this->tlg("searching for movement associated to style: " . $clipMovement);
          $mv = null;
          $mvs = R::find("movement"," name = ?", array($clipMovement));
          foreach($mvs as $mid => $mvo){
            if(R::areRelated($mvo, $st)){
              $mv = $mvo;
              $this->tlg("existing movement found: " . $mv->id);
              break;
            }
          }
          if(!($mv)){
            $mv = R::dispense("movement");
            $mv->name = $clipMovement;
            $this->tlg("new movement created for name: " . $clipMovement);
            if($test){
              $this->tlg("test mode, not storing new movement...");
            }else{
              $mv->sharedStyle[] = $st;
              //R::associate($st, $mv); // this method was deprecated as of RedBeanPHP v4
              R::store($mv);
              $this->tlg("write mode, new movement stored for id: " . $mv->id);
            }
          }
          if(!(R::areRelated($cp, $mv))){
            $this->tlg("movement not yet associated to clip...");
            if($test){
              $this->tlg("not storing clip to movement association while in test mode");
            }else{
              $cp->sharedMovement[] = $mv;
              R::store($cp);
              //R::associate($cp,$mv); // this method was deprecated as of RedBeanPHP v4
              $this->tlg("clip to movement association stored");
            }
          }else{
            $this->tlg("movement already associated to clip");
          }
        }else{
          $this->tlg("clipMovement was not set");
        }
      }
      closedir($partDirHandle);
    }
    closedir($styleDirHandle);
  }
  private function tlg($m){
      log_message("debug",$m);
      echo '<br/>' . $m;
    }
    private function dlg($m,$surviveError = false){
      echo '<br/>######## ERROR ########';
      echo '<br/>';
      echo '<br/>' . $m;
      echo '<br/>';
      echo '<br/>######## END OF ERROR ########';
      log_message("error",$m);
      if(!($surviveError)){
        die();
      }
    }
  private function _remDir($path) {
    $this->tlg("REMOVE CONTENTS OF " . $path);
    $entries = scandir($path);
    foreach($entries as $entry){
      if($entry == "." || $entry == ".."){
        continue;
      }
      $fp = $path . "/" . $entry;
      if(is_file($fp)){
        $this->tlg("remove " . $fp);
        unlink($fp);
      }else{
        $this->_remDir($fp);
      }
    }
    rmdir($path);
  }
  private function _dirForZipFile($file){
    return substr($file, 0, strrpos($file, ".zip"));
  }
  private function _duplicateTemplate($t){
    $d = R::dispense('template');
    $d->ctime = time();
    $d->mtime = time();
    $d->name = $t->name . '_DPL';
    $d->subset = $t->subset;
    $d->textslogan = $t->textslogan;
    return $d;
  }
  private function _createDuplicate($c){
    $CI =& get_instance();
    $CI->load->model('mcomposition');
    $duplicatedTemplateComposition = $CI->mcomposition->duplicate($c, $CI->Session->getUser(), false, $c->name . '_DPL');
    $duplicatedTemplate = $this->_duplicateTemplate($c->template);
    $duplicatedTemplate->composition = $duplicatedTemplateComposition;
    $duplicatedTemplate->ownComposition[] = $duplicatedTemplateComposition;
    return $duplicatedTemplate;
  }
  private function _createTemplateForSubset($subset){
    $template = R::dispense("template");
    $template->mtime = time();
    $template->ctime = time();
    $template->slogan = "none";
    $template->name = $subset->name . "TPL";
    $composition = R::dispense("composition");
    $template->ownComposition[] = $composition;
    $composition->created = time();
    $composition->published = false;
    $composition->hasmp3 = false;
    $composition->name = $subset->name . "CMP";
    $composition->pubtime = 0;
    $composition->mtime = time();
    $template->composition = $composition;
    $subset->ownTemplate[] = $template;
    $composition->user = $this->Session->getUser();
    return $template;
  }
  private function _copyClipsForParentClips($subset){
    $dir = $this->config->item('clips_dir');
    foreach($subset->ownSubpart as $subpart){
      foreach($subpart->ownSubclip as $subclip){
        $pth = $dir . $subclip->clip->id . '.MP3';
        if(!file_exists( $pth)){
          $orig = $dir . $subclip->clip->parentid . '.MP3';
          if(file_exists($orig)){
            copy($orig, $pth);
          }
        }
      }
    }
  }
  private function _exportTemplate($template){
    $beanArr = array($template);
    R::preload($beanArr,'sharedConf|conf,composition|composition,*.user|user');
    $ar = R::beansToArray(array($template));
    $exp = reset($ar);
    $exp['stats'] = $this->_getTemplateStats($template);
    return $exp;
  }
  private function _getTemplateStats($template){
    return array(
        "compositionCount" => R::count('composition', ' template_id = ? ', array($template->id))
    );
  }
  private function _extractZip($loc){
    log_message('debug','extracting ' . $loc);
    $dir = $this->_dirForZipFile($loc);
    log_message("debug","destination dir: " . $dir);
    if(!(is_dir($dir))){
      if(false === mkdir($dir)){
        log_message("error","not existent and could not create: " . $dir . " in apimodel style");
        return FALSE;
      }
    }else{
      log_message("debug","already created, assuming zip extraction unnecessary");
      return $dir;
    }
		$cmd = 'unzip ' . $loc . ' -d ' . $dir;
		$output = shell_exec($cmd . ' 2>&1 1> /dev/null');
		log_message("debug", $output );
    return $dir;
  }
  
}