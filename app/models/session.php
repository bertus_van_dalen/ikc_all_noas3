<?php

class Session extends CI_Model {

  private $sessionBean;
  private $projectBean;
  private $token_max_seconds = 86400;
  private $token_cleanup_after_seconds = 86400;
  public $ROLE_NAME_ADMIN = "admin";
  public $ROLE_NAME_MODERATOR = "moderator";
  public $ROLE_NAME_AGENT = "agent";
  private $runtime;
  
  function __construct() {
    // call super constructor
    parent::__construct();

    if (!isset($_SESSION)) {
      session_start();
    }
    // update session expiration timestamp
    $ssid = session_id();
    setcookie("PHPSESSID",$ssid,time()+60*60*48,"/");
    // initialize data access layer
    $this->runtime = time();
    $this->sessionBean = FALSE;
    log_message("debug","Session try loading rb");
    $this->load->library('rb');
    if($this->rb->hasConnection()){
      $this->_initSessionBean();
    }
  }
  public function getAdminName(){
    return isset($_SESSION["install_admin"]) ? $_SESSION["install_admin"] : "";
  }
  public function getAdmin(){
    return array(
        "admin" => array("name" => $this->getAdminName()),
        "moderator" => $this->getModerator()
            );
  }
  public function logoutAdmin(){
    if(isset($_SESSION["install_admin"])) { unset($_SESSION["install_admin"]); }
    return $this->getAdmin();
  }
  public function loginAdmin($o){
    $CI =& get_instance();
    $CI->load->model("install");
    if($CI->install->adminNameHasPassword($o["loginName"], $o["loginPassword"])){
      $_SESSION["install_admin"] = $o["loginName"];
    }
    return $this->getAdmin();
  }
  public function getModerator(){
    $name = "";
    if($this->sessionBean && $this->sessionBean->user && $this->sessionBean->user->ownRolefor){
      $vm = $this->_moderatorViewModel($this->sessionBean->user->id);
      return $vm['moderator'];
    }
    return array("name" => $name);
  }
  public function matchModerator($viewValue){
    if(!$this->authAdmin()){ show_error("permission problem"); }
    return $this->_matchModerator($viewValue);
  }
  private function _matchModerator($viewValue){
    $q  = "SELECT u.name, u.id, u.provtab ";
    $q .= "FROM user u ";
    $q .= "JOIN rolefor rf ON rf.user_id = u.id ";
    $q .= "JOIN role r ON r.id = rf.role_id ";
    $q .= "WHERE r.name = ? ";
    $q .= "AND u.name LIKE ? ";
    $r = R::getAll($q, array('moderator', '%'.$viewValue.'%'));
    return $r;
  }
  public function impersonateModerator($m){
    if(!$this->authAdmin()){ show_error("permission problem"); }
    $options = $this->_matchModerator($m);
    $bns = R::convertToBeans( 'user', $options );
    if(count($bns) === 0){ show_error("not found for " . $m); }
    if(count($bns) > 1){ show_error("multiple found for " . $m); }
    $userBean = reset($bns);
    $tabRelation = 'own' . ucfirst($userBean->provtab);
    $providerBeans = $userBean->{$tabRelation};
    $this->_setUser(reset($providerBeans), false);
    return $this->_moderatorViewModel(reset($bns)->id);
  }
  private function _moderatorViewModel($userId){
    $u = R::load('user', $userId);
    $ur = array($u);
    R::preload($ur, "ownRolefor|rolefor,*.client|client,&.role|role");
    foreach($ur as $user){
      foreach($user->ownRolefor as $rolefor){
        unset($rolefor->client->guid);
      }
    }
    return array("moderator" => reset(R::beansToArray($ur)));
  }
  public function trySetFirstAdmin($name, $installModel){
    if($installModel->countAdministrators() === 0){
      $_SESSION["install_admin"] = $name;
      return true;
    }
    return false;
  }
  private function _initSessionBean(){
    $rb = R::getRedBean();
    if(!$rb->tableExists("session")){
      return;
    }
    $this->projectBean = reset(R::findOrDispense("project", " name = ?", array(FCF_CONF)));
    log_message("debug","Session constructor setting configured log threshold...");
    $logthresh = (int)$this->projectBean->logthresh;
    $this->log->setThreshold($logthresh);
    log_message("debug","logthresh set: " . $logthresh);
    
    // try to initialize the session object
    log_message("debug","if available init session");
    if(isset($_SESSION['session_pk'])){
      $this->sessionBean = R::load('session', $_SESSION['session_pk']);
    }else{
      $this->sessionBean = R::dispense('session');
      $this->sessionBean->ctime = $this->runtime;
    }
    $this->sessionBean->mtime = $this->runtime;
    R::store($this->sessionBean);
    $_SESSION['session_pk'] = $this->sessionBean->id;
    
    if($this->sessionBean->ctime === $this->sessionBean->mtime){
      $this->_setNewAnonUser();
    }
  }
  public function hasId(){
    return $this->sessionBean != FALSE;
  }
  
  public function authAdmin(){
    return $this->getAdminName() !== "";
  }

  /**
   * Does the session user have the moderator or admin permission?
   * @return mixed A user bean if the user is 'admin' or 'moderator', else false
   */
  public function getUserAsModerator() {
    if(count($this->sessionBean->user->ownRolefor) === 0) { return false; }
    return $this->sessionBean->user;
  }

  public function getStylesForProject(){
    $ret = array();
    $q = '
      SELECT 
        s.*, 
        cl.project_id as cclimit_project_id, 
        cl.mix as cclimit_mix 
      FROM 
        style s 
      LEFT JOIN 
        cclimit cl ON cl.style_id = s.id
    ';
    
    $rows = R::getAll($q);
    foreach ($rows as $i => $row) {
      if(!$row['cclimit_project_id']){
        $ret[$row['id']] = $row;
      }
      if($row['cclimit_project_id'] == $this->projectBean->id && $row['cclimit_mix']){
        $ret[$row['id']] = $row;
      }
    }
    return $ret;
  }
  /**
   * Get all the properties of the session user that can be relevant to the client application:
   * - user has value zero unless he has a profile, then the profile properties are attached
   * - token property, a fresh random token
   * - lang property, e.g. 'nl', 'en'
   * 
   * @return mixed the properties
   */
  public function getStatus() {
    log_message("debug","session->getStatus");
    $ret = new stdClass();
    $ret->user = 0;
    if(!$this->sessionBean->id){
      log_message("error","NO SESSIONBEAN");
      exit;
    }
    if(!$this->sessionBean->user->id){
      log_message("error","NO SESSIONBEAN->USERBEAN");
      exit;
    }
    if ($this->sessionBean->user) {
      $ret->user = $this->sessionBean->user->getProperties();
      $ret->user["active"] = $this->sessionBean->user->provtab != "userprovoidanon";
      $ret->user["stylesEnabled"] = array();
      $u = $this->sessionBean->user;
    }
    $CI =& get_instance();
    $CI->load->model("mcomposition");
    $ret->scroll = $CI->mcomposition->getScrollItems();
    /*$ret->db->style = $this->getStylesForProject();*/
    $ret->token = $this->setToken();
    
    $sql = " user_id = ? ORDER BY mtime DESC LIMIT 1 ";
    $ucpr = R::findOne('composition', $sql, array($u->id));
    
    if($ucpr === null){
      log_message("debug","#######--------NULL it is null for " . $_SERVER['REQUEST_URI']);
    }else{
      log_message("debug","#######--------OK it is " . $ucpr->id . " for " . $_SERVER['REQUEST_URI']);
    }
    
    if($ucpr !== null){
      $cclimits = $ucpr->template->subset->style->ownCclimit;
      $limited = count($cclimits) > 0;
      if($limited){
        foreach($cclimits as $cclimitId => $cclimit){
          if($cclimit->project->id == $this->projectId()){
            $limited = false;
            break;
          }
        }
      }
      if($limited){
        $ucpr = null;
      }
    }
    
    
    $ret->recent_cp = $ucpr === null ? -1 : $ucpr->id;
    
    return $ret;
  }

  /**
   * Sets a new anonymous user to the session.
   * @return boolean false meaning nothing
   */
  public function setNewAnonUser() {
    $this->_setNewAnonUser();
    R::store($this->sessionBean);
  }

  /**
   * Trashed items ownership are transferred to the trash user and you can retrieve the trash user here.
   * @return RedBean_OODBBean user the trash user
   */
  public function getTrashUser() {
    $trash = R::findOne("userprovoidgoogle", " openidid = ?", array("https://www.google.com/profiles/117742232667036797969"));
    if (!$trash) {
      log_message("debug", "could not retrieve trash user");
      return false;
    }
    if (!$trash->id) {
      log_message("debug", "could not retrieve trash user");
      return false;
    }
    return $trash->user;
  }

  /**
   * Validates a response string against the project configured clients private key.
   * In retrieving the session token it is destroyed (it is for one time usage).
   * @param type $responseString
   * @return boolean success
   */
  public function validateResponseString($responseString) {
    if (false === ($t = $this->getToken())){
      return "invalid token";
    }
    log_message("debug","retrieved token " . $t);
    $privateKey = $this->projectBean->client->guid;
    log_message("debug","retrieved private key " . $privateKey);
		if(md5($privateKey . $t) != $responseString){
      if(md5($t . $privateKey) != $responseString){
        log_message("debug","response string did not match");
        return "response string did not match";
      }
    }
    log_message("debug","successfull response string validation");
    return true;
  }
  public function getKeyHash($t){
    $privateKey = $this->projectBean->client->guid;
    log_message("debug","getKeyHash for client " . $this->projectBean->client->id);
    log_message("debug","using key " . $privateKey);
    return md5($t . $privateKey);
  }

  /**
   * After successfull openID login, sets the Janrain auth info array information to the database for this user.
   * @param type $auth_info_array
   */
  public function setJanrainAuth($auth_info_array) {
    log_message("debug", "Session->setJanrainAuth()");

    $providerName = $auth_info_array["profile"]["providerName"];
    $providerName = strtolower($providerName);
    $providerName = str_replace(" ", "", $providerName);

    $providerTable = "userprovoid" . $providerName;
    $openidid = $auth_info_array["profile"]["identifier"];

    log_message("debug", "looking into " . $providerTable . " for " . $openidid);

    $jnrUser = R::findOrDispense($providerTable, ' openidid = ? ', array($openidid));
    $jnrUser = $jnrUser[key($jnrUser)];

    if ($jnrUser->id == 0) {
      log_message("debug", "created user");
      $jnrUser->openidid = $openidid;
    } else {
      log_message("debug", "found existing user");
    }

    $fields = $this->flattenFields($auth_info_array["profile"]);
    log_message("debug", "storing / updating fields");
    $jnrUser->fields = json_encode($fields);

    log_message("debug", "retrieving base user");
    if (!($jnrUser->user)) {
      $jnrUser->user = R::dispense("user");
      $jnrUser->user->provtab = $providerTable;
      log_message("debug", "base user created");
    } else {
      log_message("debug", "existing base user found");
    }

    if (!($jnrUser->user->profile)) {
      $jnrUser->user->profile = R::dispense("profile");
      log_message("debug", "created profile for base user");
    }
    if (!$jnrUser->user->profile->providername) {
      $jnrUser->user->profile->providername = $fields["providerName"];
      log_message("debug", "providerName set in profile: " . $fields["providerName"]);
    }
    if (!$jnrUser->user->profile->extsignout) {
      $jnrUser->user->profile->extsignout = $fields["url"];
      log_message("debug", "extsignout set in profile: " . $fields["url"]);
    }
    if (!$jnrUser->user->profile->username) {
      if ($fields["displayName"]) {
        $jnrUser->user->profile->username = $fields["displayName"];
      } else if ($fields["preferredUserName"]) {
        $jnrUser->user->profile->username = $fields["preferredUserName"];
      } else if ($fields["preferredUsername"]) {
        $jnrUser->user->profile->username = $fields["preferredUsername"];
      } else if ($fields["name_formatted"]) {
        $jnrUser->user->profile->username = $fields["name_formatted"];
      } else {
        $jnrUser->user->profile->username = $this->randomString(8);
      }
      $jnrUser->user->name = $jnrUser->user->profile->username;
      log_message("debug", "username set in profile: " . $jnrUser->user->profile->username);
    }
    if (array_key_exists("photo", $fields)) {
      if ($fields["photo"] != "") {
        $jnrUser->user->profile->photo = $fields["photo"];
        log_message("debug", "photo updated in profile: " . $fields["photo"]);
      } else {
        log_message("debug", "photo field was empty");
      }
    } else {
      log_message("debug", "no photo field in auth info");
    }

    $this->_setUser($jnrUser);
  }

  private function _setUser($jnrUser, $keepAnonCompositions = true) {
    log_message("debug", "getting previously stored assets");
    $anonU = $this->sessionBean->user;
    if($keepAnonCompositions){
      foreach ($anonU->ownComposition as $id => $anonComp) {
        log_message("debug","current anon session has a composition for id " . $anonComp->id );
        $jnrUser->user->ownComposition[] = $anonComp;
      }
    }else{
      log_message("debug","will not keep anon compositions");
    }
    log_message("debug", "will now store auth data...");
    $this->sessionBean->user = $jnrUser->user;
    R::store($this->sessionBean);
    R::store($jnrUser);
    log_message("debug", "auth data store OK");
  }

  /**
   * After successfull client handshake login, sets the username and (per client unique) user guid.
   * If there is no such user in the DB it is created. The user is set to the session.
   * @param type $userName
   * @param type $userGuid
   */
  public function setNpoUser($userName, $userGuid) {

    log_message("debug", "Session->setNpoUser");

    $providerTable = "userprovcli";
    $providerTable .= $this->projectBean->client->name;
    log_message("debug", "looking into " . $providerTable . " for " . $userGuid);
    
    $npoUser = R::findOrDispense($providerTable, ' userguid = ? ', array($userGuid));
    $npoUser = $npoUser[key($npoUser)];

    if ($npoUser->id == 0) {
      log_message("debug", "created user");
      $npoUser->userguid = $userGuid;
    } else {
      log_message("debug", "found existing user");
    }

    $npoUser->username = $userName;
    log_message("debug", "updating username to " . $npoUser->username);

    log_message("debug", "retrieving base user");
    if (!($npoUser->user)) {
      log_message("debug", "creating base user");
      $baseUser = R::dispense("user");
      $npoUser->user = $baseUser;
      $baseUser->provtab = $providerTable;
    } else {
      $baseUser = $npoUser->user;
      log_message("debug", "retrieved existing base user " . $baseUser->id);
    }

    if (!($baseUser->profile)) {
      $baseUser->profile = R::dispense("profile");
      log_message("debug", "creating profile");
    }
    if (!$baseUser->profile->providername) {
      $baseUser->profile->providername = $this->projectBean->client->name;
      log_message("debug", "provider name set in profile: " . $baseUser->profile->providername);
    }
    if (!$baseUser->profile->extsignout) {
      $baseUser->profile->extsignout = "ikc_cli_first";
      log_message("debug", "extsignout set in profile: " . "ikc_cli_first");
    }
    if (!$baseUser->profile->username) {
      $baseUser->profile->username = $userName;
      log_message("debug", "username name set in profile: " . $userName);
    }

    $this->_setUser($npoUser, false);
  }

  /**
   * Gives you the session user.
   * @return RedBean_OODBBean user
   */
  public function getUser() {
    $u = $this->sessionBean->user;
    return $u;
  }
  
  public function projectId(){
    return $this->projectBean->id;
  }
  public function projectCbmode(){
    return $this->projectBean->cbmode;
  }
  public function getConfs(){
	  return $this->projectBean->sharedConf;
  }
  public function projectAllowsOpenIdLogin(){
    return $this->projectBean->client->loginoid == true;
  }
  public function projectAllowsNpoLogin(){
    return $this->projectBean->client->logincli == true;
  }
  public function projectClientId(){
    return $this->projectBean->client->id;
  }
  public function projectProdLocs(){
    log_message("debug","session->getProdLocs() for project " . $this->projectBean->name);
    $r = array();
    $r[] = $this->projectBean->domain->baseurl;
    foreach($this->projectBean->sharedDomain as $id => $domain){
      log_message("debug","adding " . $domain->baseurl);
      $r[] = $domain->baseurl;
    }
    return $r;
  }
  public function projectUrl(){
    return $this->projectBean->domain->baseurl;
  }
  public function projectTitle(){
    return $this->projectBean->title;
  }
  public function projectBean(){
    return $this->projectBean;
  }
  private function flattenFields($digArr, $prefix = "") {
    $ret = array();
    foreach ($digArr as $k => $v) {
      if (is_array($v)) {
        $ret = array_merge($ret, $this->flattenFields($v, $k . "_"));
      } else {
        $ret[$prefix . $k] = $v;
      }
    }
    return $ret;
  }

  public function refreshToken(){
    return $this->setToken();
  }
  /**
   * Generates a new random string of 32 characters and returns it.
   * @return string the token.
   */
  private function setToken() {
    $min_time = time() - $this->token_cleanup_after_seconds;
    R::trashAll(R::find('token', ' t < ? ', array($min_time)));
    log_message("debug", "session->setToken");
    $this->sessionBean->token = R::dispense('token');
    $this->sessionBean->token->rand = $this->randomString(32);
    $this->sessionBean->token->t = time();
    R::store($this->sessionBean);
    log_message("debug", "stored session with new token");
    log_message("debug", "token properties: " . json_encode($this->sessionBean->token->getProperties()));
    log_message("debug", "session properties: " . json_encode($this->sessionBean->getProperties()));
    return $this->sessionBean->token->rand;
  }

  /**
   * Retrieves the token stored on this session if it is not too old.
   * You can do this only once because it is invalited whence.
   * @return string random string i.e. token
   */
  private function getToken() {
    log_message("debug", "session->getToken");
    $t = $this->sessionBean->token;
    log_message("debug", "retrieving and invalidating token " . $t);
    $rand = $t->rand;
    $time = $t->t;
    $this->sessionBean->token->t = time() - $this->token_max_seconds;
    R::store($this->sessionBean);
    log_message("debug", "token invalidated");
    if (time() - $time > $this->token_max_seconds) {
      log_message("debug", "token too old");
      return false;
    }
    log_message("debug", "token not too old, returning");
    return $rand;
  }

  private function randomString($length) {
    $str = "";
    $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    for ($i = 0; $i < $length; $i++) {
      $str .= substr($chars, rand() % 33, 1);
    }
    return $str;
  }

  /**
   * Is the user associated with a roll named $roleName?
   * 
   * @param string $roleName use one of the private vars $this->ROLE_NAME_...
   * @param RedBean_OODBBean $user if null, the session user is used.
   * 
   * @return boolean
   */
  public function userHasRole($roleName, $user = null) {
    if ($user == null) {
      if (!($this->sessionBean->user)) {
        return false;
      } else {
        $user = $this->sessionBean->user;
      }
    }
    switch ($roleName) {
      case $this->ROLE_NAME_ADMIN:
        break;
      case $this->ROLE_NAME_MODERATOR:
        break;
      case $this->ROLE_NAME_AGENT:
        break;
      default:
        die("invalid role name");
        break;
    }
    if (!($user->sharedRole)) {
      return false;
    }
    $role = R::findOne("role", " name = ?", array($roleName));
    if (!($role)) {
      die("role does not exist");
    }
    if (array_key_exists($role->id, $user->sharedRole)) {
      return true;
    }
    return false;
  }

  private function _setNewAnonUser() {
    log_message("debug","creating anonymous user");
    $provtab = "userprovoidanon";
    $pu = R::dispense($provtab);
    $pu->user = R::dispense("user");
    $pu->user->provtab = $provtab;
    $pu->ctime = time();
    $pu->user->name = 'ikc_anon';
    $this->sessionBean->user = $pu->user;
    $this->sessionBean->mtime = $this->runtime;
    R::store($pu);
    R::store($this->sessionBean);
    log_message("debug","anonymous user stored to session");
    //log_message("info", print_r($this->sessionBean,true));
  }

}

?>
