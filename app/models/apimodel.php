<?php
class Apimodel extends CI_Model{
	public function __construct() {
    log_message("debug","constructing model ApiModel");
    parent::__construct();
    log_message("debug","ApiModel constructed");
  }
  private $r; // result
  protected function setData($r){
    if (isset($this->r)) {
      print_r($this->r);
      exit;
      log_message("error", "setData() - already set!");
      return false;
    }
    if(null == $r){
      return true;
    }
    if(!(array_key_exists("data", $r))){
      $r["errors"][] = $this->get_error("6");
      return false;
    }
    if(!(is_array($r["data"]))){
      $r["errors"][] = $this->get_error("7");
      return false;
    }
    $this->r = &$r;
    return true;
  }
  protected function paramExists($paramName){
    if(!(array_key_exists($paramName, $this->r["data"]))){
      $this->r["errors"][] = $this->get_error("1", $paramName, "");
      return false;
    }
    return $this->r["data"][$paramName];
  }
  private $booleishChecked = array();
  protected function hasBooleishValue($paramName){
    $this->booleishChecked[] = $paramName;
    if(!($this->paramExists($paramName))) return false;
    $v = $this->r["data"][$paramName];
    if(!($v === true || $v === false || $v === "1" || $v === "0")){
      $this->r["errors"][] = $this->get_error("8", $paramName, $v);
      return false;
    }
    return true;
  }
  protected function getBooleishValue($paramName){
    if(!(in_array($paramName, $this->booleishChecked))){
      log_message("error","FATAL - booleish values must first be checked using hasBooleishValue");
      die();
    }
  }
  protected function getStringValue($paramName){
    if(!($this->paramExists($paramName))) return false;
    if(!(is_string($this->r["data"][$paramName]))){
      $this->r["errors"][] = $this->get_error("9", $paramName, $this->r["data"][$paramName]);
      return false;
    }
    return $this->r["data"][$paramName];
  }
  private function validateIdForType($type,$return_zero_if_none = false){
    $numID = 0;
    if(array_key_exists("id", $this->r["data"][$type])){
      if(false === ($numID = $this->getIntValue("id"))){
        $this->_error("14","id",$type);
      }else{
        return $numID;
      }
    }else if(!$return_zero_if_none){
      $this->_error("13", "", $type);
      return false;
    }
    return true;
  }
  protected function getIntValue($paramName){
    if(false === ($param = $this->paramExists($paramName))){
      return;
    }
    if(!(is_numeric($param))){
      $this->_error("18", $paramName);
      return false;
    }
    $num = 0;
    $num += $param;
    if(!(is_int($num))){
      $this->_error("18", $paramName);
      return false;
    }
    return $num;
  }
  protected function getBeanID($type,$allow_inexistent_table = false){
    if(!($this->paramExists($type))) return false;
    $rb = R::getRedBean();
    if($rb->isFrozen()) $allow_inexistent_table = false;
    if(!($allow_inexistent_table)){
      if(!($rb->tableExists($type))){
        $this->_error("12", "", $type);
        return false;
      }
    }
    return $this->validateIdForType($type);
  }
  protected function getBeanProperties($type,$allow_inexistent_table = false){
    if(!($this->paramExists($type))) return false;
    $rb = R::getRedBean();
    if($rb->isFrozen()) $allow_inexistent_table = false;
    if(!($allow_inexistent_table)){
      if(!($rb->tableExists($type))){
        $this->_error("12", "", $type);
        return false;
      }
    }
    if(false === ($this->r["data"][$type]["id"] = $this->validateIdForType($type,true))){
      return false;
    }
    return $this->r["data"][$type];
  }
  protected function setResults($results){
    $this->r["results"] = $results;
    return true;
  }
  protected function addResult($addition){
    if(!(is_array($this->r["results"]))){
      $this->_error("10");
      return false;
    }
    $this->r["results"][] = $addition;
    return true;
  }
  protected function ok(){
    $this->r["status"] = "ok";
  }
  protected function _error($errorCode,$fieldName = "",$fieldValue = ""){
    $this->r["errors"][] = $this->get_error($errorCode,$fieldName,$fieldValue);
  }
  private function get_error($errorCode,$fieldName = "",$fieldValue = ""){
		$this->load->helper("errors");
		$this->load->model("Session");
		return ikc_get_error($errorCode,$fieldName,$fieldValue,'nl');
	}
	public function hasMethod($methodName,$r){
		if(!(method_exists($this, $methodName))){
			$r["errors"][] = $this->get_error("5");
			return false;
		}
		return true;
	}
}