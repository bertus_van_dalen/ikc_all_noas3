<?php

include_once(APPPATH . "models/apimodel.php");

class Moderator extends Apimodel {
  public function __construct() {
    if(FCF_CONF === 'none') die('no conf');
    log_message("debug","constructing model moderator");
    parent::__construct();
    log_message("debug","moderator constructor ok");
  }
  public function usermessage($r){
    if(!(parent::setData(&$r))) return;
    if(false === $moderator = $this->getModerator()) return;
    if(false === ($recipientUserID = parent::getBeanID("user"))) return;
    $recipientUser = R::load("user",$recipientUserID);
    if(!$recipientUser->id){
      parent::_error("15","user",$recipientUserID);
      return;
    }
    if(!($this->_userIsOfModerableClient($recipientUser))){
      parent::_error("11","user",$recipientUserID);
      return;
    }
    if(false === ($messageP = parent::getBeanProperties("message",true))) return;
    $message = R::load("message",$messageP["id"]);
    foreach($messageP as $k => $v){
      $message->$k = $v;
    }
    $recipientShip = R::dispense("recipient");
    $recipientShip->user = $recipientUser;
    $message->ownRecipient[] = $recipientShip;
    $message->user = $moderator;
    R::store($message);
    parent::ok();
  }
  public function trashsong($r){
    if(!(parent::setData(&$r))) return;
    if(false === $moderator = $this->getModerator()) return;
    if(false === ($compositionID = parent::getBeanID("composition"))) return;
    $cp = R::load("composition",$compositionID);
    if(!$cp->id){
      parent::_error("15","id",$compositionID);
      return;
    }
    if(!($this->_userIsOfModerableClient($cp->user))){
      parent::_error("11","id",$compositionID);
      return;
    }
    if(false === ($trashUser = $this->Session->getTrashUser())){
      parent::_error("16");
      return;
    }
    $cp->user = $trashUser;
    $cp->pubtime = time();
    R::store($cp);
    if(false === $this->_unread($cp)){
      parent::_error("17");
      return;
    }
    parent::ok();
  }
  public function approvesong($r){
    if(!(parent::setData(&$r))) return;
    if(false === $moderator = $this->getModerator()) return;
    if(false === ($compositionID = parent::getBeanID("composition"))) return;
    $cp = R::load("composition",$compositionID);
    if(!$cp->id){
      parent::_error("15","id",$compositionID);
      return;
    }
    if(!($this->_userIsOfModerableClient($cp->user))){
      parent::_error("11","id",$compositionID);
      return;
    }
    if(false === $this->_unread($cp)){
      parent::_error("17");
      return;
    }
    parent::ok();
  }
  private $_mod;
  private function getModerator(){
    if(isset($this->_mod)) return $this->_mod;
    $ci = get_instance();
    $ci->load->model("Session");
    if(false === ($this->_mod = $ci->Session->getUserAsModerator())){
      parent::_error("11");
      return false;
    }
    return $this->_mod;
  }
  private function _userIsOfModerableClient($user){
    log_message("error","not implemented - model moderator -> _userIsOfModerableClient");
    die("not implemented");
    if(true === $this->Session->isUserOfThisClient($user)) return true;
    $others = $this->config->item("moderate_clients");
    foreach($others as $id){
      if(true === ($allow = $this->Session->isUserOfThisClient($user,R::load("client",$id)))){
        return true;
      }
    }
    return false;
  }
  private function _unread($cp){
    $approvalcomp = R::dispense("approval");
    $approvalcomp->user = $this->getModerator();
    $approvalcomp->composition = $cp;
    R::store($approvalcomp);
  }
}