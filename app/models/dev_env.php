<?php

class Dev_env extends CI_Model{
  private $DB_CF_FILE;
  private $_exportsDir;
  private $_ZIP_FILE_NAME = 'ikcomponeer.tar.gz';
	function __construct(){
    parent::__construct();
    $this->_exportsDir = $this->config->item('devenv_exports_dir');
    $this->DB_CF_FILE = FCF_PRIV_CACHE_DIR . '/dbconf/db_conf.json';
	}
	public function GetAllExports($post = null){
    if($post){
      if(!isset($_SESSION["install_admin"])){
        show_error("permission problem");
      }
      if(isset($post["command"])){
        $this->_applyCommand($post);
        unset($post["command"]);
        if(isset($post["commandSubject"])){
          unset($post["commandSubject"]);
        }
      }
    }
    return $this->_viewModel();
  }
  private function _applyCommand($post){
    if($post["command"] == "remove"){
      $this->_checkName($post["commandSubject"]);
      $this->_remove($post["commandSubject"]);
    }else if($post["command"] == "download"){
      $this->_checkName($post["commandSubject"]);
      $this->_download($post["commandSubject"]);
    }else if($post["command"] == "create"){
      $this->_export($post["selected"], isset($post["addComps"]) ? $post["addComps"] : null);
    }
  }
  private function _getExportDir($name, $required = false){
    $dir = $this->_exportsDir . $name;
    if(!is_dir($dir)){
      if($required || is_file($dir)){
        show_error("cannot get required directory " . $dir);
      }else{
        mkdir($dir);
      }
    }
    return $dir;
  }
  private function _getExportBaseUrl(){
    return FCF_DATA_URL . '/devenv/exports/';
  }
  private function _getDownloadUrl($name){
    return $this->_getExportBaseUrl() . $name . '/' . $this->_ZIP_FILE_NAME;
  }
  private function _getExportMetaFile($name, $required = false){
    $dir = $this->_getExportDir($name, $required);
    $file = $dir . '/export.json';
    if(!file_exists($file) && $required){
      show_error("no file " . $file);
    }
    return $file;
  }
  private function _checkName($name){
    if(!ctype_alnum($name)){
      show_error("bad characters in export name: " . $name, 500);
    }
  }
  private function _export($name, $compositions = null){
    $this->_checkName($name);
    $exportVars = new stdClass();
    $exportVars->exportName = $name;
    $exportVars->status = "new";
    $exportVars->addComps = $compositions ? array_map(function($v){ return $v["id"]; }, $compositions) : array();
    file_put_contents($this->_getExportMetaFile($name), json_encode($exportVars));
    $this->_startExportSemiAsync($name);
  }
  private function _viewModel(){
    $viewModel["loggedIn"] = isset($_SESSION["install_admin"]);
    if($viewModel["loggedIn"]){
      $viewModel["exports"] = $this->_getExportDirectoryNames();
    }
    return $viewModel;
  }
  private function _getExportDirectoryNames(){
    $directories = scandir($this->_exportsDir);
    $out = array();
    foreach($directories as $name){
      if($name === '.' || $name === '..') { 
        continue;
      }
      if(!is_dir($this->_exportsDir . $name)){
        show_error('file is not a directory: ' . $name, 500);
      }
      $vars = json_decode(file_get_contents($this->_getExportMetaFile($name, true)));
      $out[] = $vars;
    }
    return $out;
  }
  private function _startExportSemiAsync($name){
    $url = $this->config->item("base_url") . 'manage/startExportAsync/' . $name;
    if(false === $ch = curl_init()){
      show_error("could not initialize curl for " . $url);
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1 );
    curl_exec($ch);
    curl_close($ch);
  }
  private function _setStatus($exportVars, $status){
    $exportVars->status = $status;
    log_message("debug","status set to " . $exportVars->status . " for " . $exportVars->exportName);
    $this->_storeExportVars($exportVars);
  }
  private function _storeExportVars($exportVars){
    $fl = $this->_getExportMetaFile($exportVars->exportName);
    if(false === file_put_contents($fl, json_encode($exportVars))){
      show_error("the export vars could not be written to file " . $fl);
    }
  }
  private function _setDownloadUrl($exportVars){
    $exportVars->downloadUrl = $this->_getDownloadUrl($exportVars->exportName);
    log_message("debug","downloadUrl set to " . $exportVars->downloadUrl . " for " . $exportVars->exportName);
    $this->_storeExportVars($exportVars);
  }
  public function StartExport($name){
    log_message("debug","starting export " . $name);
    $exportVars = json_decode(file_get_contents($this->_getExportMetaFile($name, true)));
    log_message("debug","retrieved export vars for " . $name . ": " . print_r($exportVars,true));
    
    $this->_setStatus($exportVars, "preparing to export code");
    sleep(3);
    
    $this->_setStatus($exportVars, "exporting code");
    set_time_limit(30);
    $this->_copyCode($name);
    
    $this->_setStatus($exportVars, "preparing to export database");
    sleep(3);
    
    $this->_setStatus($exportVars, "exporting database");
    set_time_limit(300);
    $this->_exportDb($name);
    
    $this->_setStatus($exportVars, "preparing to export audio");
    sleep(3);
    
    $this->_setStatus($exportVars, "exporting audio");
    set_time_limit(300);
    $this->_exportAudio($name, $exportVars->addComps);
    
    $this->_setStatus($exportVars, "preparing to zip");
    sleep(3);
    
    set_time_limit(300);
    $this->_setStatus($exportVars, "zipping");
    $this->_zip($name);
    
    $this->_setStatus($exportVars, "preparing download");
    sleep(3);
    
    $this->_setStatus($exportVars, "done");
    $this->_setDownloadUrl($exportVars);
  }
  private $_COPY_CODE_LIST = array(
        APPPATH, // applications
        'ci/', // code igniter framework
        'ckfinder/', // file manager
        '.htaccess', // server configurations
        'crossdomain.xml', // allow from other domains
        'favicon.ico', // icon for the browser tab
        'index.php', // main script file
        'whoami.php', // helper file for installation
        'p3p.xml', // privacy policy description
        'ng/', // angular and css for management pages
        'data/css', // css files
        'data/js', // js files
        'data/swf', // flash files
        'data/clients' // img files
        );
  private function _exportAudio($name, $compIds){
    $clipIdList = array();
    foreach($compIds as $compId){
      $clipIdList = array_merge($clipIdList, $this->_clipIdsForCompostionId($compId));
    }
    $this->_copyClips($name, $clipIdList);
  }
  private function _copyClips($name, $clipIdList){
    $srcBase = FCPATH . 'data/clips8/';
    $destBase = $this->_getExportDir($name, true) . '/public_html/data/clips8/';
    mkdir($destBase, 0777, true);
    foreach($clipIdList as $i => $clipId){
      $source = $srcBase . $clipId . '.MP3';
      $dest = $destBase . $clipId . '.MP3';
      copy($source, $dest);
    }
  }
  private function _clipIdsForCompostionId($id){
    $cb = R::load('composition', $id);
    $tpl = $cb->template;
    $ret = array();
    $subset = $tpl->subset;
    $subparts = $subset->ownSubpart;
    foreach($subparts as $subpart){
      if(!$subpart->active) { continue; }
      $subclips = $subpart->ownSubclip;
      foreach($subclips as $scID => $subclip){
        if(!$subclip->active){ continue; }
        
        $ret[] = $subclip->clip->id;
      }
    }
    return $ret;
  }
  private function _copyCode($name){
    $copyList = $this->_COPY_CODE_LIST;
    $srcBase = FCPATH;
    $destBase = $this->_getExportDir($name, true) . '/public_html/';
    mkdir($destBase);
    log_message("debug","created dir " . $destBase);
    $this->_copyFiles($srcBase, $destBase, $copyList);
  }
  private function _getDumpDir($name){
    $dumpDir = $this->_getExportDir($name, true) . '/public_html/db_install/';
    mkdir($dumpDir);
    return $dumpDir;
  }
  private function _getDumpScript($dumpDir){
    $dumpScript = $dumpDir . "dump.bash";
    touch($dumpScript);
    chmod($dumpScript, 0755);
    return $dumpScript;
  }
  private function _GET_BASH_SOURCE_DIR(){
    return APPPATH . 'versioning/';
  }
  private function _define_outfile_var($out_file, $append_write_handler){
    $bash = file_get_contents($this->_GET_BASH_SOURCE_DIR() . '_init_outfile.bash');
    $replace = array('{{out_path}}' => $out_file);
    $bashRepl = str_replace(array_keys($replace), array_values($replace), $bash);
    fwrite($append_write_handler, $bashRepl);
  }
  private function _define_database_vars($append_write_handler){
    $bash = file_get_contents($this->_GET_BASH_SOURCE_DIR() . "_init_credentials.bash");
    $dc = json_decode(file_get_contents($this->DB_CF_FILE));
    $replace = array(
        '{{db_user}}' => $dc->username, 
        '{{db_pass}}' => $dc->password, 
        '{{db_db}}' => $dc->database, 
        '{{db_host}}' => $dc->hostname);
    $bashRepl = str_replace(array_keys($replace), array_values($replace), $bash);
    fwrite($append_write_handler, $bashRepl);
  }
  private function _dump_db_structure($append_write_handler){
    $bash = file_get_contents($this->_GET_BASH_SOURCE_DIR() . "_dump_structure.bash");
    fwrite($append_write_handler, $bash);
  }
  private function _zip($name){
    $zipRoot = $this->_getExportDir($name, true) . '/public_html/';
    $zipFile = '../' . $this->_ZIP_FILE_NAME;
    $zipDir = '.';
    $zipCommand = 'cd ' . $zipRoot . ' && tar cvpzf ' . $zipFile . ' ' . $zipDir;
    log_message("debug","will execute zip command: " . $zipCommand);
    $this->_exec($zipCommand);
  }
  private function _exportDb($name){
    $dumpDir = $this->_getDumpDir($name);
    $dumpScript = $this->_getDumpScript($dumpDir);
    $wh = fopen($dumpScript, 'a');
    
    $this->_define_outfile_var($dumpDir . "db.sql", $wh);
    $this->_define_database_vars($wh);
    $this->_dump_db_structure($wh);
    
    //$this->_insert_table_contents($wh);
    
    fclose($wh);
    $this->_exec($dumpScript);
  }
  private function _exec($commandLineScript){
    $message=shell_exec($commandLineScript . " 2>&1");
    log_message("debug","executed command line script: " . $message);
  }
  private function _copyFiles($srcBase, $destBase, $copyList){
    $this->load->helper("fs");
    foreach($copyList as $path){
      $s = $srcBase . $path;
      $d = $destBase . $path;
      log_message("debug","try copy " . $s . " to " . $d);
      fs_copy_all($s, $d);
    }
  }
  private function _remove($name){
    $dir = $this->_exportsDir . $name;
    $this->load->helper("fs");
    fs_delete_all($dir);
    rmdir($dir);
  }
  private function _download($name){
    log_message("debug","download: " . $name);
  }
}