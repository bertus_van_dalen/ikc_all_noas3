if(typeof(ikc) == "undefined") ikc = {};
ikc.musbas = {
  submit : function(e){
    var $form = ikc.musbas.getForm(e.target);
    $fields = $form.find(".ikc-formfield");
    var keyvals = {};
    $fields.each(function(k,v){
      keyvals[v.id] = $(v).val();
    })
    ikc.musbas.process[$form.attr("id")](keyvals,$form);
  },
  getForm : function(elem){
    while(!($(elem).hasClass("ikc-form"))){
      elem = elem.parentElement;
    }
    return $(elem);
  },
  process : {
    create_template_for_template : function(data, $formElem){
      if(!(data.hasOwnProperty("template_id"))){
        alert ("property template_id missing");
      }
      // require a subset from the back-end
      var busy = $("#ikc_busy_creating_template_TPL").render();
      $("#ikc_workspace").html(busy);
      //
      // setup for ajax
      //
      var url = ikc.conf.project_url + "musbas/createTemplateOfTemplate/" + data.template_id;
      var dataType = 'json';
      var success = function(data){
        var editLink = $("#ikc_success_creating_template_TPL").render(data);
        $("#ikc_workspace").html(editLink);
      }
      ikc.ajax.send(url,dataType,success);
    },
    create_musicpool : function(data, $formElem){
      if(!(data.hasOwnProperty("directory_name"))){
        alert ("missing property directory_name");
      }
      
      var busy = $("#ikc_busy_TPL").render();
      $("#ikc_workspace").html(busy);
      
      var url = ikc.conf.project_url + "musbas/createMusicPoolForDir/";
      var success = function(data){
        var editLink = $("#ikc_success_creating_musicpool_TPL").render(data);
        $("#ikc_workspace").html(editLink);
      }
      ikc.ajax.send(url,"json",success,null,data);
    },
    create_template_from_subset : function(data, $formElem){
      if(!(data.hasOwnProperty("subset_id"))){
        alert ("property subset_id missing");
      }
      if(!(ikc.db.subsets)){
        alert("no subsets in dataset");
      }
      var subset = null;
      $.each(ikc.db.subsets, function(k,v){
        if(v.id == data.subset_id){
          subset = v;
          return false; // = break
        }
      });
      if(subset == null){
        alert("this is not a subset, please type the correct id without spaces...");
        return;
      }
      // require a subset from the back-end
      var busy = $("#ikc_busy_creating_template_TPL").render();
      $("#ikc_workspace").html(busy);
      //
      // setup for ajax
      //
      var url = ikc.conf.project_url + "musbas/createTemplateOfSubsetForId/" + subset.id;
      var dataType = 'json';
      var success = function(data){
        var editLink = $("#ikc_success_creating_template_TPL").render(data);
        $("#ikc_workspace").html(editLink);
      }
      ikc.ajax.send(url,dataType,success);
    },
    create_subset_from_set : function(data, $formElem){
      if(!(data.hasOwnProperty("soundset_id"))){
        alert ("property soundset_id missing");
      }
      if(!(ikc.db.styles)){
        alert("no styles in dataset");
      }
      var style = null;
      $.each(ikc.db.styles, function(k,v){
        if(v.id == data.soundset_id){
          style = v;
          return false; // = break
        }
      });
      if(style == null){
        alert("this is not a style, please type the correct id without spaces...");
        return;
      }
      // require a subset from the back-end
      var busy = $("#ikc_busy_creating_subset_TPL").render();
      $("#ikc_workspace").html(busy);
      //
      // setup for ajax
      //
      var url = ikc.conf.project_url + "musbas/createSubsetOfSoundsetForId/" + style.id;
      var dataType = 'json';
      var success = function(data){
        var editLink = $("#ikc_success_creating_subset_TPL").render(data);
        $("#ikc_workspace").html(editLink);
      }
      ikc.ajax.send(url,dataType,success);
    },
    delete_subset_for_id : function(data,$formElem){
      if(!(data.hasOwnProperty("subset_id"))){
        alert ("property subset_id missing");
      }
      var url = ikc.conf.project_url + "musbas/deleteSubset/" + data.subset_id;
      var dataType = 'json';
      var success = function(data){
        window.location.href = ikc.conf.project_url + "musbas/editSubset";
      }
      ikc.ajax.send(url,dataType,success);
    },
    delete_template_for_id : function(data,$formElem){
      if(!(data.hasOwnProperty("template_id"))){
        alert ("property template_id missing");
      }
      var url = ikc.conf.project_url + "musbas/deleteTemplate/" + data.template_id;
      var dataType = 'json';
      var success = function(data){
        window.location.href = ikc.conf.project_url + "musbas/editTemplate";
      }
      ikc.ajax.send(url,dataType,success);
    },
    own_template_for_id : function(data,$formElem){
      if(!(data.hasOwnProperty("template_id"))){
        alert ("property template_id missing");
      }
      var url = ikc.conf.project_url + "musbas/ownTemplateForId/" + data.template_id;
      var dataType = 'json';
      var success = function(data){
        alert("It should be yours now. go to the My Songs Screen to see it this title appears...")
      }
      ikc.ajax.send(url,dataType,success);
    },
    edit_subset_for_id : function(data,$formElem){
      if(!(data.hasOwnProperty("subset_id"))){
        alert ("property subset_id missing");
      }
      window.location.href = ikc.conf.project_url + "musbas/editSubset/" + data.subset_id;
    },
    edit_template_for_id : function(data,$formElem){
      if(!(data.hasOwnProperty("template_id"))){
        alert ("property template_id missing");
      }
      window.location.href = ikc.conf.project_url + "musbas/editTemplate/" + data.template_id;
    }
  }
}
window.onload = function(){
  $(".ikc-formsubmit").click(ikc.musbas.submit);
};