ikc.mco10car = {
  removeIndexFromCarousel : function(v){
    this.initCarouselIndexArray();
    var vint = parseInt(v);
    var arrind = this.instrumentGroupClicked.done.indexOf(vint);
    this.instrumentGroupClicked.done.splice(arrind,1);
  },
  iFrameModal : function(iframe,title){
    var xmldoc = $R.parseXML(iframe);  
    var xmlifr = $R(xmldoc).children()[0];
    var wdt = $R(xmlifr).attr("width");
    var hgt = $R(xmlifr).attr("height");
    $R( "#movieViewer" ).html(iframe);
    $R( "#movieViewer" ).show();	
    $R( "#movieViewer" ).dialog({
      close: function(event, ui) {
        $R("#movieViewer").empty();
      },
      modal: true,
      height: parseInt(hgt)+100,
      width: parseInt(wdt)+40,
      title: title
    });
  },
  adt_media : [
    ["http://www.youtube.com/embed/eAnglPCT5bs","MCO-making-off-piano.jpg","De opnames voor Ikcomponeer"],
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20051123_hobo01","Hobo.jpeg","Houtblazers"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20051123_viool01","Viool.jpeg", "Strijkers"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20060411_harp01","Harp.jpeg", "Harp"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20051123_hoorn01","Hoorn.jpeg", "Koperblazers"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20051123_percussie01","Percussie.jpeg", "Percussie"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20110608_houtblazers01","NTR-houtblazer.jpg","Houtblazers"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20110608_viool01","NTR-violist.jpg", "Strijkers"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20110608_hoorn01","NTR-koperblazer-hoorn.jpg","Koperblazers"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20110608_harp01","NTR-divers-harp.jpg","Harp"], // DONE
    ["http://www.schooltv.nl/beeldbank/embedded.jsp?clip=20051123_trompet01","trompet.jpeg","Koperblazers"], // DONE
    ["http://www.youtube.com/embed/J5JbIyJMZOw","MCO-making-off-hout.jpg","Houtblazers"], // DONE
    ["http://www.youtube.com/embed/I9XUugg4TXE","MCO-making-off-pauken.jpg","Percussie"], // DONE
    ["http://www.youtube.com/embed/FFqfJ_oUSRQ","MCO-making-off-strijkers.jpg","Strijkers"], // DONE
    ["http://www.youtube.com/embed/9hsbZ_7aLW0","MCO-making-off-piano.jpg","Piano"]	// DONE
  ],
  addIndexToCarousel : function(v){
    this.initCarouselIndexArray();
    this.instrumentGroupClicked.done.push(v);
  },
  initCarouselIndexArray : function(){
    if(typeof(this.instrumentGroupClicked.done) === "undefined") this.instrumentGroupClicked.done = [];
  },
  isIndexInCarousel : function(v){
    this.initCarouselIndexArray();
    this.instrumentGroupClicked.done.indexOf(v) !== -1;
  },
  instrumentGroupClicked : function(v){
    if(typeof(v) === "undefined"){
      var notInCarousel = false;
      while(!notInCarousel){
        v = Math.floor(Math.random() * (this.adt_media.length));
        notInCarousel = !this.isIndexInCarousel(v);
      }
    }
    if(this.isIndexInCarousel(v)) return;
    this.addIndexToCarousel(v);
    var tojs = v+"";
    var flim = this.adt_media[tojs][0];
    var voto = this.adt_media[tojs][1];
    var tpl = flim.indexOf("yout")>=0?$R.templates.iFrameYouTube:$R.templates.iFrameSchoolTv;
    var iframe = tpl.render({ url:flim });
    var imgurl = ikc.conf.client_url + "images/video_thumbs/" + voto;
    var img = $R.templates.fotoPlaatje.render({url:imgurl,index:v});
    $R("#mycarousel").prepend(img);
    var that = this;
    $R("#mycarousel div").each(function(index,element){
      if(index === 0){
        $R(element).css({
          "width": "128px",
          "height": "72px",
          "top":"25px", 
          "left":"27px"
        });
        $R(element).show("slide",{},200);
        $R("img",element).bind('click', {self:that}, function(e) {
          e.data.self.iFrameModal(iframe,e.data.self.adt_media[tojs][2]);
        });
        $R("img",element).mouseover(function(){
          $R(this).css("cursor","pointer");
        });
      }
      else if(index === 1){
        $R(element).animate({
          left:"157px"
        });
      }else if(index === 2){
        $R(element).animate({
          left:"287px"
        });
      }else if(index === 3){
        $R(element).animate({
          left:"417px"
        });
      }else if(index === 4){
        $R(element).animate({
          left:"547px"
        });
      }else if(index === 5){
        $R(element).animate({
          left:"677px"
        });
      }else if(index === 6){
        $R(element).animate({
          left:"807px"
        });
      }else{
        that.removeIndexFromCarousel(element.getAttribute("id"));
        $R(element).fadeOut(600, function(){ $R( this ).remove(); });
      }
    });
  }
};
ikc.l.r({callback:function(){
  $R.templates("iFrameYouTube", '<iframe width="560" height="315" src="{{:url}}" frameborder="0"></iframe>');
  $R.templates("iFrameSchoolTv", '<iframe src="{{:url}}" width="350" height="198" marginheight="0" marginwidth="0" frameborder="0" scrolling="no"></iframe>');
  $R.templates("fotoPlaatje", '<div id={{:index}}><img src={{:url}} class="thumb"></img><img src="data/css/images/clickToPlay.png" class="button"></img></div>');
  for(var c = 0; c < 7; c++){
    ikc.mco10car.instrumentGroupClicked(c);
  }
  $R("#mycarousel a").click(function(){
    ikc.mco10car.instrumentGroupClicked();
  });
}});





  