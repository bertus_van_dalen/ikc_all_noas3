ikc.steps = {
  
  init : function(){
    
  },
  
  currentPath : "",
  
  currentIndex : -1,
  
  addressChange : function(e){
    
    if(e.pathNames.length === 0) 
    { 
      return; 
    }
    
    ikc.steps.handleAddressChange(e.pathNames[0]);
  },
  handleAddressChange : function(path){
    
    if(path === this.currentPath) 
    {
      return;
    }
    this.currentPath = path;
    
    if(path.substr(0,4) === "ikc_"){
      ikc.s.recent_cp = path.substr(4);
      this.templateToLoad = "ikcomponeer_mps_ikc";
    }else{
      this.templateToLoad = this.currentPath;
    }
    this.loadTemplateToLoad();
  },
  loadTemplateToLoad : function(){
    if ($R.templates[this.templateToLoad])
    {
      this.insertTemplate(this.templateToLoad);
    }
    else
    {
      var that = this;
      $R.get( ikc.conf.js_url + this.templateToLoad + ".html?cb" + new Date().getTime() , null, function(d){
        $R.templates(that.templateToLoad, d);
        that.insertTemplate(that.templateToLoad);
      }, 'html' );
    }
  },
  insertTemplate : function(path){
    if(path === "ikcomponeer_mps_ikc"){
      var currentPage = Enumerable.From(ikc.s.scroll).Where(function(x){ return x.id === ikc.s.recent_cp; }).FirstOrDefault()
      var curIdx = Enumerable.From(ikc.s.scroll).OrderBy('$.order').IndexOf(currentPage);
    }
    var rev = curIdx > -1 && curIdx < this.currentIndex;
    this.currentIndex = curIdx > -1 ? curIdx : this.currentIndex;
    if(path.indexOf("_ikc") === -1) { $R("#cclogo").hide(); }
    else { $R("#cclogo").show(); }
    
    var embedContainer = $R("#" + ikc.l.d);
    var left = rev ? "-960px" : "960px";
    var newDiv = $R('<div class="ikc-contentBox" style="left:' + left + ';width:960px;height:821px;position:absolute;background-color:black;"/>');
    newDiv[rev ? "prependTo" : "appendTo"](embedContainer);
    var oldDiv = $R('.ikc-active').first();
    oldDiv.removeClass('ikc-active');
    newDiv.addClass('ikc-active');
    newDiv.html($R.templates[path].render({}));
    
    if(path.indexOf("_ikc") !== -1){
      var el = $R("#ikcDivNew");
      var elId = "ikcDivNew_" + new Date().getTime();
      el.attr("id", elId);
      ikc.startComponeertool(elId);
    }
    
    var horizontalShift = (rev ? "+=" : "-=") + "960";
    setTimeout(function(){
      $R(".ikc-contentBox").animate({left:horizontalShift},2000,function(){
        $R('.ikc-contentBox').not('.ikc-active').remove();
      });
    }, 1000);
  },
  
  templatePages : {
    ikcomponeer_mps_start_help              : { path : "ikcomponeer_mps_start_help", type : "staticPage" },
    auteursrechtelijk                       : { path : "auteursrechtelijk", type : "staticPage"  },
    informatie_voor_docenten                : { path : "informatie_voor_docenten", type : "staticPage"  },
    vraag_en_antwoord                       : { path : "vraag_en_antwoord", type : "staticPage"  },
    privacy                                 : { path : "privacy", type : "staticPage"  },
    contactformulier                        : { path : "contactformulier", type : "staticPage" }
  },
  
  ikcShift : function(left){
    if(this.currentIndex === -1){
      this.ikcShiftOnIdx(0);
    }else{
      if(left){
        if(this.currentIndex === 0){
          return;
        }else{
          this.ikcShiftOnIdx(this.currentIndex -1);
        }
      }else{
        if(this.currentIndex === ikc.s.scroll.length -1){
          return;
        }else{
          this.ikcShiftOnIdx(this.currentIndex +1);
        }
      }
    }
  },
  
  ikcShiftOnIdx : function(idx){
    var id = Enumerable.From(ikc.s.scroll).OrderBy('$.order').ElementAt(idx).id;
    this.ikcSetPath(id);
  },
  
  ikcSetPath : function(id){
    $R.address.path("ikc_" + id);
  },
  
  ikcSetPathNoChange : function(id){
    var pth = "ikc_" + id;
    this.currentPath = pth;
    $R.address.path(pth);
  }
};

ikc.mps = {
  submitNieuwsbriefAddress : function(){
    var address = $R("#nieuwsbriefEmail").val();
    $R.ajax({
      type: "POST",
      url: ikc.conf.base_url + "info/nieuwsbrief",
      data: {uwMailAdres:address},
      success: function(){
        $R("#nieuwsbriefEmailFeedback").html("<p>Dank u wel voor uw inschrijving, deze is succesvol verwerkt.</p>");
      },
      error: function(d){
        $R("#nieuwsbriefEmailFeedback").html(d.responseText);
      }
    });
  },
  submitContactForm : function(){
    var contactFormName                = $R('#contactFormName').val(); 
    var contactFormMessage             = $R('#contactFormMessage').val();   
    var contactFormEmail               = $R('#contactFormEmail').val();
    var contactFormPhone               = $R('#contactFormPhone').val();
    var contactFormIsTechnicalProblem  = $R('#contactFormIsTechnicalProblem').is(':checked');
    var contactFormIsSuggestion        = $R('#contactFormIsSuggestion').is(':checked');
    var contactFormIsInquiry           = $R('#contactFormIsInquiry').is(':checked');
    var contactFormGrecaptchaResponse  = grecaptcha.getResponse();
    var data = {
      contactFormName               : contactFormName,
      contactFormMessage            : contactFormMessage,
      contactFormEmail              : contactFormEmail,
      contactFormPhone              : contactFormPhone,
      contactFormIsTechnicalProblem : contactFormIsTechnicalProblem,
      contactFormIsSuggestion       : contactFormIsSuggestion,
      contactFormIsInquiry          : contactFormIsInquiry,
      contactFormGrecaptchaResponse : contactFormGrecaptchaResponse
    };
    $R.ajax({
      type: "POST",
      url: ikc.conf.base_url + "info/contactform",
      data: data,
      success: function(){
        $R("#contactFormSubmitButton").attr("disabled", true);
        $R("#contactFormFeedback").html("<p>Dank u wel voor uw bericht, er zal zo snel mogelijk contact met u worden opgenomen.</p>");
      },
      error: function(d){
        $R("#contactFormFeedback").html(d.responseText);
      }
    });
  }
};

ikc.l.r({callback:function(){
    ikc.steps.init();
}});

var ikc_api_onOpeningComp = function(id){
  ikc.steps.ikcSetPathNoChange(id);
};