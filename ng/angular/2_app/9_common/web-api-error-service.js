manage.service('WebApiError', ['$modal', function($modal) {
    this.handle = function(response) {
      var ModalInstanceCtrl = function($scope, $modalInstance, htmlContent, $sce) {
        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
        $scope.htmlContent = htmlContent;
        $scope.deliberatelyTrustDangerousSnippet = function() {
          return $sce.trustAsHtml($scope.htmlContent);
        };
      };
      var modalInstance = $modal.open({
        templateUrl: ikc.conf.angular_templates + 'error-modal.html',
        controller: ModalInstanceCtrl,
        size: 'lg',
        resolve: {
          htmlContent: function () {
            return response.responseText;
          }
        }
      });
    }
  }]);




