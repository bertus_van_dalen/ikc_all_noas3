 manage.controller(
        'TestE2EController', 
        [
          '$scope',
          'TestE2ERepo',
          function(
            $scope, 
            repo)
{
  $scope.button = function(){
    repo.rpc("install", "button", {});
  };
}]);
manage.factory ('TestE2ERepo', ['$http','$q', 'WebApiError', function ($http, $q, webApiError) { 
  return {
    apiUrl:"manage/install/",
    get: function () { 
      var d = $q.defer();
      $.get(this.apiUrl, function(data) {
        d.resolve(data);
      }, 'JSON').fail( webApiError.handle );
      return d.promise;
    },
    post: function(data, api_url) {
      if(typeof(api_url) === "undefined") api_url = this.apiUrl;
      var d = $q.defer();
      $.post(api_url, data, function(data){
        d.resolve(data);
      }, 'JSON').fail(function(r){ d.reject(r); webApiError.handle(r);});
      return d.promise;
    },
    rpc : function(model, command, args){
      return this.post({model:model, command:command, args:args}, 'manage/rpc');
    }
  }; 
}]); 