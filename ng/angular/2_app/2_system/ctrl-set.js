manage.controller('SetController',[
  '$scope','$window','AdminService','StyleService',function (
    $scope,
    $window,
    adminSrv,
    styleSrv){
      
      $scope.moderator = adminSrv.moderator;
      $scope.style = styleSrv;
      
      $scope.browseServer = function(selector){
        $window.ikc.browseServer(selector);
      };
  
      styleSrv.initialize();
      
    }]);