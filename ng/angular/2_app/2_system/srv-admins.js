  manage.factory('AdminsService', ['ManageRepository', 'AdminService', function(repo, adminSrv) {
  return {
    admins: {editName: "", editPassword: "", admin: adminSrv.admin},
    writeAdmin: function(){
      var that = this;
      repo.rpc('install', 'writeAdmin', this.admins.admin_edit ).then(function(r){
        that.xt(r);
      });
    },
    selectEditAdmin: function(){
      var that = this;
      repo.rpc("install", "selectAdmin", this.admins.select_edit_admin.file).then(function(r){
        that.xt(r);
      });
    },
    selectDeleteAdmin: function(){
      var nameToDelete = this.admins.select_delete_admin.name;
      m = "Type 'yes' if you are sure to delete admin account for " + nameToDelete + '?';
      var sure = prompt(m);
      if (sure === 'yes') {
        var that = this;
        repo.rpc("install", "deleteAdmin", this.admins.select_delete_admin.file).then(function(r){
          that.xt(r);
        });
      }
    },
    xt : function(r){
      $.extend(this.admins, r);
      if(r.currentAdmin){
        adminSrv.setAdmin(r.currentAdmin);
      }
    },
    get: function(flush){
      if(typeof(flush) === 'undefined') flush = false;
      if((!this.admins.list) || flush){
        var that = this;
        repo.rpc('install','getAdmins',{}).then(function(r){
          that.xt(r);
        });
      }
    }
  };
}]);