 manage.factory('ProjbasService', ['ManageRepository', 'MusbasService', function(repo, musbas) {
  var projbasService = {
    musbas: musbas,
    fprojects: [],
    sproject: {},
    editedProject: {},
    get: function(){
      musbas.get();
      var that = this;
      repo.rpc('style','getProjectList').then(function(r){
        that.fprojects.length = 0;
        $.extend(that.fprojects, r);
      });
    },
    loadScrollForProject: function(){
      var that = this;
      repo.rpc('style','loadScrollForProject', this.sproject.id).then(function(r){
        $.extend(that.sproject, r);
      });
    },
    saveScrollForProject: function(){
      repo.rpc('style','saveScrollForProject', angular.copy(this.sproject)).then(function(r){
        alert("success saving scroll for project!");
      });
    },
    sortScroll : function(){
      var sorted = Enumerable.From(this.sproject.ownScrollitem).OrderBy(function(i){ return parseInt(i.order); }).ToArray();
      this.sproject.ownScrollitem.length = 0;
      $.extend(this.sproject.ownScrollitem, sorted);
    },
    addScrollitem : function(tpl){
      var that = this;
      repo.rpc('style','addScrollitem', {template_id: tpl.template_id, project_id: this.sproject.id}).then(function(r){
        that.sproject.ownScrollitem.push(r);
      });
    },
    deleteScrollItem : function(id){
      var that = this;
      repo.rpc('style','deleteScrollItem', id).then(function(r){
        var index = Enumerable.From(that.sproject.ownScrollitem).IndexOf(Enumerable.From(that.sproject.ownScrollitem).Where(function(i){ return i.id === id; }).First());
        that.sproject.ownScrollitem.splice(index, 1);
      });
    }
  };
  return projbasService;
}]);