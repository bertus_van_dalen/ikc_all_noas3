manage.controller(
        'ViewAgentsController', 
        [
          '$scope',
          'ViewAgentsRepo',
          function(
            $scope, 
            repo)
{
  repo.getLastHour().then(function(data){
    var WIDTH = 1000;
    var HEIGHT = 250;
    var MARGINS = {
      top: 20,
      right: 20,
      bottom: 20,
      left: 50
    };
    var tasksGraph = d3.select('#tasksGraph');
    var agentsGraph = d3.select('#agentsGraph');
    var bli = 0;
    var tMin = Enumerable.From(data).Select(function(i){ return i.t; }).Min();
    var tMax = Enumerable.From(data).Select(function(i){ return i.t; }).Max();
    var agentsMin = Enumerable.From(data).Select(function(i){ return parseInt(i.agents); }).Min();
    var agentsMax = Enumerable.From(data).Select(function(i){ return parseInt(i.agents); }).Max();
    var tasksMin = Enumerable.From(data).Select(function(i){ return parseInt(i.tasks); }).Min();
    var tasksMax = Enumerable.From(data).Select(function(i){ return parseInt(i.tasks); }).Max();
    var xRangeTasks = d3.scale.linear();
    xRangeTasks.range([MARGINS.left, WIDTH - MARGINS.right]);
    xRangeTasks.domain([tMin,tMax]);
    var yRangeTasks = d3.scale.linear();
    yRangeTasks.range([HEIGHT - MARGINS.top, MARGINS.bottom]);
    yRangeTasks.domain([tasksMin,tasksMax]);
    var xAxisTasks = d3.svg.axis()
      .scale(xRangeTasks)
      .tickSize(5)
      .tickSubdivide(true);
    var yAxisTasks = d3.svg.axis()
      .scale(yRangeTasks)
      .tickSize(5)
      .orient('left')
      .tickSubdivide(true);
    tasksGraph.append('svg:g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')')
      .call(xAxisTasks);
    tasksGraph.append('svg:g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(' + (MARGINS.left) + ',0)')
      .call(yAxisTasks);
    var lineFuncTasks = d3.svg.line()
      .x(function(d) {
        return xRangeTasks(d.t);
      })
      .y(function(d) {
        return yRangeTasks(d.tasks);
      })
      .interpolate('linear');
    tasksGraph.append('svg:path')
      .attr('d', lineFuncTasks(data))
      .attr('stroke', 'blue')
      .attr('stroke-width', 2)
      .attr('fill', 'none');
      
    var xRangeAgents = d3.scale.linear();
    xRangeAgents.range([MARGINS.left, WIDTH - MARGINS.right]);
    xRangeAgents.domain([tMin,tMax]);
    var yRangeAgents = d3.scale.linear();
    yRangeAgents.range([HEIGHT - MARGINS.top, MARGINS.bottom]);
    yRangeAgents.domain([0,agentsMax]);
    var xAxisAgents = d3.svg.axis()
      .scale(xRangeAgents)
      .tickSize(5)
      .tickSubdivide(true);
    var yAxisAgents = d3.svg.axis()
      .scale(yRangeAgents)
      .tickSize(5)
      .orient('left')
      .tickSubdivide(true);
    agentsGraph.append('svg:g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + (HEIGHT - MARGINS.bottom) + ')')
      .call(xAxisAgents);
    agentsGraph.append('svg:g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(' + (MARGINS.left) + ',0)')
      .call(yAxisAgents);
    var lineFuncAgents = d3.svg.line()
      .x(function(d) {
        return xRangeAgents(d.t);
      })
      .y(function(d) {
        return yRangeAgents(d.agents);
      })
      .interpolate('linear');
    agentsGraph.append('svg:path')
      .attr('d', lineFuncAgents(data))
      .attr('stroke', 'blue')
      .attr('stroke-width', 2)
      .attr('fill', 'none');
  });
}]);
manage.factory ('ViewAgentsRepo', ['$http','$q', 'WebApiError', function ($http, $q, webApiError) { 
  return {
    getLastHour : function(){
      var d = $q.defer();
      $.get("ecasound/agentStats/hour", function(data) {
        d.resolve(data);
      }, 'JSON').fail( webApiError.handle );
      return d.promise;
    }
  }; 
}]);