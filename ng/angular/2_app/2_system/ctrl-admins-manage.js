manage.controller(
        'ManageAdminsController', 
        [
          '$scope',
          'AdminsService',
          function(
            $scope, 
            adminsSrv)
{
  $scope.admins = adminsSrv.admins;
  $scope.write_admin = function(){ adminsSrv.writeAdmin(); };
  $scope.select_edit_admin = function(){ adminsSrv.selectEditAdmin(); }
  $scope.select_delete_admin = function(){ adminsSrv.selectDeleteAdmin(); }
  adminsSrv.get(true);
}]); 