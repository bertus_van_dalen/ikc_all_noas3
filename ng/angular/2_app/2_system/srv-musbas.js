manage.factory('MusbasService', ['ManageRepository', '$upload', '$window', function(repo, $upload, $window) {
  var musbasService = {
    upl: {
      files : ["bumblebee"]
    },
    fstyles: [],
    fsubsets: [],
    ftemplates: [],
    
    sstyle: {},
    ssubset: {},
    stemplate: {},
    edited: {}, // is a subset
    editedTemplate: {},
    colorSeries: [],
    confValOpts: [],
    get: function(){
      var that = this;
      var o = {};
      if(this.sstyle.style_id){ o.style_id = this.sstyle.style_id; }
      if(this.ssubset.subset_id){ o.subset_id = this.ssubset.subset_id; }
      if(this.stemplate.template_id){ o.template_id = this.stemplate.template_id; }
      repo.rpc('style','getTemplatesWithSubsetAndStyle', o).then(function(r){
        that.fstyles.length = 0;
        that.fsubsets.length = 0;
        that.ftemplates.length = 0;
        $.extend(that.fstyles, Enumerable.From(r).Distinct(function(i){ return i.style_id; }).ToArray());
        $.extend(that.fsubsets, Enumerable.From(r).Distinct(function(i){ return i.subset_id; }).ToArray());
        $.extend(that.ftemplates, Enumerable.From(r).Distinct(function(i){ return i.template_id; }).ToArray());
      });
    },
    reset_style: function(){
      this.sstyle = {};
      this.get();
    },
    reset_subset: function(){
      this.ssubset = {};
      this.get();
    },
    reset_template: function(){
      this.stemplate = {};
      this.get();
    },
    getSubsetAndColors: function(id){
      var that = this;
      repo.rpc('style','getFullSubsetAggregationAndAllColorSeries',id).then(function(r){
        $.extend(that.edited,r.subset);
        that.colorSeries.length = 0;
        $.extend(that.colorSeries, r.colorSeries);
      });
    },
    getTemplate: function(t){
      var that = this;
      $.each(this.stemplate, function(k,v){ 
        delete that.stemplate[k]; 
      });
      $.extend(this.stemplate, t);
      repo.rpc("style", "getTemplateNg", t.template_id).then(function(r){
        $.each(that.editedTemplate, function(k,v){ 
          delete that.editedTemplate[k]; 
        });
        $.extend(that.editedTemplate, r);
      });
    },
    createTemplateForCompositionId : function(id){
      var that = this;
      repo.rpc("style", "createTemplateForCompositionId", id).then(function(r){
        $.each(that.editedTemplate, function(k,v){ delete that.editedTemplate[k]; });
        $.extend(that.editedTemplate, r.created);
        that.ftemplates.length = 0;
        $.extend(that.ftemplates, Enumerable.From(r.all).Distinct(function(i){ return i.template_id; }).ToArray());
      });
    },
    createTemplateForSubsetId : function(id) {
      var that = this;
      repo.rpc("style", "createTemplateForSubsetId", id).then(function(r){
        $.each(that.editedTemplate, function(k,v){ delete that.editedTemplate[k]; });
        $.extend(that.editedTemplate, r.created);
        that.ftemplates.length = 0;
        $.extend(that.ftemplates, Enumerable.From(r.all).Distinct(function(i){ return i.template_id; }).ToArray());
      });
    },
    getCompositionByCompositionIdUserNameCompositionNameTemplateNameSubsetName : function(viewVal){
      return repo.rpc('style', 'getCompositionByCompositionIdUserNameCompositionNameTemplateNameSubsetName', viewVal).then(function(r){
        return r;
      });
    },
    searchExistingConfigKeys : function(viewVal) {
      return repo.rpc('style', 'searchExistingConfigKeys', viewVal).then(function(r){
        return r;
      });
    },
    listConfValOpts : function(name){
      var that = this;
      repo.rpc('style', 'listConfValOpts', name).then(function(r){
        that.confValOpts.length = 0;
        $.extend(that.confValOpts, r);
      });
    },
    searchExistingConfigValuesForKey : function(templateAddConfName, $viewValue){
      return repo.rpc('style', 'searchExistingConfigValuesForKey', {name: templateAddConfName, viewValue: $viewValue}).then(function(r){
        return r;
      });
    },
    addTemplateConfig : function(name, value){
      var that = this;
      repo.rpc('style', 'addTemplateConfig', {name:name, value:value, templateId:this.editedTemplate.id}).then(function(r){
        $.each(that.editedTemplate, function(k,v){ 
          delete that.editedTemplate[k]; 
        });
        $.extend(that.editedTemplate, r);
      });
    },
    deleteTemplateConfig : function(id){
      var that = this;
      repo.rpc('style', 'deleteTemplateConfig', {id:id, templateId:this.editedTemplate.id}).then(function(){
        var conf = Enumerable.From(that.editedTemplate.sharedConf).Where(function(c){ return c.id === id; }).First();
        var idx = Enumerable.From(that.editedTemplate.sharedConf).IndexOf(conf);
        that.editedTemplate.sharedConf.splice(idx, 1);
      });
    },
    getSubsetBySubsetIdSubsetName : function(viewVal){
      return repo.rpc('style', 'getSubsetBySubsetIdSubsetName', viewVal).then(function(r){
        return r;
      });
    },
    storeEdited: function(){
      return repo.rpc('style','storeFullSubsetAggregation',angular.copy(this.edited));
    },
    duplicateSubset: function(){
      return repo.rpc('style','duplicateFullSubsetAggregation',angular.copy(this.edited));
    },
    addSubpart: function(){
      var that = this;
      repo.rpc('style','addSubpart',this.edited.id).then(function(r){
        $.extend(that.edited,r.subset);
      });
    },
    addClip: function(subpart_id){
      var that = this;
      repo.rpc('style','addClip',subpart_id).then(function(r){
        $.extend(that.edited,r.subset);
      });
    },
    uploadClip: function (file, subclipId) {
      var that = this;
      $upload.upload({
        url: 'manage/clipUpload/' + subclipId + '/' + this.edited.id,
        headers: {'Content-Type': file.type},
        method: 'POST',
        data: file,
        file: file
      }).progress(function (evt) {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        window.console && console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
      }).error(function (data) {
        window.console && console.log('error uploading: ' + data);
      }).success(function (d) {
        that.getSubsetAndColors(that.edited.id);
      });
    },
    removeMp3 : function(subclip){
      var that = this;
      repo.rpc('style','removeMp3',subclip.id).then(function(r){
        $.extend(that.edited,r.subset);
      });
    },
    openTemplateInFlash : function(composition_id){
      var url = $window.ikc.conf.base_url + "#?ikcc=open&ikca_compositionId=" + composition_id;
      var win = $window.open(url, '_blank');
      win.focus();
    },
    storeEditedTemplate : function(){
      repo.rpc('style','saveTemplateNg',angular.copy(this.editedTemplate)).then(function(){
        alert("storeEditedTemplate OK");
      });
    },
    duplicateEditedTemplate : function(){
      var that = this;
      repo.rpc('style','duplicateTemplate',angular.copy(this.editedTemplate)).then(function(r){
        $.each(that.editedTemplate, function(k,v){ delete that.editedTemplate[k]; });
        $.extend(that.editedTemplate, r.duplicated);
        that.ftemplates.length = 0;
        $.extend(that.ftemplates, Enumerable.From(r.all).Distinct(function(i){ return i.template_id; }).ToArray());
      });
    },
    deleteTemplate : function(){
      var that = this;
      repo.rpc('style', 'deleteTemplateNg', angular.copy(this.editedTemplate)).then(function(r){
        $.each(that.editedTemplate, function(k,v){ delete that.editedTemplate[k]; });
        that.ftemplates.length = 0;
        $.extend(that.ftemplates, Enumerable.From(r).Distinct(function(i){ return i.template_id; }).ToArray());
      });
    },
    clearEditedTemplateCompositionContents : function(){
      repo.rpc('style','clearEditedTemplateCompositionContents',angular.copy(this.editedTemplate)).then(function(){
        alert("clearEditedTemplateCompositionContents OK");
      });
    }
  };
  return musbasService;
}]); 