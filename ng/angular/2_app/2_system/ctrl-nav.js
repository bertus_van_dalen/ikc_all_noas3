manage.controller('NavController', ['$scope', '$state', '$timeout', 'AdminService', function($scope, $state, $timeout, adminSrv) {
  if(typeof(ikc) === 'undefined' || typeof(ikc.conf) === 'undefined' || typeof(ikc.conf.db_conf_exists) === 'undefined'){
    alert("technical problem: the ikc.conf.db_conf_exists property is missing. Unable to determine database existence!");
  }
  $scope.admin = adminSrv.admin;
  $scope.moderator = adminSrv.moderator;
  $scope.project = "[project]";
  $scope.set = "[set]";
  $scope.subset = "[subset]";
  $scope.pool = "[pool]";
  $scope.template = "[template]";
  $scope.scroll = "[scroll]";
  $scope.steps = "[steps]";
}]);