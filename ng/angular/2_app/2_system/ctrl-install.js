manage.controller(
        'InstallController', 
        [
          '$scope',
          'InstallRepository',
          'AdminService',
          '$state',
          function(
            $scope, 
            repo,
           adminSrv,
           state)
{
  $scope.adminTemplate = state.get('manage.adminLogin').templateUrl;
  $scope.adminsTemplate = state.get('manage.installAdmins').templateUrl;
  $scope.Enumerable = Enumerable;
  $scope.status = null;
  $scope.projectCollapsed = {};
  $scope.$watch(function() {
    return adminSrv.admin.name;
  }, function(newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getData();
    }
  });
  $scope.getData = function(){
    repo.get().then(function(data){
      $scope.status = data;
      $scope.countDirs();
    });
  };
  $scope.xt = function(r){
    $scope.status = $.extend(false, $scope.status, r);
  };
  $scope.countDirs = function(){
    $scope.status.existingDirectoriesCount = Enumerable.From($scope.status.directories).Where('$.exists').Count();
  };
  $scope.installDir = function(e, dir){
    if($(e.target).is(":checked")){
      repo.installDir(dir.dir).then(function(){
        dir.exists = true;
        $scope.countDirs();
      });
    }
  };
  $scope.update_dbConfig_selection = function(){
    repo.rpc("install", "selectDbConfig", $scope.dbConfig_selection.file).then(function(r){
      $scope.xt(r);
    });
  };
  $scope.update_dbConfig_delete = function(){
    var todel = $scope.dbConfig_delete;
    $scope.dbConfig_delete = null;
    var m = todel.hostname + '/' + todel.database + '/' + todel.username;
    m = "Type 'yes' if you are sure to delete " + m + '?';
    var sure = prompt(m);
    if (sure === 'yes') {
      repo.rpc("install", "deleteDbConfig", todel.file).then(function(r){
        $scope.xt(r);
      });
    }
  };
  $scope.log_out = function(){
    repo.rpc("install", "logOut", {}).then(function(r){
      $scope.xt(r);
    });
  };
  $scope.createDbCredentials = function(){
    if($scope.status.db_conf.file){
      delete $scope.status.db_conf.file;
    }
    repo.rpc("install", "setDbCredentials", $scope.status.db_conf).then(function(r){
      $scope.xt(r);
    });
  };
  $scope.saveDbCredentials = function(){
    repo.rpc("install", "setDbCredentials", $scope.status.db_conf).then(function(r){
      $scope.xt(r);
    });
  };
  $scope.testDbCredentials = function(){
    $scope.status.db_conf.test = true;
    repo.rpc("install", "setDbCredentials", $scope.status.db_conf).then(function(r){
      delete($scope.status.db_conf.test);
      alert("connection successful");
    });
  };
  $scope.toggleTableSelection = function(){
    $.each($scope.status.tables, function(k,v){ v.selected = !v.selected; });
  };
  $scope.dropAllTables = function(){
    m = "Type 'yes' if you are sure to drop all the tables?";
    var sure = prompt(m);
    if (sure === 'yes') {
      repo.rpc("install", "dropTables", {}).then(function(r){
        $scope.xt(r);
      });
    }
  };
  $scope.sync_projects = function() {
    repo.rpc("install", "syncProjects", {}).then(function(r) {
      $scope.xt(r);
    });
  };
  $scope.create_project = function(){
    $scope.createProjectScreenOpen = true;
    $scope.projectsUncollapsed = false;
    $scope.cProj = null;
    $scope.createProjectForm.$setPristine();
  };
  $scope.writeProject = function() {
    var projectName = $scope.cProj.name;
    repo.rpc("install","writeProject",angular.copy($scope.cProj)).then(function(r){
      $scope.edit_project(projectName);
    });
  };
  $scope.add_playlist_to_cproj = function(){
    var projectName = $scope.cProj.name;
    repo.rpc("install","writeProject",angular.copy($scope.cProj)).then(function(r){
      repo.rpc("install","addPlaylistForProjectId",$scope.cProj.id).then(function(r){
        $scope.edit_project(projectName);
      });
    });
  }
  $scope.build_playlist_for_cproj = function(){
    var projectName = $scope.cProj.name;
    repo.rpc("install","writeProject",angular.copy($scope.cProj)).then(function(r){
      repo.rpc("install","buildPlaylistForProjectId",$scope.cProj.id).then(function(r){
        $scope.edit_project(projectName);
      });
    });
  }
  $scope.edit_project = function(name){
    repo.rpc("install","getProjectForEdit",{name:name}).then(function(r){
      $scope.cProj = r;
      $scope.createProjectScreenOpen = true;
      $scope.projectsUncollapsed = false;
    });
  };
  $scope.add_domain = function(){
    repo.rpc("install","createDomain",{baseurl:$scope.cBaseurl}).then(function(r){
      $scope.xt(r);
    });
  };
  $scope.run_db_backup = function() {
    var bakfile = $scope.status.db_backups[$scope.dbBackup_selection];
    $('#myModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    var prev = function(e) {
      e.preventDefault();
    };
    var hideModal = function(){
      $('#myModal').off('hide.bs.modal', prev);
      $('#myModal').modal('hide');
    };
    $('#myModal').on('hide.bs.modal', prev);
    if ($scope.dbBackup_selection) {
      repo.rpc("install", "runDbBackup", bakfile).then(function(r){
        hideModal();
        $scope.xt(r);
      }, hideModal);
    }
  };
}]);
manage.factory ('InstallRepository', ['$http','$q', 'WebApiError', function ($http, $q, webApiError) { 
  return {
    apiUrl:"manage/install/",
    get: function () { 
      var d = $q.defer();
      $.get(this.apiUrl, function(data) {
        d.resolve(data);
      }, 'JSON').fail( webApiError.handle );
      return d.promise;
    },
    post: function(data, api_url) {
      if(typeof(api_url) === "undefined") api_url = this.apiUrl;
      var d = $q.defer();
      $.post(api_url, data, function(data){
        d.resolve(data);
      }, 'JSON').fail(function(r){ d.reject(r); webApiError.handle(r);});
      return d.promise;
    },
    installDir : function(dir){
      return this.post({dir:dir}, 'manage/installDirs/');
    },
    runDbBackup : function(s){
      return this.post({bak:s}, 'manage/runDbBackup');
    },
    rpc : function(model, command, args){
      return this.post({model:model, command:command, args:args}, 'manage/rpc');
    }
  }; 
}]); 