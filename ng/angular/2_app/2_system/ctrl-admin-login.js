manage.controller(
        'AdminLoginController', 
        [
          '$scope',
          'AdminService',
          function(
            $scope, 
            adminSrv)
{
  $scope.admin = adminSrv.admin;
  $scope.log_out = function(){ adminSrv.logOut(); };
  $scope.log_in = function(){ adminSrv.logIn(); }; 
  $scope.test_auth = function(){ adminSrv.testAuth(); };
}]);