 manage.factory('AdminService', ['ManageRepository', function(repo) {
  var adminService = {
    admin: {},
    moderator: {},
    moderators: [],
    xt : function(r){
      if(r.admin) $.extend(this.admin, r.admin);
      if(r.moderator) $.extend(this.moderator, r.moderator);
    },
    get: function(){
      var that = this;
      repo.rpc('session','getAdmin',{}).then(function(r){
        that.xt(r);
      });
    },
    getModerator: function(){
      var that = this;
      repo.rpc('session','getModerator',{}).then(function(r){
        $.extend(that.moderator,r);
      });
    },
    matchModerator: function(viewValue){
      return repo.rpc('session','matchModerator',viewValue);
    },
    impersonateModerator: function(moderator){
      var that = this;
      repo.rpc('session','impersonateModerator', moderator).then(function(r){
        that.xt(r);
      });
    },
    logOut : function(){
      var that = this;
      repo.rpc('session','logoutAdmin',{}).then(function(r){
        that.xt(r);
      });
    },
    logIn : function(){
      var that = this;
      repo.rpc('session','loginAdmin',{loginName:this.admin.loginName,loginPassword:this.admin.loginPassword}).then(function(r){
        that.admin.loginName = "";
        that.admin.loginPassword = "";
        that.xt(r);
      });
    },
    testAuth: function(){
      repo.rpc("install", "isAuthorizedAdmin", {}).then(function(){
        alert("You really have Admin authorization!");
      });
    },
    setAdmin: function(admin){
      this.xt(admin);
    }
  };
  adminService.get(true);
  return adminService;
}]);