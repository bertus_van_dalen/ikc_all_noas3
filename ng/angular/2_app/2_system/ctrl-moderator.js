manage.controller(
        'ModeratorController', 
        [
          '$scope',
          'AdminService',
          function(
            $scope, 
            adminSrv)
{
  $scope.admin = adminSrv.admin;
  $scope.moderator = adminSrv.moderator;
  $scope.moderators = adminSrv.moderators;
  $scope.match_moderator = function($viewValue){
    return adminSrv.matchModerator($viewValue).then(function(r){
      return r;
    });
  };
  $scope.impersonate_moderator = function(){
    adminSrv.impersonateModerator($scope.impersonatedModerator);
  };
  adminSrv.getModerator();
}]); 