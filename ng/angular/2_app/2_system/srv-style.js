manage.factory('StyleService', ['ManageRepository', '$window', function(repo, $window) {
    var styleService = {
      
      // properties available for binding (will not be re-assigned, only modified)
      styles : [],
      style : {},
      
      _xt : function(d){
        $.each(this.style, function(k,v){ 
          delete this.style[k]; 
        });
        while(this.styles.length){
          this.styles.pop();
        }
        $.extend(this.styles, d.styles);
        $.extend(this.style, d.style);
      },
      
      initialize : function(){
        that = this;
        repo.rpc('style','getStylesAndRuleTypes', name).then(function(r){ that._xt(r); });
      },
      
      createStyle : function(name){
        that = this;
        repo.rpc('style','createStyle', name).then(function(r){ that._xt(r); });
      },
      
      loadStyle : function(name){
        that = this;
        repo.rpc('style','loadStyle', name).then(function(r){ that._xt(r); });
      },
      
      deleteStyle : function(){
        that = this;
        repo.rpc('style','deleteStyle', this.style.name).then(function(r){ $window.location.reload(); });
      },
      
      saveStyle : function(){
        that = this;
        repo.rpc('style','saveStyle', angular.copy(this.style)).then(function(){ alert("ok, saved!"); })
      },
      
      createSubset : function(name){
        that = this;
        repo.rpc('style','generateSubset', name).then(function(){ alert("ok, subset created! Go to the subset page to further moderate the subset."); })
      }
    };
    return styleService;
}]);