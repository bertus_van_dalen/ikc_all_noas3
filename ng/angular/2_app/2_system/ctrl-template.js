 manage.controller(
        'TemplateController', 
        [
          '$scope',
          '$window',
          '$modal',
          'AdminService',
          'MusbasService',
          function(
            $scope,
            $window,
            $modal,
            adminSrv,
            musbas)
{
  $.extend($scope, musbas);
  $scope.moderator = adminSrv.moderator;
  
  musbas.get(true);
  
  $scope.tplSelected = function(t){
    if(t) musbas.getTemplate(t);
  };
  
  $scope.tplByCompSelect = function(c){
    if(c) musbas.createTemplateForCompositionId(c.composition_id);
  };
  
  $scope.tplBySubsSelect = function(s){
    if(s) musbas.createTemplateForSubsetId(s.subset_id);
  };
  
  $scope.showTemplateStatistics = function(tplId){
    $scope.tplStatsUncollapsed = true;
  };
  
  $scope.hideTemplateStatistics = function(){
    $scope.tplStatsUncollapsed = false;
  }
  
}]);  