manage.controller(
        'ExportDevEnvController', 
        [
          '$scope',
          'ExportDevEnvRepository', 
          '$interval',
          '$state',
          function(
            $scope, 
            repo, 
            $interval,
            $state)
{
  $scope.$on('$destroy', function () {
    $scope.cancelTimer();
  });
  $scope.resetFormDefaults = function(){
    var defaults = {
      selected : ""
    };
  };
  $scope.addComposition = function(){
    repo.rpc("MComposition", "getComposition", $scope.addComp).then(function(d) {
      if(typeof($scope.devenv.addComps) === 'undefined') { $scope.devenv.addComps = []; }
      $scope.devenv.addComps.push(d);
    });
  };
  $scope.download = function(exp){
    window.open(exp.downloadUrl, '_blank', '');
  };
  $scope.remove = function(){
    $scope.devenv.command = "remove";
    $scope.post();
  };
  $scope.create = function(){
    $scope.devenv.command = "create";
    $scope.post();
  };
  $scope.post = function(){
    repo.post(angular.copy($scope.devenv)).then(function(data){
      $scope.devenv = data;
      $scope.processViewModel();
    });
  };
  $scope.stop;
  $scope.startUpdateExportsTimer = function(){
    $scope.stop = $interval(function(){
      repo.get().then(function(data){
        $scope.devenv.exports = data.exports;
        $scope.processViewModel();
      });
    }, 1000);
  };
  $scope.cancelTimer = function(){
    if(typeof($scope.stop) !== 'undefined'){
      $interval.cancel($scope.stop);
      $scope.stop = undefined;
    }
  }
  $scope.processViewModel = function(){
    $scope.cancelTimer();
    if(Enumerable.From($scope.devenv.exports).Where(function(e){ return e.status !== 'done';}).Any()){
      $scope.startUpdateExportsTimer();
    }
  };
  repo.get().then(function(data){
    if(!data.loggedIn){
      $state.go("adminLogin");
    }
    $scope.devenv = data;
    $scope.processViewModel();
  });
}]);
manage.factory ('ExportDevEnvRepository', ['$http','$q', 'WebApiError', function ($http, $q, webApiError) { 
  return {
    apiUrl:"manage/exportDevEnv/",
    get: function () { 
      var d = $q.defer();
      $.get(this.apiUrl, function(data) {
        d.resolve(data);
      }, 'JSON').fail( webApiError.handle );
      return d.promise;
    },
    post: function(data, api_url) {
      if(typeof(api_url) === 'undefined') api_url = this.apiUrl;
      var d = $q.defer();
      $.post(api_url, data, function(data){
        d.resolve(data);
      }, 'JSON').fail(function(r){ d.reject(r); webApiError.handle(r);});
      return d.promise;
    },
    rpc : function(model, command, args){
      return this.post({model:model, command:command, args:args}, 'manage/rpc');
    }
  }; 
}]);