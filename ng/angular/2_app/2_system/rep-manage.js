manage.factory ('ManageRepository', ['$q', 'WebApiError', function ($q, webApiError) { 
  return {
    post: function(data, api_url) {
      if(typeof(api_url) === "undefined") api_url = this.apiUrl;
      var d = $q.defer();
      $.post(api_url, data, function(data){
        d.resolve(data);
      }, 'JSON').fail(function(r){ d.reject(r); webApiError.handle(r);});
      return d.promise;
    },
    rpc : function(model, command, args){
      return this.post({model:model, command:command, args:args}, 'manage/rpc');
    }
  }; 
}]); 