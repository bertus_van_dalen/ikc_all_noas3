 manage.controller(
        'SubsetController', 
        [
          '$scope',
          '$window',
          '$modal',
          'AdminService',
          'MusbasService',
          function(
            $scope,
            $window,
            $modal,
            adminSrv,
            musbas)
{
  $.extend($scope, musbas);
  $scope.moderator = adminSrv.moderator;
  $scope.subsetSelectionUncollapsed = true;
  $scope.subpartUncollapse = {};
  $scope.subpartClipsUncollapse = {};
  $scope.updateAllClipsOfSubpart = function(subpartId, propertyName, value){
    Enumerable.From(musbas.edited.ownSubpart).Where(function(sp){ 
      return sp.id === subpartId; 
    }).SelectMany(function(sp){ 
      return sp.ownSubclip; 
    }).ForEach(function(sc){
      var prps = propertyName.split('.');
      var c = sc;
      while(prps.length > 1){
        var p = prps.shift();
        c = c[p];
      }
      c[prps[0]] = value;
    });
  };
  $scope.updateAllClipsOfSubset = function(propertyName, value){
    Enumerable.From(musbas.edited.ownSubpart).SelectMany(function(sp){
      return sp.ownSubclip;
    }).ForEach(function(sc){
      sc[propertyName] = value;
    });
  };
  $scope.updateAllSubsets = function(propertyName, value){
    Enumerable.From(musbas.edited.ownSubpart).ForEach(function(sp){
      sp[propertyName] = value;
    });
  };
  $scope.randomColorSchemaForAllSubparts = function(){
    var shuf = Enumerable.From(musbas.colorSeries).Shuffle().ToArray();
    Enumerable.From(musbas.edited.ownSubpart).ForEach(function(sp){
      var colorSeries = shuf.shift();
      $scope.distributeColorSeriesOverSubpart(colorSeries,sp);
    });
  };
  $scope.distributeColorSeriesOverSubpart = function(cs, sp){
    var colors = cs.ownColor;
    var tri = 0;
    var trimax = colors.length - 1;
    var up = true;
    Enumerable
            .From(sp.ownSubclip)
            .Where(function (sc) {
              return sc.active === "1";
            })
            .OrderBy(function (sc) {
              return sc.order;
            })
            .ForEach(function (sc) {
              sc.color = colors[tri].v;
              tri = tri + (up ? 1 : -1);
              if (tri === trimax || tri === 0)
                up = !up;
            });
  };
  $scope.autoOrderAllSubparts = function(){
    var i = 0;
    Enumerable.From($scope.edited.ownSubpart).OrderBy(function(sp){
      return sp.name;
    }).ForEach(function(sp){
      sp.order = i;
      i++;
    });
  };
  $scope.autoOrderAllClipsOfAllSubparts = function(){
    Enumerable.From($scope.edited.ownSubpart).ForEach(function(sp){
      $scope.autoOrderAllClipsOfSubpart(sp);
    });
  };
  $scope.autoOrderAllClipsOfSubpart = function(sp){
    var i = 0;
    Enumerable.From(sp.ownSubclip).OrderBy(function(sc){
      return sc.name;
    }).ForEach(function(sc){
      sc.order = i;
      i++;
    });
  };
  $scope.columnsShown = {};
  $scope.hideSubsetEditorColumn = function(topic){
    delete $scope.columnsShown[topic];
  };
  $scope.showSubsetEditorColumn = function(topic){
    $scope.columnsShown[topic] = true;
  };
  $scope.subsetEditorColumnVisible = function(topic){
    return $scope.columnsShown[topic] === true;
  };
  $scope.browseServer = function(selector){
    $window.ikc.browseServer(selector);
  };
  $scope.getClipSrc = function(id){
    return $window.ikc.conf.clips_url + id + '.MP3';
  };
  $scope.storeEdited = function(){
    var numUserCompositions = Enumerable.From($scope.fsubsets).Where(function(s){ return s.subset_id === $scope.edited.id; }).First().num_user_compositions;
    if(numUserCompositions > 0 && !adminSrv.admin.name){
      $scope.errorModal("You are not allowed to change a subset that already has user compositions. Consider cloning the subset and changing the clone...");
    }else{
      var r = confirm("As an administrator, you are about to update a subset that already has user compositions. Are you sure?");
      if (r !== true) {
          return;
      }
    }
    var pleaseWaitModal = $scope.errorModal("please wait...");
    musbas.storeEdited().then(function(){
      pleaseWaitModal.close();
      $scope.errorModal("Successfully stored the subset!");
    });
  };
  
  $scope.duplicateSubset = function(){
    var pleaseWaitModal = $scope.errorModal("please wait...");
    musbas.duplicateSubset().then(function(){
      pleaseWaitModal.close();
      $scope.errorModal("Successfully duplicated the subset!");
    });
  };
  
  $scope.files = [];
  
  $scope.fileDropped = function($files, $evt, $rejectedFiles, subclipId) {
    if(typeof($files) !== "object"){ return; }
    if(typeof($files.length) !== "number"){ return; }
    if($files.length !== 1){ return; }
    if(typeof($files[0]) !== "object"){ return; }
    if(typeof($files[0].type) !== "string"){ return; }
    if($files[0].type !== "audio/mp3"){ return; }
    if(typeof($files[0].size) !== "number"){ return; }
    if($files[0].size > 5242880){ return; }
    musbas.uploadClip($files[0], subclipId);
  };
  
  musbas.get(true);
  
  $scope.topics = {
    restore : "Use the round arrow to restore ALL values in the subset to how they were before you began editing. NB - once you pressed the 'save' button, you can NOT use this to undo anymore!",
    storeEdited : "Use the save button to save your changes.<strong>You will not be allowed to store subsets that already have user compositions. Consider creating a clone...</strong>",
    openInFlash : "Use this to open the template of this composition in the composition environment.<br/><br/><strong>To view your modifications you will have to store them first!</strong>",
    subpartName : "The name of a subpart is prominently displayed above the collection of clips.",
    subpartOrderAll : "The order determines in what order they will be displayed. From left to right and bottom to top, the lowest numbers go first.<br/><strong>Press the sort button to order alphabetically based on subpart name</strong>.",
    subpartOrder : "The order determines in what order they will be displayed. From left to right and bottom to top, the lowest numbers go first.",
    subpartActive : "Inactive subsets will not be displayed",
    subpartMarginX : "Margin X is the horizontal margin that spaces clips apart within a subpart section.",
    subpartMarginY : "Margin Y is the vertical margin that spaces clips apart within a subpart section.",
    subpartWidth : "Subpart width means the width of the section that is used to display clips onto it.",
    clipName : "The clip name is currently not displayed to the user but, who knows... please keep it suitable to do so.",
    clipActive : "Clips that are not active are not shown in the collection.",
    clipTag : "The tag is shown on the clip. It must not be longer than three characters. Display of it may be turned off by a setting on a per-template basis.",
    clipOrderAll : "Clips placement left to right, top to bottom order relate to their numerical ascending order.<br/><strong>Press the sort button to order alphabetically within subpart based on clip name</strong>.",
    clipOrder : "Clips placement left to right, top to bottom order relate to their numerical ascending order.",
    clipColor : "The background color of the clip. When an image (aka icon) entirely covers the clip, this color remains unseen.",
    clipColorAll : "The background color of the clip. When an image (aka icon) entirely covers the clip, this color remains unseen.<br/><br/><strong>Using the global subset color editing for all clips you can <br/>1. Set the background color of all the clips at once, to the same color, using the color picker OR <br/>2. Use the random button to assign random color schema for each subpart where each individual clip within a subpart will have a slightly different color from that color schema.</strong>",
    clipColorSchema : "Choose a color schema to distribute over the clips of this subset using the dropdown menu.",
    clipWidth : "This field is about the icon, not about the clip (as the clip width is determined by the duration of its sound).<br/><br/>- If you specify widht only, the height will be proportional to the width and vice versa.<br/>- If you specify a percentage, it will be related to the height or width of the containing clip. Example: 105%.<br/>- If you specify pixels, they will be used as an absulute value for that dimension. Example: 80px.<br/>- If you speficy relative pixels, the will be added or substracted to/from the containing clip dimension. Example: +=5px or -= 5px",
    clipHeight : "This field is about the icon, not about the clip (as the clip height is determined by the height of its track).<br/><br/>- If you specify widht only, the height will be proportional to the width and vice versa.<br/>- If you specify a percentage, it will be related to the height or width of the containing clip. Example: 105%.<br/>- If you specify pixels, they will be used as an absulute value for that dimension. Example: 80px.<br/>- If you speficy relative pixels, the will be added or substracted to/from the containing clip dimension. Example: +=5px or -= 5px",
    clipAudio : "For reference, you may listen to the MP3 using the player.",
    clipIcon : "The image (aka icon) displayed on the clip. If its source is an SVG file it will be rendered to an image but you will not see that image yet when you browse the images.",
    finderIcon: "If not set, the clip icon will be used. The image (aka icon) displayed on the clip in the finder. If its source is an SVG file it will be rendered to an image but you will not see that image yet when you browse the images.",
    finderIconRoll: "If not set, the clip icon will be used. The image (aka icon) displayed on the clip in the finder ON ROLL OVER. If its source is an SVG file it will be rendered to an image but you will not see that image yet when you browse the images.",
    clipEntrypoint : "The clip may start sounding before taking space on the grid. Here, you submit the amount of milliseconds for that period. E.g. a clip that sounds one second before being in view has an entrypoint of 1000.",
    clipExitpoint : "The clip may end sounding (e.g. reverberation) after it ended. The exitpoint should be duration from the entrypoint.",
    duplicateSubset : "You may create a duplicate of this subset to work on. All clips you will not have set to active will not be transmitted to the new subset. All clips active will be copied so you may modify the duplicate without impacting existing material."
  };
  
  $scope.openHelpModal = function(topic){
    
    var content = $scope.topics[topic];
    
    var ModalInstanceCtrl = function($scope, $modalInstance, htmlContent, $sce) {
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
      $scope.htmlContent = content;
      $scope.deliberatelyTrustDangerousSnippet = function() {
        return $sce.trustAsHtml($scope.htmlContent);
      };
    };
    var modalInstance = $modal.open({
      templateUrl: ikc.conf.angular_templates + 'modal.html',
      controller: ModalInstanceCtrl,
      size: 'lg',
      resolve: {
        htmlContent: function () {
          return content;
        }
      }
    });
  };
  
  $scope.errorModal = function(text){
    
    var content = text;
    
    var ModalInstanceCtrl = function($scope, $modalInstance, htmlContent, $sce) {
      $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
      };
      $scope.htmlContent = content;
      $scope.deliberatelyTrustDangerousSnippet = function() {
        return $sce.trustAsHtml($scope.htmlContent);
      };
    };
    var modalInstance = $modal.open({
      templateUrl: ikc.conf.angular_templates + 'modal.html',
      controller: ModalInstanceCtrl,
      size: 'lg',
      resolve: {
        htmlContent: function () {
          return content;
        }
      }
    });
    
    return modalInstance;
  };
}]); 