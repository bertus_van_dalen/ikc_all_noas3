var manage = angular.module('manage', ['ui.bootstrap', 'ui.multiselect', 'ui.router', 'colorpicker.module', 'angularFileUpload']);
manage.config(function($stateProvider, $urlRouterProvider){

  // For any unmatched url, send to /route1
  $urlRouterProvider.otherwise("/manage");
  
  $stateProvider.state('manage', {
    url: "/manage",
    templateUrl: ikc.conf.angular_templates + 'manage.html' + '?cb=' + (new Date().getTime())
  });
  
  $stateProvider.state('manage.welcome', {
    url: "/welcome",
    templateUrl: ikc.conf.angular_templates + 'manage-init.html' + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.export-devenv', {
    url: "/export_devenv",
    templateUrl: ikc.conf.angular_templates + "manage-export-devenv.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.install', {
    url: "/install",
    templateUrl: ikc.conf.angular_templates + "manage-install.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.adminLogin', {
    url: "/adminLogin",
    templateUrl: ikc.conf.angular_templates + "manage-admin-login.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.moderator', {
    url: "/moderator",
    templateUrl: ikc.conf.angular_templates + "manage-moderator.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.set', {
    url: "/set",
    templateUrl: ikc.conf.angular_templates + "manage-set.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.subset', {
    url: "/subset",
    templateUrl: ikc.conf.angular_templates + "manage-subset.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.template', {
    url: "/template",
    templateUrl: ikc.conf.angular_templates + "manage-template.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.installAdmins', {
    url: "/installAdmins",
    templateUrl: ikc.conf.angular_templates + "manage-admins-install.html" + "?cb=" + (new Date().getTime())
  });  
  
  $stateProvider.state('manage.scroll', {
    url: "/scroll",
    templateUrl: ikc.conf.angular_templates + "manage-scroll.html" + "?cb=" + (new Date().getTime())
  });  
  
  $stateProvider.state('manage.test-e2e', {
    url: "/testE2E",
    templateUrl: ikc.conf.angular_templates + "manage-test-e2e.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.project', {
    url: "/project",
    templateUrl: ikc.conf.angular_templates + "manage-project.html" + "?cb=" + (new Date().getTime())
  });
  
  $stateProvider.state('manage.viewAgents', {
    url: "/viewAgents",
    templateUrl: ikc.conf.angular_templates + "manage-viewAgents.html" + "?cb=" + (new Date().getTime())
  });
});

manage.run(function($rootScope) {
  $rootScope.name = "Root Scope";
});