#!/usr/bin/env oo-ruby

require 'rubygems'
require 'json'

json = File.read(ENV['HOME']+"/gear-registry/gear-registry.json")
gear_list = JSON.parse(json)

gear_count = 0

gear_list['web'].each do |uuid, array|
  gear_count += 1
end

puts gear_count